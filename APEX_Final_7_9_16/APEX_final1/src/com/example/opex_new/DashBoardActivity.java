package com.example.opex_new;


import android.app.ActionBar.LayoutParams;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;


public class DashBoardActivity extends ActionBarBaseActivity  {

	private Button btnCreateMessageDb, btnGroupDB, btnTempDB, /* btnSyncDB, */
			btnReportDB, btnFagDB, btnSettingDB, btnLogout, btnUpgrade, btnOkLogout, btnCancelLogout;
	private TextView tvUserName, tvLogoutMsg;
	public TextView initiate,reports,templates,autoforward,settings,group;
	private ImageView ivPhoto;
	private Dialog logoutDialog;
	private RelativeLayout rlDashboard;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//actionBar.show();
		//setActionBarTitle(getString(R.string.title_dashboard));
		actionBar.hide();
		setContentView(R.layout.activity_dashboard);
		ivPhoto = (ImageView) findViewById(R.id.ivPhoto);
		btnCreateMessageDb = (Button) findViewById(R.id.btnCreateMessageDb);
		btnGroupDB = (Button) findViewById(R.id.btnGroupDB);
		btnTempDB = (Button) findViewById(R.id.btnTempDB);
		// btnSyncDB = (Button) findViewById(R.id.btnSyncDB);
		btnReportDB = (Button) findViewById(R.id.btnReportDB);
		btnFagDB = (Button) findViewById(R.id.btnFagDB);
		btnSettingDB = (Button) findViewById(R.id.btnSettingDB);
		tvUserName = (TextView) findViewById(R.id.tvUserName);
		btnLogout = (Button) findViewById(R.id.btnLogout);
		btnUpgrade = (Button) findViewById(R.id.btnUpgrade);
		rlDashboard = (RelativeLayout) findViewById(R.id.rlDashboard);
          initiate=(TextView)findViewById(R.id.initiate);
          reports=(TextView)findViewById(R.id.reports);
          group=(TextView)findViewById(R.id.group);
          templates=(TextView)findViewById(R.id.templates);
          autoforward=(TextView)findViewById(R.id.autoforward);
          settings=(TextView)findViewById(R.id.setting);
		btnGroupDB.setOnClickListener(this);
		btnTempDB.setOnClickListener(this);
		// btnSyncDB.setOnClickListener(this);
		btnReportDB.setOnClickListener(this);
		btnFagDB.setOnClickListener(this);
		btnSettingDB.setOnClickListener(this);
		btnCreateMessageDb.setOnClickListener(this);
		btnLogout.setOnClickListener(this);

		startSMSIntentService();

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if (!TextUtils.isEmpty(preferenceHelper.getUserName())) {
			tvUserName.setText(
					getString(R.string.text_hello) + preferenceHelper.getUserName().substring(0, 1).toUpperCase()
							+ preferenceHelper.getUserName().substring(1));
		}
		if (preferenceHelper.getUserPhoto() != null) {
			new AQuery(this).id(ivPhoto).image(preferenceHelper.getUserPhoto());
		}
//
//		setFontColor(tvUserName);
//		// setFontSize(tvUserName);
	setFonts(initiate);
	setFonts(reports);
	setFonts(autoforward);
	setFonts(templates);
	setFonts(group);
	setFonts(settings);
	setFontColor(initiate);
	setFontColor(reports);
	setFontColor(autoforward);
	setFontColor(templates);
	setFontColor(group);
	setFontColor(settings);
	

		setFontColor(tvUserName);
		// setFontSize(btnCreateMessageDb);
		setFonts(tvUserName);
	
//		setFontColor(btnFagDB);
//		 setFontSize(btnFagDB);
	setFonts(btnUpgrade);
	setFontColor(btnUpgrade);
//
//		setFontColor(btnGroupDB);
//		// setFontSize(btnGroupDB);
setFonts(btnLogout);
setFontColor(btnLogout);
//
//		setFontColor(btnReportDB);
//		// setFontSize(btnReportDB);
//		setFonts(btnReportDB);
//
//		setFontColor(btnSettingDB);
//		// setFontSize(btnSettingDB);
//		setFonts(btnSettingDB);
//
//		setFontColor(btnTempDB);
//		// setFontSize(btnTempDB);
//		setFonts(btnTempDB);

	//	SetBackgroundColor(btnUpgrade);
		// setFontSize(btnUpgrade);
//		setFonts(btnUpgrade);

	//	SetBackgroundColor(btnLogout);
		// setFontSize(btnLogout);
//		setFonts(btnLogout);

//	setBackground(rlDashboard);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent(DashBoardActivity.this, GroupActivity.class);
		switch (v.getId()) {
		case R.id.btnCreateMessageDb:
			intent.putExtra(Const.POSITION, Const.CREATE_MESSAGE);
			startActivity(intent);
			break;
		case R.id.btnGroupDB:
			intent.putExtra(Const.POSITION, Const.GROUPS);
			startActivity(intent);
			break;
		case R.id.btnTempDB:
			intent.putExtra(Const.POSITION, Const.TEMPLETES);
			startActivity(intent);
			break;
		case R.id.btnReportDB:
			intent.putExtra(Const.POSITION, Const.REPORT_VIEW);
			startActivity(intent);
			break;
		case R.id.btnFagDB:
			intent.putExtra(Const.POSITION, Const.AUTO_RELAY);
			startActivity(intent);
			break;
		case R.id.btnSettingDB:
			intent.putExtra(Const.POSITION, Const.SETTIINGS);
			startActivity(intent);
			break;
		case R.id.btnLogout:
			showLogoutDialog();
			break;
		case R.id.ivBack:
			onBackPressed();
			break;
		case R.id.btnCancelLogout:
			logoutDialog.dismiss();
			break;
		case R.id.btnOkLogout:
			Intent logoutIntent = new Intent(DashBoardActivity.this, LoginActivity.class);
			logoutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(logoutIntent);
			finish();
			preferenceHelper.Logout();
			break;
		// default:
		// break;
		}

	}

	private void showLogoutDialog() {

		logoutDialog = new Dialog(this);
		logoutDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		logoutDialog.setContentView(R.layout.dialog_logout);

		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(logoutDialog.getWindow().getAttributes());
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = LayoutParams.WRAP_CONTENT;
		lp.gravity = Gravity.CENTER;

		logoutDialog.getWindow().setAttributes(lp);

		tvLogoutMsg = (TextView) logoutDialog.findViewById(R.id.tvLogoutMsg);
		btnOkLogout = (Button) logoutDialog.findViewById(R.id.btnOkLogout);
		btnCancelLogout = (Button) logoutDialog.findViewById(R.id.btnCancelLogout);
		btnOkLogout.setOnClickListener(this);
		btnCancelLogout.setOnClickListener(this);

		// setFontSize(tvLogoutMsg);
		setFonts(tvLogoutMsg);

		SetBackgroundColor(btnOkLogout);
		// setFontSize(btnOkLogout);
		setFonts(btnOkLogout);

		SetBackgroundColor(btnCancelLogout);
		// setFontSize(btnCancelLogout);
		setFonts(btnCancelLogout);

		logoutDialog.show();
		// new AlertDialog.Builder(this)
		// .setMessage(getString(R.string.text_logout_msg))
		// .setPositiveButton(R.string.text_ok,
		// new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog,
		// int which) {
		// // TODO Auto-generated method stub
		// Intent intent = new Intent(
		// DashBoardActivity.this,
		// LoginActivity.class);
		// intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		// startActivity(intent);
		// finish();
		// preferenceHelper.Logout();
		// }
		// })
		// .setNegativeButton(R.string.text_cancel,
		// new DialogInterface.OnClickListener() {
		//
		// @Override
		// public void onClick(DialogInterface dialog,
		// int which) {
		// // TODO Auto-generated method stub
		// dialog.dismiss();
		// }
		// }).show();
	}
}
