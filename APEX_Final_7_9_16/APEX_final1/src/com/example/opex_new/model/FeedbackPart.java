package com.example.opex_new.model;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class FeedbackPart implements Serializable, Parcelable {

	private long id, feedbackId;
	private String msg, time;
	private Member member;
	private boolean sent, delivered, received;
	private int retry, answerA, answerB, answerC,answerD,answerE, msgStatus;

	public int getMsgStatus() {
		return msgStatus;
	}

	public void setMsgStatus(int msgStatus) {
		this.msgStatus = msgStatus;
	}

	public boolean isReceived() {
		return received;
	}

	public void setReceived(boolean received) {
		this.received = received;
	}

	public int getAnswerA() {
		return answerA;
	}

	public void setAnswerA(int answerA) {
		this.answerA = answerA;
	}

	public int getAnswerB() {
		return answerB;
	}

	public void setAnswerB(int answerB) {
		this.answerB = answerB;
	}

	public int getAnswerC() {
		return answerC;
	}

	public void setAnswerC(int answerC) {
		this.answerC = answerC;
	}
	public int getAnswerD() {
		return answerD;
	}

	public void setAnswerD(int answerD) {
		this.answerD = answerD;
	}

	public int getAnswerE() {
		return answerE;
	}

	public void setAnswerE(int answerE) {
		this.answerE = answerE;
	}

	public long getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(long feedbackId) {
		this.feedbackId = feedbackId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

	public int getRetry() {
		return retry;
	}

	public void setRetry(int retry) {
		this.retry = retry;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}
}
