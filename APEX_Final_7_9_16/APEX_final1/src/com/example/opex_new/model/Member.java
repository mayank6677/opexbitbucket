package com.example.opex_new.model;

import java.io.Serializable;

import android.os.Parcel;
import android.os.Parcelable;

public class Member implements Parcelable, Serializable {

	private int id;
	private String name, phone;
	private String photo;
	public boolean isSelected;

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setIsSelected(boolean isSelect) {
		isSelected = isSelect;
	}

	public boolean getIsSelected() {
		return isSelected;
	}

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		// TODO Auto-generated method stub

	}

}
