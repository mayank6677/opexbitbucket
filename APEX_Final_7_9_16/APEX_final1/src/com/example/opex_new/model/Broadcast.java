package com.example.opex_new.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Broadcast implements Serializable {

	private int id, sent, delivered, msgStatus;
	private String time, message;
	private Group group;
	private ArrayList<BroadcastPart> broadcastPartList;

	public int getMsgStatus() {
		return msgStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setMsgStatus(int msgStatus) {
		this.msgStatus = msgStatus;
	}

	public ArrayList<BroadcastPart> getBroadcastPartList() {
		return broadcastPartList;
	}

	public void setBroadcastPartList(ArrayList<BroadcastPart> broadcastPartList) {
		this.broadcastPartList = broadcastPartList;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSent() {
		return sent;
	}

	public void setSent(int sent) {
		this.sent = sent;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
