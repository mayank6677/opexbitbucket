package com.example.opex_new.model;

public class Contact {

	private String contact;
	public static String name;
	public boolean isChecked;

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		Contact.name = name;
	}

}
