package com.example.opex_new.model;

import java.io.Serializable;

public class BroadcastPart implements Serializable {

	private long id, broadcastId;
	private String msg, time;
	private Member member;
	private boolean sent, delivered;
	private int retry;

	public long getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(long broadcastId) {
		this.broadcastId = broadcastId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}

	public boolean isSent() {
		return sent;
	}

	public void setSent(boolean sent) {
		this.sent = sent;
	}

	public boolean isDelivered() {
		return delivered;
	}

	public void setDelivered(boolean delivered) {
		this.delivered = delivered;
	}

	public int getRetry() {
		return retry;
	}

	public void setRetry(int retry) {
		this.retry = retry;
	}
}
