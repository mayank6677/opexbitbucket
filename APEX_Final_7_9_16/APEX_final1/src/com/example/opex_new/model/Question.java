package com.example.opex_new.model;

import java.io.Serializable;

public class Question implements Serializable {

	private String msg, time;
	private long questionNo, feedbackId;
	private int id, send, delivered, received, answerA, answerB, answerC;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public int getAnswerA() {
		return answerA;
	}

	public void setAnswerA(int answerA) {
		this.answerA = answerA;
	}

	public int getAnswerB() {
		return answerB;
	}

	public void setAnswerB(int answerB) {
		this.answerB = answerB;
	}

	public int getAnswerC() {
		return answerC;
	}

	public void setAnswerC(int answerC) {
		this.answerC = answerC;
	}

	public int getSend() {
		return send;
	}

	public void setSend(int send) {
		this.send = send;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public int getReceived() {
		return received;
	}

	public void setReceived(int received) {
		this.received = received;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public long getQuestionNo() {
		return questionNo;
	}

	public void setQuestionNo(long questionNo) {
		this.questionNo = questionNo;
	}

	public long getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(long feedbackPartId) {
		this.feedbackId = feedbackPartId;
	}

}
