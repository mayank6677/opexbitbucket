package com.example.opex_new.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Feedback implements Serializable {

	private int id, sent, delivered, received, answerA, answerB, answerC, answerD, answerE, msgStatus;
	private String time, messgae;
	private Group group;
	private ArrayList<FeedbackPart> feedbackPartList;

	
	public String getMessgae() {
		return messgae;
	}

	public void setMessgae(String messgae) {
		this.messgae = messgae;
	}

	public int getMsgStatus() {
		return msgStatus;
	}

	public void setMsgStatus(int msgStatus) {
		this.msgStatus = msgStatus;
	}

	public ArrayList<FeedbackPart> getFeedbackPartList() {
		return feedbackPartList;
	}

	public void setFeedbackPartList(ArrayList<FeedbackPart> feedbackPartList) {
		this.feedbackPartList = feedbackPartList;
	}

	public int getReceived() {
		return received;
	}

	public void setReceived(int received) {
		this.received = received;
	}

	public int getAnswerA() {
		return answerA;
	}

	public void setAnswerA(int answerA) {
		this.answerA = answerA;
	}

	public int getAnswerB() {
		return answerB;
	}

	public void setAnswerB(int answerB) {
		this.answerB = answerB;
	}

	public int getAnswerC() {
		return answerC;
	}

	public void setAnswerC(int answerC) {
		this.answerC = answerC;
	}
	public int getAnswerD() {
		return answerD;
	}

	public void setAnswerD(int answerD) {
		this.answerD = answerD;
	}

	public int getAnswerE() {
		return answerE;
	}

	public void setAnswerE(int answerE) {
		this.answerE = answerE;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getSent() {
		return sent;
	}

	public void setSent(int sent) {
		this.sent = sent;
	}

	public int getDelivered() {
		return delivered;
	}

	public void setDelivered(int delivered) {
		this.delivered = delivered;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

}
