package com.example.opex_new.adapter;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Relay;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AutoRelayAdapter extends BaseAdapter {

	private ArrayList<Relay> relayList;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private GroupActivity activity;

	public AutoRelayAdapter(GroupActivity activity, ArrayList<Relay> relayList) {
		// TODO Auto-generated constructor stub
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.relayList = relayList;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return relayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_auto_relay, parent, false);
			holder = new ViewHolder();
			holder.tvPrefixCode = (TextView) convertView.findViewById(R.id.tvPrefixCode);
			holder.tvMasterNo = (TextView) convertView.findViewById(R.id.tvMasterNo);
			holder.tvGroup = (TextView) convertView.findViewById(R.id.tvGroup);
			holder.ivDeleteRelay = (ImageView) convertView.findViewById(R.id.ivDeleteRelay);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvPrefixCode.setText(relayList.get(position).getPrefix());
		holder.tvMasterNo.setText(relayList.get(position).getNumber() + "\n" + relayList.get(position).getName());
		holder.tvGroup.setText(relayList.get(position).getGroup().getName());

		holder.ivDeleteRelay.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new DBHelper(activity).deleteRelay(relayList.get(position).getId());
				relayList.remove(position);
				notifyDataSetChanged();
			}
		});

		activity.setFontColor(holder.tvGroup);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvGroup);

		activity.setFontColor(holder.tvMasterNo);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvMasterNo);

		activity.setFontColor(holder.tvPrefixCode);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvPrefixCode);

		return convertView;
	}

	private class ViewHolder {
		TextView tvMasterNo, tvPrefixCode, tvGroup;
		ImageView ivDeleteRelay;
	}
}
