package com.example.opex_new.adapter;

import java.util.ArrayList;
import java.util.TreeSet;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.model.Templete;
import com.hb.views.PinnedSectionListView.PinnedSectionListAdapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TempleteListAdapter extends BaseAdapter implements PinnedSectionListAdapter {
	private ArrayList<Templete> templeteList;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private TreeSet<Integer> mSeparatorsSet;
	public static final int TYPE_ITEM = 0;
	public static final int TYPE_SEPARATOR = 1;
	private static final int TYPE_MAX_COUNT = TYPE_SEPARATOR + 1;
	private GroupActivity activity;

	public TempleteListAdapter(GroupActivity activity, ArrayList<Templete> templeteList,
			TreeSet<Integer> mSeparatorsSet) {
		this.activity = activity;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.templeteList = templeteList;
		this.mSeparatorsSet = mSeparatorsSet;
	}

	private class ViewHolder {
		TextView tvContact;
		ImageView ivDeleteContact;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return templeteList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return templeteList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		System.out.println("Sending View");
		int type = getItemViewType(position);
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_contact_list, parent, false);
			holder = new ViewHolder();
			holder.tvContact = (TextView) convertView.findViewById(R.id.tvContactName);
			holder.ivDeleteContact = (ImageView) convertView.findViewById(R.id.ivDeleteContact);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvContact.setText(templeteList.get(position).getName());

		if (type == TYPE_SEPARATOR) {
			holder.tvContact.setTypeface(null, Typeface.BOLD);
			holder.ivDeleteContact.setVisibility(View.VISIBLE);
			holder.ivDeleteContact.setImageDrawable(activity.getResources().getDrawable(R.drawable.plus));
			holder.ivDeleteContact.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					activity.addTemplateFragment(templeteList.get(position).getName());
				}
			});

		} else {
			holder.ivDeleteContact.setVisibility(View.GONE);

		}

		activity.setFontColor(holder.tvContact);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvContact);

		return convertView;
	}

	@Override
	public boolean isItemViewTypePinned(int viewType) {
		// TODO Auto-generated method stub
		return viewType == TYPE_SEPARATOR;
	}

	@Override
	public int getViewTypeCount() {
		return TYPE_MAX_COUNT;
	}

	@Override
	public int getItemViewType(int position) {
		return mSeparatorsSet.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
	}

}
