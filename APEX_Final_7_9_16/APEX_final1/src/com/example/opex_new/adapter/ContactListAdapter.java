package com.example.opex_new.adapter;

import java.util.ArrayList;
import java.util.List;

import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.Member;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

public class ContactListAdapter extends BaseAdapter {

	public ArrayList<Member> memberList;
	public ArrayList<Member> wholeContactList;
	public ArrayList<Member> selectedMemberList;
	private LayoutInflater inflater;
	private ViewHolder holder;

	public ContactListAdapter(Context context, ArrayList<Member> memberList, ArrayList<Member> selectedList) {
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.memberList = memberList;
		this.wholeContactList = memberList;
		this.selectedMemberList = selectedList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return memberList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return memberList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		Log.e("GetView", "Got in GetView");
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_contact_list, parent, false);
			holder = new ViewHolder();
			holder.tvContact = (TextView) convertView.findViewById(R.id.tvContactName);
			holder.tvContactNumber = (TextView) convertView.findViewById(R.id.tvContactNumber);
			holder.checkImage=(ImageView) convertView.findViewById(R.id.check);
			// holder.cb = (CheckBox) convertView
			// .findViewById(R.id.cb);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvContact.setText(memberList.get(position).getName());
		holder.tvContactNumber.setText(memberList.get(position).getPhone());
		holder.tvContactNumber.setVisibility(View.VISIBLE);
//		if(holder.checkImage.getVisibility()==View.VISIBLE){
//			holder.checkImage.setVisibility(View.VISIBLE);
//		}else{
//			holder.checkImage.setVisibility(View.GONE);
//		}
		
		// holder.cb.setVisibility(View.VISIBLE);

		// Log.d("", "isChecked" + contactList.get(position).isChecked);
		// holder.cbContact.setChecked(contactList.get(position).isChecked);
		//
		if(selectedMemberList!= null)
if(selectedMemberList.contains(memberList.get(position)))
{
	holder.checkImage.setVisibility(View.VISIBLE);

}
else
{
	holder.checkImage.setVisibility(View.GONE);
}
		return convertView;
	}

	private class ViewHolder {
		TextView tvContact, tvContactNumber;
		ImageView checkImage;
		CheckBox cb;
	}

	public Filter filterMember() {
		Filter filter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				AppLog.Log("filter", constraint + "");
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = wholeContactList;
					results.count = wholeContactList.size();
				} else {
					// We perform filtering operation
					List<Member> filteredList = new ArrayList<Member>();
					for (Member p : wholeContactList) {
						if (p.getName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
							filteredList.add(p);

					}
				
					results.values = filteredList;
					results.count = filteredList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				// Now we have to inform the adapter about the new list filtered
				// if (results.count == 0)
				// notifyDataSetInvalidated();
				// else {
				memberList = (ArrayList<Member>) results.values;
				notifyDataSetChanged();
				AppLog.Log(Const.TAG, "publishResults" + memberList.size());
				// }

			}

		};
		return filter;
	}

}
