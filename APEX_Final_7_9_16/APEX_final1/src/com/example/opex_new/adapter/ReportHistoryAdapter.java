package com.example.opex_new.adapter;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.FeedbackPart;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ReportHistoryAdapter extends BaseAdapter {
	private GroupActivity context;

	private ArrayList<FeedbackPart> feedbackPartList;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private GroupActivity activity;

	public ReportHistoryAdapter(GroupActivity activity, ArrayList<FeedbackPart> feedbackPartList) {
		// TODO Auto-generated constructor stub
		this.context = activity;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.feedbackPartList = feedbackPartList;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return feedbackPartList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_report_history, parent, false);
			holder = new ViewHolder();
			holder.tvNameHistory = (TextView) convertView.findViewById(R.id.tvNameHistory);
			holder.tvfeedbackresponse = (TextView) convertView.findViewById(R.id.feedbackresponse);
			holder.tvReceivedHistory = (TextView) convertView.findViewById(R.id.tvReceivedHistory);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		try{
		AppLog.Log(Const.TAG, "feedbackIDVal" + feedbackPartList.get(position).getAnswerA());
		if(feedbackPartList.get(position).getAnswerA()==1)
		holder.tvfeedbackresponse.setText("A");
		}
		catch(Exception e){
			AppLog.Log(Const.TAG, "feedbackIDValError" +e.getStackTrace());
			
		}
		try{
			AppLog.Log(Const.TAG, "feedbackIDVal" + feedbackPartList.get(position).getAnswerB());
			if(feedbackPartList.get(position).getAnswerB()==1)
			holder.tvfeedbackresponse.setText("B");
			}
			catch(Exception e){
				AppLog.Log(Const.TAG, "feedbackIDValError" +e.getStackTrace());
				
			}
		try{
			AppLog.Log(Const.TAG, "feedbackIDVal" + feedbackPartList.get(position).getAnswerC());
			if(feedbackPartList.get(position).getAnswerC()==1)
			holder.tvfeedbackresponse.setText("C");
			}
			catch(Exception e){
				AppLog.Log(Const.TAG, "feedbackIDValError" +e.getStackTrace());
				
			}
		try{
			AppLog.Log(Const.TAG, "feedbackIDVal" + feedbackPartList.get(position).getAnswerD());
			if(feedbackPartList.get(position).getAnswerD()==1)
			holder.tvfeedbackresponse.setText("D");
			}
			catch(Exception e){
				AppLog.Log(Const.TAG, "feedbackIDValError" +e.getStackTrace());
				
			}
		
		try{
			AppLog.Log(Const.TAG, "feedbackIDVal" + feedbackPartList.get(position).getAnswerE());
			if(feedbackPartList.get(position).getAnswerE()==1)
			holder.tvfeedbackresponse.setText("E");
			}
			catch(Exception e){
				AppLog.Log(Const.TAG, "feedbackIDValError" +e.getStackTrace());
				
			}
		
		
		holder.tvNameHistory.setText(feedbackPartList.get(position).getMember().getName());
		AppLog.Log("isReceived", "isReceived" + feedbackPartList.get(position).isReceived());
		if (feedbackPartList.get(position).isReceived()) {
			holder.tvReceivedHistory.setText(context.getString(R.string.text_received));
		} else {
			holder.tvReceivedHistory.setText(context.getString(R.string.text_pending));
		}

		activity.setFontColor(holder.tvNameHistory);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvNameHistory);

		activity.setFontColor(holder.tvReceivedHistory);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvReceivedHistory);

		return convertView;
	}

	private class ViewHolder {
		TextView tvNameHistory, tvReceivedHistory,tvfeedbackresponse;
	}

}
