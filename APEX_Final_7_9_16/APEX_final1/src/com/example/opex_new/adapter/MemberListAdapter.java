package com.example.opex_new.adapter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Member;

import android.content.Context;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;

import android.widget.ImageView;
import android.widget.TextView;

public class MemberListAdapter extends BaseAdapter {

	private ArrayList<Member> memberList, originalMemberList;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private Context context;
	private boolean isGroupCreated;
	public GroupActivity activity;

	public MemberListAdapter(GroupActivity activity, ArrayList<Member> memberList, boolean isGroupCreated) {
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = activity;
		this.memberList = memberList;
		this.originalMemberList = memberList;
		this.isGroupCreated = isGroupCreated;
		this.activity = activity;
		AppLog.Log("", "size:" + memberList.size());
	}

	private class ViewHolder {
		TextView tvContact, tvContactNumber;
		ImageView ivContactImage, ivDeleteContact;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return memberList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		AppLog.Log("getView", "getView");
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_contact_list, parent, false);
			holder = new ViewHolder();
			holder.tvContact = (TextView) convertView.findViewById(R.id.tvContactName);
			holder.tvContactNumber = (TextView) convertView.findViewById(R.id.tvContactNumber);
			holder.ivContactImage = (ImageView) convertView.findViewById(R.id.ivContactImage);
			holder.ivDeleteContact = (ImageView) convertView.findViewById(R.id.ivDeleteContact);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		AppLog.Log("", "name:" + memberList.get(position).getPhone());
		holder.tvContact.setText(memberList.get(position).getName());
		holder.tvContactNumber.setVisibility(View.VISIBLE);
		holder.tvContactNumber.setText(memberList.get(position).getPhone());
		holder.ivContactImage.setVisibility(View.VISIBLE);
		if (!TextUtils.isEmpty(memberList.get(position).getPhoto())) {
			// holder.ivContactImage.setVisibility(View.VISIBLE);
			try {
				holder.ivContactImage.setImageBitmap(MediaStore.Images.Media.getBitmap(context.getContentResolver(),
						Uri.parse(memberList.get(position).getPhoto())));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} else {
			holder.ivContactImage.setImageDrawable(context.getResources().getDrawable(R.drawable.default_pic));
		}
		holder.ivDeleteContact.setVisibility(View.VISIBLE);
		holder.ivDeleteContact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (isValidate()) {
					new DBHelper(context).deleteMember(memberList.get(position).getId());
					memberList.remove(position);
					try{
				TextView tv=(TextView)activity.findViewById(R.id.tvNumberOfMembers);
				tv.setText(activity.getString(R.string.text_total_members) + memberList.size());
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					notifyDataSetChanged();
				}
			}
		});

		activity.setFontColor(holder.tvContact);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvContact);

		activity.setFontColor(holder.tvContactNumber);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvContactNumber);

		return convertView;
	}

	public Filter filterMember() {
		Filter filter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				AppLog.Log("filter", constraint + "");
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalMemberList;
					results.count = originalMemberList.size();
				} else {
					// We perform filtering operation
					List<Member> filteredList = new ArrayList<Member>();
					for (Member p : originalMemberList) {
						if (p.getName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
							filteredList.add(p);

					}
					results.values = filteredList;
					results.count = filteredList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				// Now we have to inform the adapter about the new list filtered
				// if (results.count == 0)
				// notifyDataSetInvalidated();
				// else {
				memberList = (ArrayList<Member>) results.values;
				notifyDataSetChanged();
				// }

			}

		};
		return filter;
	}

	private boolean isValidate() {
		String msg = null;

		if (isGroupCreated && memberList.size() <= 2) {
			msg = context.getString(R.string.text_min_2);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, context);
			return false;
		}

		return true;
	}
}
