package com.example.opex_new.adapter;

import java.util.ArrayList;
import java.util.List;

import com.androidquery.AQuery;
import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Group;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class GroupListAdapter extends BaseAdapter {

	private ArrayList<Group> groupsList, originalGroupList;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private AQuery aQuery;
	private boolean isDeletable;

	private GroupActivity activity;

	public GroupListAdapter(GroupActivity activity, ArrayList<Group> groupList, boolean isDeletable) {
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.groupsList = groupList;
		this.originalGroupList = groupList;
		aQuery = new AQuery(activity);
		this.activity = activity;
		this.isDeletable = isDeletable;

	}

	private class ViewHolder {
		TextView tvContact;
		ImageView ivGroupImage, ivDeleteGroup;
		LinearLayout llItemGroupList;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return groupsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return groupsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_group_list, parent, false);
			holder = new ViewHolder();
			holder.tvContact = (TextView) convertView.findViewById(R.id.tvContact);

			holder.ivGroupImage = (ImageView) convertView.findViewById(R.id.ivGroupImage);
			holder.llItemGroupList = (LinearLayout) convertView.findViewById(R.id.llItemGroupList);
			holder.ivDeleteGroup = (ImageView) convertView.findViewById(R.id.ivDeleteGroup);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		AppLog.Log("picture", "picture:" + groupsList.get(position).getPicture());
		holder.tvContact.setText(groupsList.get(position).getName());
		if (groupsList.get(position).getPicture() != null) {
			aQuery.id(holder.ivGroupImage).image(groupsList.get(position).getPicture());
		}

		if (groupsList.get(position).isSelected) {
			holder.llItemGroupList.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
		} else {
			holder.llItemGroupList.setBackgroundColor(activity.getResources().getColor(android.R.color.transparent));
		}

		if (!isDeletable) {
			holder.ivDeleteGroup.setVisibility(View.GONE);
		}

		holder.ivDeleteGroup.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				new AlertDialog.Builder(activity).setTitle(activity.getString(R.string.text_attention))
						.setMessage(activity.getString(R.string.text_delete_confirmation))
						.setPositiveButton(activity.getString(R.string.text_ok), new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								// TODO Auto-generated method stub
								new DBHelper(activity).deleteGroup(groupsList.get(position).getId());
								groupsList.remove(position);
								
								notifyDataSetChanged();
								dialog.dismiss();
							}
						}).setNegativeButton(activity.getString(R.string.text_cancel),
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog, int which) {
										// TODO Auto-generated method stub
										dialog.dismiss();
									}
								})
						.show();

			}
		});

		activity.setFontColor(holder.tvContact);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvContact);

		return convertView;
	}

	public Filter filterGroups() {
		Filter filter = new Filter() {

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				AppLog.Log("filter", constraint + "");
				// We implement here the filter logic
				if (constraint == null || constraint.length() == 0) {
					// No filter implemented we return all the list
					results.values = originalGroupList;
					results.count = originalGroupList.size();
				} else {
					// We perform filtering operation
					List<Group> filteredList = new ArrayList<Group>();
					for (Group p : originalGroupList) {
						if (p.getName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
							filteredList.add(p);

					}
					results.values = filteredList;
					results.count = filteredList.size();
				}
				return results;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {

				// Now we have to inform the adapter about the new list filtered
				// if (results.count == 0)
				// notifyDataSetInvalidated();
				// else {
				groupsList = (ArrayList<Group>) results.values;
				notifyDataSetChanged();
				// }

			}

		};
		return filter;
	}

}
