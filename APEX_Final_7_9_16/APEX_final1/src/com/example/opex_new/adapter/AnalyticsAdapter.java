package com.example.opex_new.adapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.model.Broadcast;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AnalyticsAdapter extends BaseAdapter {

	private ArrayList<Broadcast> broadcastList;
	private LayoutInflater inflater;
	private ViewHolder holder;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
	private GroupActivity activity;

	public AnalyticsAdapter(GroupActivity activity, ArrayList<Broadcast> broadcastList) {
		// TODO Auto-generated constructor stub
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.broadcastList = broadcastList;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return broadcastList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.item_analytics, parent, false);
			holder = new ViewHolder();
			holder.tvAnalyticsId = (TextView) convertView.findViewById(R.id.tvAnalyticsId);
			holder.tvAnalytics = (TextView) convertView.findViewById(R.id.tvAnalyticsMsg);
			holder.tvAnalyticsTime = (TextView) convertView.findViewById(R.id.tvAnalyticsTime);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		holder.tvAnalyticsId.setText(broadcastList.get(position).getId() + "");
		holder.tvAnalytics.setText(broadcastList.get(position).getMessage());

		try {
			holder.tvAnalyticsTime.setText(timeFormat.format(dateFormat.parse(broadcastList.get(position).getTime())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		activity.setFontColor(holder.tvAnalytics);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvAnalytics);

		activity.setFontColor(holder.tvAnalyticsId);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvAnalyticsId);

		activity.setFontColor(holder.tvAnalyticsTime);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(holder.tvAnalyticsTime);

		return convertView;
	}

	private class ViewHolder {
		TextView tvAnalytics, tvAnalyticsId, tvAnalyticsTime;
	}
}
