package com.example.opex_new.Utils;

import com.example.opex_new.R;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;

public class PreferenceHelper {

	private SharedPreferences app_prefs;
	private final String USER_ID = "user_id";
	private final String EMAIL = "email";
	private final String PASSWORD = "password";
	private final String DEVICE_TOKEN = "device_token";
	private final String SESSION_TOKEN = "session_token";
	private final String USERNAME = "username";
	private final String PHOTO = "photo";
	private final String COLOR = "color";
	private final String FONTS = "fonts";
	private final String SIZE = "size";
	private final String STYLE = "style";
	private final String STYLE_FROM_GALLARY = "style-from_gallary";
	private final String BACKGROUNG = "backgroung";

	private Context context;

	public PreferenceHelper(Context context) {
		app_prefs = context.getSharedPreferences(Const.PREF_NAME, Context.MODE_PRIVATE);
		this.context = context;
	}

	public void putUserId(long userId) {
		Editor edit = app_prefs.edit();
		edit.putLong(USER_ID, userId);
		edit.commit();
	}

	public void putEmail(String email) {
		Editor edit = app_prefs.edit();
		edit.putString(EMAIL, email);
		edit.commit();
	}

	public String getEmail() {
		return app_prefs.getString(EMAIL, null);
	}

	public void putPassword(String password) {
		Editor edit = app_prefs.edit();
		edit.putString(PASSWORD, password);
		edit.commit();
	}

	public String getPassword() {
		return app_prefs.getString(PASSWORD, null);
	}

	public long getUserId() {
		return app_prefs.getLong(USER_ID, 0);

	}

	public void putDeviceToken(String deviceToken) {
		Editor edit = app_prefs.edit();
		edit.putString(DEVICE_TOKEN, deviceToken);
		edit.commit();
	}

	public String getDeviceToken() {
		return app_prefs.getString(DEVICE_TOKEN, null);

	}

	public void putSessionToken(String sessionToken) {
		Editor edit = app_prefs.edit();
		edit.putString(SESSION_TOKEN, sessionToken);
		edit.commit();
	}

	public String getSessionToken() {
		return app_prefs.getString(SESSION_TOKEN, null);

	}

	public void putUserName(String userName) {
		Editor edit = app_prefs.edit();
		edit.putString(USERNAME, userName);
		edit.commit();
	}

	public String getUserName() {
		return app_prefs.getString(USERNAME, null);

	}

	public void putUserPhoto(String photo) {
		Editor edit = app_prefs.edit();
		edit.putString(PHOTO, photo);
		edit.commit();
	}

	public String getUserPhoto() {
		return app_prefs.getString(PHOTO, null);

	}

	public void putColor(int color) {
		Editor edit = app_prefs.edit();
		edit.putInt(COLOR, color);
		edit.commit();
	}

	public int getColor() {
		return app_prefs.getInt(COLOR, context.getResources().getColor(R.color.twitter));

	}

	public void putFonts(String fonts) {
		Editor edit = app_prefs.edit();
		edit.putString(FONTS, fonts);
		edit.commit();
	}

	public String getFonts() {
		return app_prefs.getString(FONTS, "fonts/BATANG.ttf");
	}

	public void putSize(Float size) {
		Editor edit = app_prefs.edit();
		edit.putFloat(SIZE, size);
		edit.commit();
	}

	public float getSize() {
		return app_prefs.getFloat(SIZE, context.getResources().getDimension(R.dimen.size_10));
	}

	public void putStyle(int style) {
		Editor edit = app_prefs.edit();
		edit.putInt(STYLE, style);
		edit.commit();
	}

	public int getStyle() {
		return app_prefs.getInt(STYLE, Typeface.NORMAL);

	}

	public void putStyleFromGallary(String style) {
		Editor edit = app_prefs.edit();
		edit.putString(STYLE_FROM_GALLARY, style);
		edit.commit();
	}

	public String getStyleFromGallary() {
		return app_prefs.getString(STYLE_FROM_GALLARY, null);

	}

	public void putBackground(int backGround) {
		Editor edit = app_prefs.edit();
		edit.putInt(BACKGROUNG, backGround);
		edit.commit();
	}

	public int getBackground() {
		return app_prefs.getInt(BACKGROUNG, R.drawable.img2);
	}

	public void Logout() {
		// clearRequestData();
		// new DBHelper(context).deleteUser();
		putUserId(0);
		putSessionToken(null);
		// putSocialId(null);
		// putClientDestination(null);
		// putLoginBy(Const.MANUAL);
		app_prefs.edit().clear();
	}
}
