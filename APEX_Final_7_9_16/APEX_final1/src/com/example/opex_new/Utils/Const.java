package com.example.opex_new.Utils;

public class Const {
	public static final String TAG = "APEX";

	// temp key
	public static final String PLACES_AUTOCOMPLETE_API_KEY = "AIzaSyCqcD3tNdTc8uUpa8j1D4_BXUi1Vtr5QF0";

	// web services
	public class ServiceType {
		private static final String HOST_URL = "http://bluegamerzstudio.com/uberforx/public/";

		private static final String BASE_URL = HOST_URL + "provider/";
		private static final String BASE_URL_WALKER = HOST_URL + "walker/";

		public static final String LOGIN = BASE_URL + "login";
		public static final String REGISTER = BASE_URL + "register";
		public static final String SEND_OTP = BASE_URL + "verify";
		public static final String SET_PASSWORD = BASE_URL + "add_password";
		public static final String FORGET_PASSWORD = HOST_URL + "application/forgot-password";
		public static final String ADD_MEMBER = BASE_URL_WALKER + "add_member";
		public static final String GET_GROUPS = BASE_URL + "get_groups?";
		public static final String GET_TEMPLETS = BASE_URL + "get_templets?";

	}

	// prefname
	public static String PREF_NAME = "APEX";

	// fragments tag
	public static String FRAGMENT_SELECT_IMEGE = "FRAGMENT_SELECT_IMEGE";
	public static String FRAGMENT_GROUP_DETAIL = "FRAGMENT_GROUP_DETAIL";
	public static String FRAGMENT_CREATE_GROUP = "FRAGMENT_CREATE_GROUP";
	public static String FRAGMENT_VIEW_GROUP = "FRAGMENT_VIEW_GROUP";
	public static String FRAGMENT_TEMPLETE = "FRAGMENT_TEMPLETE";
	public static String FRAGMENT_ADD_TEMPLETE = "FRAGMENT_ADD_TEMPLETE";
	public static String FRAGMENT_CHANGE_TEMPLETE = "FRAGMENT_CHANGE_TEMPLETE";
	public static String FRAGMENT_CREATE_MESSAGE = "FRAGMENT_CREATE_MESSAGE";
	public static String FRAGMENT_SETTINGS = "FRAGMENT_SETTINGS";
	public static String FRAGMENT_AUTO_RELAY = "FRAGMENT_AUTO_RELAY";
	public static String FRAGMENT_WRITE_MESSAGE = "FRAGMENT_WRITE_MESSAGE";
	public static String FRAGMENT_REPORT_VIEW = "FRAGMENT_REPORT_VIEW";
	public static String FRAGMENT_ANALYTICS = "FRAGMENT_ANALYTICS";
	public static String FRAGMENT_ANALYTICS2 = "FRAGMENT_ANALYTICS2";
	public static String FRAGMENT_APPEARANCE = "FRAGMENT_APPEARANCE";
	public static String FRAGMENT_PROFILE = "FRAGMENT_PROFILE";
	public static String FRAGMENT_FEEDBACk = "FRAGMENT_FEEDBACk";
	public static String FRAGMENT_AUTORELAYLIST = "FRAGMENT_AUTORELAYLIST";
	public static String FRAGMENT_REPORT_HISTORY = "FRAGMENT_REPORT_HISTORY";
	public static String delete_polls = "delete_polls";

	// service codes
	public class ServiceCode {
		public static final int REGISTER = 1;
		public static final int LOGIN = 2;
		public static final int FORGET_PASSWORD = 3;
		public static final int ADD_MEMBER = 4;
		public static final int GET_GROUPS = 5;
		public static final int GET_TEMPLETS = 6;
		public static final int SEND_OTP = 7;
		public static final int SET_PASSWORD = 8;
	}

	// service parameters
	public class Params {
		public static final String EMAIL = "email";
		public static final String PASSWORD = "password";
		public static final String PHONE = "phone";
		// public static final String DEVICE_TOKEN = "device_token";
		public static final String ID = "id";
		public static final String TOKEN = "token";
		public static final String PICTURE = "picture";
		public static final String NAME = "name";
		public static final String GROUP_NAME = "group_name";
		public static final String GROUP = "group";
		public static final String MEMBER = "member";
		public static final String ADDED_BY = "added_by";
		public static final String DATA = "data";
		public static final String TEMPLET = "templet";
		public static final String OTP = "otp";
	}

	public static final String GROUP = "group";
	public static final String TEMPLETE = "templete";
	public static final String IS_REGISTER = "is_register";
	public static final String ID = "id";
	public static final String TOKEN = "token";
	public static final String POSITION = "position";
	public static final String USER = "user";
	public static final String ISFORMESSAGING = "IsForMessaging";
	public static final String ISBROADCAST = "isBroadcast";
	public static final String MSGSTATUS = "msgStatus";
	public static final String PHONENO = "phoneNo";
	public static final String MSG = "msg";

	public static final String SENDSMS = "sendSMS";
	public static final String DELIVEREDSMS = "deliveredSMS";
	public static final String SENDSMSFEEDBACK = "sendSMSFeedback";
	public static final String DELIVEREDSMSFEEDBACK = "deliveredSMSFeedback";
	public static final String BROADCASTPARTID = "broadcastPartId";
	public static final String FEEDBACKPARTID = "feedbackPartId";
	public static final String BROADCASTPART = "broadcastPart";
	public static final String FEEDBACKPART = "feedbackPart";
	public static final String FEEDBACKPARTLIST = "feedbackPartList";
	public static final String BROADCAST = "broadcast";
	public static final String FEEDBACK = "feedback";
	public static final String QUESTION = "question";
	public static final String CATEGORY = "category";

	public static final int CREATE_MESSAGE = 6;
	public static final int GROUPS = 1;
	public static final int TEMPLETES = 2;
	public static final int SETTIINGS = 3;
	public static final int AUTO_RELAY = 4;
	public static final int REPORT_VIEW = 5;

	// msgStatus
	public static final int NORMAL = 0;
	public static final int EXPRESS = 1;
	public static final int URGENT = 2;
	public static final int EXPRESSDELAY = 1 * 60 * 1000;
	// public static final int URGENTDELAY = 2;

	// general
	public static final int CHOOSE_PHOTO = 112;
	public static final int TAKE_PHOTO = 113;
	public static final String URL = "url";
	public static final int MAX_MSG_SIZE = 160;
	public static final int TEMPLATE_NAME_SIZE = 20;

	// feedback answers
	public static final int answerA = 1;
	public static final int answerB = 2;
	public static final int answerC = 3;
	public static final int answerD = 4;
	public static final int answerE = 5;

}
