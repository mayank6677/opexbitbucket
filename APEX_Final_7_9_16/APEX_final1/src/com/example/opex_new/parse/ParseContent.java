package com.example.opex_new.parse;

import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;
import com.example.opex_new.Utils.ReadFiles;

import android.app.Activity;
import android.text.TextUtils;

/**
 * @author Hardik A Bhalodi
 */
public class ParseContent {
	private Activity activity;
	private final String KEY_SUCCESS = "success";
	private final String KEY_ERROR = "error";
	private PreferenceHelper preferenceHelper;

	public ParseContent(Activity activity) {
		// TODO Auto-generated constructor stub
		this.activity = activity;
		preferenceHelper = new PreferenceHelper(activity);
	}

	public boolean isSuccessWithStoreId(String response) {
		// AppLog.Log(Const.TAG, response);
		if (TextUtils.isEmpty(response))
			return false;
		try {
			JSONObject jsonObject = new JSONObject(response);
			if (jsonObject.getBoolean(KEY_SUCCESS)) {
				// preferenceHelper.putUserId(jsonObject
				// .getString(Const.Params.ID));
				preferenceHelper.putSessionToken(jsonObject.getString(Const.Params.TOKEN));
				// preferenceHelper.putEmail(jsonObject
				// .optString(Const.Params.EMAIL));
				// preferenceHelper.putLoginBy(jsonObject
				// .getString(Const.Params.LOGIN_BY));
				// // preferenceHelper.putReferee(jsonObject
				// // .getInt(Const.Params.IS_REFEREE));
				// if (!preferenceHelper.getLoginBy().equalsIgnoreCase(
				// Const.MANUAL)) {
				// preferenceHelper.putSocialId(jsonObject
				// .getString(Const.Params.SOCIAL_UNIQUE_ID));
				// }

				return true;
			} else {
				AndyUtils.showToast(jsonObject.getString(KEY_ERROR), activity);
				// AndyUtils.showErrorToast(jsonObject.getInt(KEY_ERROR_CODE),
				// activity);
				return false;
				// AndyUtils.showToast(jsonObject.getString(KEY_ERROR),
				// activity);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	// public User parseUserAndStoreToDb(String response) {
	// User user = null;
	// try {
	// JSONObject jsonObject = new JSONObject(response);
	//
	// if (jsonObject.getBoolean(KEY_SUCCESS)) {
	// user = new User();
	// // DBHelper dbHelper = new DBHelper(activity);
	// // user.setUserId(jsonObject.getInt(Const.Params.ID));
	// user.setEmail(jsonObject.optString(Const.Params.EMAIL));
	// // user.setFname(jsonObject.getString(Const.Params.FIRSTNAME));
	// // user.setLname(jsonObject.getString(Const.Params.LAST_NAME));
	// //
	// // user.setAddress(jsonObject.getString(Const.Params.ADDRESS));
	// // user.setBio(jsonObject.getString(Const.Params.BIO));
	// // user.setZipcode(jsonObject.getString(Const.Params.ZIPCODE));
	// // user.setPicture(jsonObject.getString(Const.Params.PICTURE));
	// user.setContact(jsonObject.getString(Const.Params.PHONE));
	// // dbHelper.createUser(user);
	//
	// } else {
	// // AndyUtils.showToast(jsonObject.getString(KEY_ERROR),
	// // activity);
	//
	// }
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return user;
	// }

	// public boolean isSuccess(String response) {
	// if (TextUtils.isEmpty(response))
	// return false;
	// try {
	// AppLog.Log(Const.TAG, response);
	// JSONObject jsonObject = new JSONObject(response);
	// if (jsonObject.getBoolean(KEY_SUCCESS)) {
	// return true;
	// } else {
	// AndyUtils.showToast(jsonObject.getString(KEY_ERROR), activity);
	// // AndyUtils.showErrorToast(jsonObject.getInt(KEY_ERROR_CODE),
	// // activity);
	// return false;
	// }
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return false;
	// }

	public ArrayList<String> parseCountryCodes() {
		String response = "";
		ArrayList<String> list = new ArrayList<String>();
		try {
			response = ReadFiles.readRawFileAsString(activity, R.raw.countrycodes);

			JSONArray array = new JSONArray(response);
			for (int i = 0; i < array.length(); i++) {
				JSONObject object = array.getJSONObject(i);
				list.add(object.getString("phone-code") + " " + object.getString("name"));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}

}
