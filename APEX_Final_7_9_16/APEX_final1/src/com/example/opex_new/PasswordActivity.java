package com.example.opex_new;

import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.User;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class PasswordActivity extends ActionBarBaseActivity {

	private EditText etNewPassword, etReEnterPassWord;
	private Button btnSubmitPassword;
	private User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		actionBar.hide();
		setContentView(R.layout.activity_password);
		etNewPassword = (EditText) findViewById(R.id.etNewPassword);
		etReEnterPassWord = (EditText) findViewById(R.id.etReEnterPassWord);
		btnSubmitPassword = (Button) findViewById(R.id.btnSubmitPassword);
		btnSubmitPassword.setOnClickListener(this);
		user = (User) getIntent().getSerializableExtra(Const.USER);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		setFonts(btnSubmitPassword);
		// setFontSize(btnSubmitPassword);
	//	SetBackgroundColor(btnSubmitPassword);
	}

	// @Override
	// public void onTaskCompleted(String response, int serviceCode) {
	// // TODO Auto-generated method stub
	// AndyUtils.removeSimpleProgressDialog();
	// if (parseContent.isSuccess(response)) {
	//
	// if (getIntent().getBooleanExtra(Const.IS_REGISTER, false)) {
	// startActivity(new Intent(PasswordActivity.this,
	// DashBoardActivity.class));
	// } else {
	// startActivity(new Intent(PasswordActivity.this,
	// LoginActivity.class));
	// }
	// finish();
	//
	// }
	// }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (TextUtils.isEmpty(etNewPassword.getText())) {
			AndyUtils.showToast(getString(R.string.enter_pass), this);
		} else if (TextUtils.isEmpty(etReEnterPassWord.getText())) {
			AndyUtils.showToast(getString(R.string.enter_re_enter_pass), this);
		} else if (!etNewPassword.getText().toString().equals(etReEnterPassWord.getText().toString())) {
			AndyUtils.showToast(getString(R.string.enter_same_password), this);
		} else {
			setPasswod();
		}
	}

	private void setPasswod() {
		dbHelper.addPassword(user.getUserId(), etNewPassword.getText().toString());
		Intent startLoginActivity = new Intent(this, LoginActivity.class);
		startLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startLoginActivity);
		// AndyUtils.showSimpleProgressDialog(this);
		// HashMap<String, String> map = new HashMap<String, String>();
		// map.put(Const.URL, Const.ServiceType.SET_PASSWORD);
		// map.put(Const.Params.PASSWORD, etNewPassword.getText().toString());
		// map.put(Const.Params.ID, "" + getIntent().getIntExtra(Const.ID, 0));
		// map.put(Const.Params.TOKEN, getIntent().getStringExtra(Const.TOKEN));
		// new HttpRequester(this, map, Const.ServiceCode.SET_PASSWORD, this);
	}
}
