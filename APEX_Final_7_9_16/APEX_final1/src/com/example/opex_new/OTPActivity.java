package com.example.opex_new;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.User;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.view.View;
import android.widget.ImageView;

public class OTPActivity extends ActionBarBaseActivity {

	// private EditText etOtp;
	// private Button btnSubmitOTP;
	private User user;
	// private BroadcastReceiver sendReceiver, deliveredReceiver;
	// private String SEND = "send", DELIVERED = "delivered";
	// private PendingIntent sendPendingIntent, deliveredPendingIntent;
	private OTPVerificationReceiver otpVerificationReceiver;
	private ImageView ivOTP;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		actionBar.hide();

		setContentView(R.layout.activity_otp);
		// etOtp = (EditText) findViewById(R.id.etOtp);
		// btnSubmitOTP = (Button) findViewById(R.id.btnSubmitOTP);
		// btnSubmitOTP.setOnClickListener(this);
		user = (User) getIntent().getSerializableExtra(Const.USER);
		otpVerificationReceiver = new OTPVerificationReceiver();
		ivOTP = (ImageView) findViewById(R.id.ivOTP);
		// AndyUtils.hideKeyboard(this, ivOTP);

		registerOTPReceiver();
		// unRegisterAutoRelayReceiver();
		// sendReceiver = new BroadcastReceiver() {
		//
		// @Override
		// public void onReceive(Context context, Intent intent) {
		// // TODO Auto-generated method stub
		// AppLog.Log("sendReceiver", "sendReceiver");
		// // AndyUtils.showToast("sendReceiver", OTPActivity.this);
		// switch (getResultCode()) {
		// case Activity.RESULT_OK:
		// AppLog.Log("sendReceiver", "RESULT_OK");
		// AndyUtils.showToast("sendReceiver RESULT_OK",
		// OTPActivity.this);
		// break;
		// case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
		// AppLog.Log("sendReceiver", "RESULT_ERROR_GENERIC_FAILURE");
		// AndyUtils.showToast(
		// "sendReceiver RESULT_ERROR_GENERIC_FAILURE",
		// OTPActivity.this);
		// break;
		// case SmsManager.RESULT_ERROR_NO_SERVICE:
		// AppLog.Log("sendReceiver", "RESULT_ERROR_NO_SERVICE");
		// AndyUtils.showToast("sendReceiver RESULT_ERROR_NO_SERVICE",
		// OTPActivity.this);
		// break;
		// case SmsManager.RESULT_ERROR_NULL_PDU:
		// AppLog.Log("sendReceiver", "RESULT_ERROR_NULL_PDU");
		// AndyUtils.showToast("sendReceiver RESULT_ERROR_NULL_PDU",
		// OTPActivity.this);
		// break;
		// case SmsManager.RESULT_ERROR_RADIO_OFF:
		// AppLog.Log("sendReceiver", "RESULT_ERROR_RADIO_OFF");
		// AndyUtils.showToast("sendReceiver RESULT_ERROR_RADIO_OFF",
		// OTPActivity.this);
		// break;
		// default:
		// break;
		// }
		// }
		// };
		//
		// deliveredReceiver = new BroadcastReceiver() {
		//
		// @Override
		// public void onReceive(Context context, Intent intent) {
		// // TODO Auto-generated method stub
		// AppLog.Log("deliveredReceiver", "deliveredReceiver");
		// AndyUtils.showToast("deliveredReceiver", OTPActivity.this);
		// switch (getResultCode()) {
		// case Activity.RESULT_OK:
		// AppLog.Log("deliveredReceiver", "RESULT_OK");
		// AndyUtils.showToast("deliveredReceiver RESULT_OK",
		// OTPActivity.this);
		// break;
		// case Activity.RESULT_CANCELED:
		// AppLog.Log("deliveredReceiver", "RESULT_CANCELED");
		// AndyUtils.showToast("deliveredReceiver RESULT_CANCELED",
		// OTPActivity.this);
		// break;
		// default:
		// break;
		// }
		// }
		// };
		// Intent sendIntent = new Intent(SEND);
		// Intent deliveredIntent = new Intent(DELIVERED);
		// sendPendingIntent = PendingIntent.getBroadcast(this, 0, sendIntent,
		// 0);
		// deliveredPendingIntent = PendingIntent.getBroadcast(this, 0,
		// deliveredIntent, 0);
		// AppLog.Log(Const.TAG, "contact:" + user.getContact());
		sendSMS(user.getContact(), user.getOtp() + " " + getString(R.string.text_otp_message), null, null);
		// AppLog.Log("", "otp=====>" + user.getOtp());
		// registerReceiver(sendReceiver, new IntentFilter(SEND));
		// registerReceiver(deliveredReceiver, new IntentFilter(DELIVERED));

	}


	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		unRegisterOTPReceiver();
		// unregisterReceiver(sendReceiver);
		// unregisterReceiver(deliveredReceiver);
	}

	// @Override
	// public void onTaskCompleted(String response, int serviceCode) {t
	// // TODO Auto-generated method stub
	// AndyUtils.removeSimpleProgressDialog();
	// if (parseContent.isSuccess(response)) {
	// gotoPasswordActivity(this,
	// getIntent().getBooleanExtra(Const.IS_REGISTER, false),
	// getIntent().getIntExtra(Const.ID, 0), getIntent()
	// .getStringExtra(Const.TOKEN));
	// // startActivity(new Intent(OTPActivity.this,
	// // PasswordActivity.class));
	// // finish();
	// } else {
	// Toast.makeText(this, getString(R.string.text_otp_wrong),
	// Toast.LENGTH_LONG).show();
	// }
	// }

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// if (TextUtils.isEmpty(etOtp.getText().toString())) {
		// AndyUtils.showToast(getString(R.string.toast_enter_otp), this);
		// } else if (user.getOtp() == Integer
		// .parseInt(etOtp.getText().toString())) {
		// AppLog.Log("", "id otp" + user.getUserId());
		// Intent intent = new Intent(OTPActivity.this, PasswordActivity.class);
		// intent.putExtra(Const.USER, user);
		// startActivity(intent);
		// finish();
		// } else {
		// AndyUtils.showToast(getString(R.string.toast_invalid_otp), this);
		// }

		// AndyUtils.showSimpleProgressDialog(this);
		// HashMap<String, String> map = new HashMap<String, String>();
		// map.put(Const.URL, Const.ServiceType.SEND_OTP);
		// map.put(Const.Params.OTP, etOtp.getText().toString());
		// map.put(Const.Params.ID, "" + getIntent().getIntExtra(Const.ID, 0));
		// map.put(Const.Params.TOKEN, getIntent().getStringExtra(Const.TOKEN));
		// new HttpRequester(this, map, Const.ServiceCode.SEND_OTP, this);
	}

	private void registerOTPReceiver() {
		AppLog.Log(Const.TAG, "registerOTPReceiver");
		IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
		registerReceiver(otpVerificationReceiver, intentFilter);
	}

	private void unRegisterOTPReceiver() {
		AppLog.Log(Const.TAG, "unRegisterOTPReceiver");
		unregisterReceiver(otpVerificationReceiver);

	}

	public class OTPVerificationReceiver extends BroadcastReceiver {

		// private ActionBarBaseActivity activity;
		private Bundle bundle;
		private String OTPFromMsg;
		private SmsMessage message = null;
		private StringBuilder text;
		private Object[] pdus;

		// private User user;

		// public OTPVerificationReceiver(ActionBarBaseActivity activity, User
		// user) {
		// // TODO Auto-generated constructor stub
		// this.activity = activity;
		// this.user = user;
		// text = new StringBuilder();
		// }

		public OTPVerificationReceiver() {
			text = new StringBuilder();
		}

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub

			bundle = intent.getExtras();

			AppLog.Log(Const.TAG, "onReceive");
			if (bundle != null) {
				pdus = (Object[]) bundle.get("pdus");
				for (int i = 0; i < pdus.length; i++) {
					message = SmsMessage.createFromPdu((byte[]) pdus[i]);
					text.append(message.getDisplayMessageBody());
					OTPFromMsg = text.toString().split(" ")[0];

					AppLog.Log(Const.TAG, "OTPFromMsg" + OTPFromMsg);
					AppLog.Log(Const.TAG, "otp" + user.getOtp());
					if (Integer.parseInt(OTPFromMsg) == user.getOtp()) {
						Intent otpIntent = new Intent(OTPActivity.this, PasswordActivity.class);
						otpIntent.putExtra(Const.USER, user);
						startActivity(otpIntent);
						finish();
					}
				}
			}
		}

	}

}
