package com.example.opex_new;


import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import android.widget.TimePicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.BroadcastPart;
import com.example.opex_new.model.Feedback;
import com.example.opex_new.model.FeedbackPart;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;
import com.example.opex_new.parse.ParseContent;
import com.example.opex_new.service_receiver.SMSIntentService;
import com.example.opex_new.service_receiver.SchedualMessageFeedbackReceiver;
import com.example.opex_new.service_receiver.SchedualMessageReceiver;




public abstract class ActionBarBaseActivity extends ActionBarActivity implements OnClickListener {

	public ActionBar actionBar;
	public TextView tvTitle;
	public ImageView ivBack;
	private int mFragmentId = 0;
	private String mFragmentTag = null;
	public PreferenceHelper preferenceHelper;
	public ParseContent parseContent;
	public RequestQueue requestQueue;
	public DBHelper dbHelper;
	// private AutoRelayReceiver autoRelayReceiver;
	private String SEND = "send", DELIVERED = "delivered";
	private BroadcastReceiver sendReceiver, deliveredReceiver;
	public PendingIntent sendPendingIntent, deliveredPendingIntent;
	public LinearLayout relMain;
	private Intent sendIntent, deliveredIntent;
	private long Id;
	public Dialog countryCodeDialog, schedualMessageDialog;
	private ListView lvCountryCode;
	private TextView tvCountryCode;
	public ArrayList<String> list;
	private ArrayAdapter<String> adapter;
	private DatePicker dpSchedualMsg;
	private TimePicker tpSchedualMsg;
	private Button btnOkSchedual;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		actionBar = getSupportActionBar();
		preferenceHelper = new PreferenceHelper(this);
		parseContent = new ParseContent(this);
		requestQueue = Volley.newRequestQueue(this);
		dbHelper = new DBHelper(this);
		// autoRelayReceiver = new AutoRelayReceiver(this);

		LayoutInflater inflater = (LayoutInflater) actionBar.getThemedContext()
				.getSystemService(LAYOUT_INFLATER_SERVICE);
		View customActionBarView = inflater.inflate(R.layout.custom_action_bar, null);
		tvTitle = (TextView) customActionBarView.findViewById(R.id.tvTitle);
		ivBack = (ImageView) customActionBarView.findViewById(R.id.ivBack);
		relMain = (LinearLayout) customActionBarView.findViewById(R.id.relMain);
		ivBack.setOnClickListener(this);
		try {
			actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
					ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
			actionBar.setCustomView(customActionBarView, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
					ViewGroup.LayoutParams.MATCH_PARENT));
		} catch (Exception e) {
			e.printStackTrace();
		}
		// registerAutoRelayReceiver();

		// make an intent service and register this receivers from there also
		// check broadcastId
		sendReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				AppLog.Log("sendReceiver activity", "sendReceiver");
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					AppLog.Log("sendReceiver", "RESULT_OK" + intent.getLongExtra(SEND, -1));
					// Id = intent.getLongExtra(SEND, -1);
					// if (intent.getBooleanExtra(Const.ISBROADCAST, true)) {
					// AppLog.Log(Const.TAG, "broadcastId onreceived" + Id);
					// dbHelper.addSent(Id);
					// } else {
					// AppLog.Log(Const.TAG, "feedbackId onreceived" + Id);
					// dbHelper.addSentFeedback(Id);
					// }

					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					AppLog.Log("sendReceiver", "RESULT_ERROR_GENERIC_FAILURE");
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					AppLog.Log("sendReceiver", "RESULT_ERROR_NO_SERVICE");
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					AppLog.Log("sen" + "" + "dReceiver", "RESULT_ERROR_NULL_PDU");
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					AppLog.Log("sendReceiver", "RESULT_ERROR_RADIO_OFF");
					break;
				default:
					break;
				}
			}
		};

		deliveredReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				AppLog.Log("deliveredReceiver activity", "deliveredReceiver");
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					AppLog.Log("deliveredReceiver", "RESULT_OK" + intent.getLongExtra(DELIVERED, -1));

					// Id = intent.getLongExtra(DELIVERED, -1);
					// if (intent.getBooleanExtra(Const.ISBROADCAST, true)) {
					// AppLog.Log(Const.TAG, "broadcastId onreceived" + Id);
					// dbHelper.addDeliverd(Id);
					// } else {
					// AppLog.Log(Const.TAG, "feedbackId onreceived" + Id);
					// dbHelper.addDeliverdFeedback(Id);
					// }

					break;
				case Activity.RESULT_CANCELED:
					AppLog.Log("deliveredReceiver", "RESULT_CANCELED");
					break;
				default:
					break;
				}
			}
		};
		// registerSMSReceiver();
		// startSMSIntentService();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	//	SetBackgroundColor(relMain);
		setFonts(tvTitle);
		// setFontSize(tvTitle);
		// registerSMSReceiver();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// unRegisterSMSReceiver();
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		// unRegisterAutoRelayReceiver();
		// unRegisterSMSReceiver();
	}

	public void setActionBarTitle(String str) {
		AppLog.Log(Const.TAG, "tvTitle" + tvTitle);
		AppLog.Log(Const.TAG, "str:" + str);
		tvTitle.setText(str);
	}

	public void addFragment(Fragment fragment, boolean addToBackStack, String tag) {
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();
		if (addToBackStack) {
			ft.addToBackStack(tag);
		}
		ft.replace(R.id.content_frame, fragment, tag);
		ft.commitAllowingStateLoss();
	}

	public void removeAllFragment(Fragment replaceFragment, boolean addToBackStack, String tag) {
		FragmentManager manager = getSupportFragmentManager();
		FragmentTransaction ft = manager.beginTransaction();

		manager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		if (addToBackStack) {
			ft.addToBackStack(tag);
		}
		ft.replace(R.id.content_frame, replaceFragment);
		ft.commit();
	}

	public void removeThisFragment() {
		FragmentManager manager = getSupportFragmentManager();
		manager.popBackStackImmediate();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Fragment fragment = null;

		if (mFragmentId > 0) {
			fragment = getSupportFragmentManager().findFragmentById(mFragmentId);
		} else if (mFragmentTag != null && !mFragmentTag.equalsIgnoreCase("")) {
			fragment = getSupportFragmentManager().findFragmentByTag(mFragmentTag);
		}
		if (fragment != null) {
			fragment.onActivityResult(requestCode, resultCode, data);
		}
	}

	public void startActivityForResult(Intent intent, int requestCode, String fragmentTag) {
		mFragmentTag = fragmentTag;
		mFragmentId = 0;
		super.startActivityForResult(intent, requestCode);
	}

	public void setFbTag(String tag) {
		mFragmentId = 0;
		mFragmentTag = tag;
	}

	public void gotoOtpActivity(ActionBarBaseActivity activity, boolean isAlreadyRegister, int id, String token) {
		Intent intent = new Intent(activity, OTPActivity.class);
		intent.putExtra(Const.IS_REGISTER, isAlreadyRegister);
		intent.putExtra(Const.ID, id);
		intent.putExtra(Const.TOKEN, token);
		startActivity(intent);
		activity.finish();
	}

	public void gotoPasswordActivity(ActionBarBaseActivity activity, boolean isAlreadyRegister, int id, String token) {
		Intent intent = new Intent(activity, PasswordActivity.class);
		intent.putExtra(Const.IS_REGISTER, isAlreadyRegister);
		intent.putExtra(Const.ID, id);
		intent.putExtra(Const.TOKEN, token);
		startActivity(intent);
		activity.finish();
	}

	public void sendSMS(final String phoneNo, final String message, final PendingIntent sendPendingIntent,
			final PendingIntent deliveredPendingIntent) {
		AppLog.Log("", "sendSMS");
		// AndyUtils.showSimpleProgressDialog(this);
		SmsManager smsManager = SmsManager.getDefault();
		try {
			// AppLog.Log("", "try");

			if (message.length() > 160) {
				ArrayList<String> messageList = smsManager.divideMessage(message);
				for (int i = 0; i < messageList.size(); i++) {
					smsManager.sendTextMessage(phoneNo, null, messageList.get(i), sendPendingIntent,
							deliveredPendingIntent);
				}
			} else {
				smsManager.sendTextMessage(phoneNo, null, message, sendPendingIntent, deliveredPendingIntent);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	// public class SendMessageAsyncTask extends AsyncTask<Object, Void, Void> {
	//
	// ArrayList<Member> memberList;
	// final String message;
	// final PendingIntent sendPendingIntent;
	// final PendingIntent deliveredPendingIntent;
	//
	// public SendMessageAsyncTask(ArrayList<Member> memberList,
	// final String message, final PendingIntent sendPendingIntent,
	// final PendingIntent deliveredPendingIntent) {
	// // TODO Auto-generated constructor stub
	// this.memberList = memberList;
	// this.message = message;
	// this.sendPendingIntent = sendPendingIntent;
	// this.deliveredPendingIntent = deliveredPendingIntent;
	// }
	//
	// @Override
	// protected Void doInBackground(Object... params) {
	// // TODO Auto-generated method stub
	//
	// for (int i = 0; i < memberList.size(); i++) {
	// sendSMS(memberList.get(i).getPhone(), message,
	// sendPendingIntent, deliveredPendingIntent);
	// }
	// return null;
	// }
	// }

	public void sendMultipleSMS(ArrayList<Member> memberList, final String message,
			final PendingIntent sendPendingIntent, final PendingIntent deliveredPendingIntent) {
		AppLog.Log("", "sendMultipleSMS");
		// for (int i = 0; i < memberList.size(); i++) {
		// try {
		// sendSMS(memberList.get(i).getPhone(), message,
		// sendPendingIntent, deliveredPendingIntent);
		// Thread.sleep(3000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }

		new SendMessageAsyncTask(memberList, message, sendPendingIntent, deliveredPendingIntent).execute();
	}

	public long[] addBroadcast(Group selectedGroup, String msg, long[] ids, int msgStatus) {
		Broadcast broadcast = new Broadcast();
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String time = formatDateTime(date);
		broadcast.setTime(time);
		broadcast.setGroup(selectedGroup);
		broadcast.setMsgStatus(msgStatus);
		broadcast.setMessage(msg);
		ArrayList<BroadcastPart> broadcastPartList = new ArrayList<BroadcastPart>();
		for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
			BroadcastPart broadcastPart = new BroadcastPart();
			broadcastPart.setMsg(msg);
			broadcastPart.setTime(time);
			broadcastPart.setMember(selectedGroup.getMemberList().get(i));
			broadcastPartList.add(broadcastPart);
		}
		broadcast.setBroadcastPartList(broadcastPartList);
		return dbHelper.addBroadcast(broadcast, ids);
	}

	public long[] addFeedback(Group selectedGroup, /* String msg, */
			long[] ids, int msgStatus) {
		Feedback feedback = new Feedback();
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();
		String time = formatDateTime(date);
		feedback.setTime(time);
		feedback.setGroup(selectedGroup);
		feedback.setMsgStatus(msgStatus);
		ArrayList<FeedbackPart> feedbackPartList = new ArrayList<FeedbackPart>();
		for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
			FeedbackPart feedbackPart = new FeedbackPart();
			// feedbackPart.setMsg(msg);
			feedbackPart.setMsgStatus(msgStatus);
			feedbackPart.setTime(time);
			feedbackPart.setMember(selectedGroup.getMemberList().get(i));
			feedbackPartList.add(feedbackPart);
		}
		feedback.setFeedbackPartList(feedbackPartList);
		return dbHelper.addFeedback(feedback, ids);
	}

	public Broadcast getSchedualedBroadcast(Group selectedGroup, String msg, int msgStatus, Date date) {
		Broadcast broadcast = new Broadcast();
		// Calendar calendar = Calendar.getInstance();
		// Date date = calendar.getTime();
		String time = formatDateTime(date);
		broadcast.setTime(time);
		broadcast.setGroup(selectedGroup);
		broadcast.setMsgStatus(msgStatus);
		broadcast.setMessage(msg);
		ArrayList<BroadcastPart> broadcastPartList = new ArrayList<BroadcastPart>();
		for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
			BroadcastPart broadcastPart = new BroadcastPart();
			broadcastPart.setMsg(msg);
			broadcastPart.setTime(time);
			broadcastPart.setMember(selectedGroup.getMemberList().get(i));
			broadcastPartList.add(broadcastPart);
		}
		broadcast.setBroadcastPartList(broadcastPartList);
		return broadcast;
	}

	public long[] addSchedualFeedback(Group selectedGroup, long[] ids, int msgStatus, Date date) {
		Feedback feedback = new Feedback();
		// Calendar calendar = Calendar.getInstance();
		// Date date = calendar.getTime();
		String time = formatDateTime(date);
		feedback.setTime(time);
		feedback.setGroup(selectedGroup);
		feedback.setMsgStatus(msgStatus);
		ArrayList<FeedbackPart> feedbackPartList = new ArrayList<FeedbackPart>();
		for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
			FeedbackPart feedbackPart = new FeedbackPart();
			// feedbackPart.setMsg(msg);
			feedbackPart.setMsgStatus(msgStatus);
			feedbackPart.setTime(time);
			feedbackPart.setMember(selectedGroup.getMemberList().get(i));
			feedbackPartList.add(feedbackPart);
		}
		feedback.setFeedbackPartList(feedbackPartList);

		AppLog.Log(Const.TAG, "addSchedualFeedback" + date);
		return dbHelper.addFeedback(feedback, ids);
	}

	public String formatDateTime(Date date) {
		return dateFormat.format(date);
	}

	public Date parseDateTime(String date) {
		try {
			return dateFormat.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public int generateOtp() {
		return (int) (Math.random() * 1000);
	}

	// protected void sendMsg(Context context, SmsMessage smsMessage) {
	// SmsManager smsMgr = SmsManager.getDefault();
	// ArrayList<string> smsMessageText = smsMgr.divideMessage(smsMessage
	// .getMsgBody());
	// PendingIntent sentPI = PendingIntent.getBroadcast(context, 0,
	// new Intent("SMS_SENT"), 0);
	// PendingIntent deliveredPI = PendingIntent.getBroadcast(context, 0,
	// new Intent("SMS_DELIVERED"), 0);
	// int AddresseesPerMessage = 10;
	// StringBuilder builder = new StringBuilder();
	// String delim = "";
	// for (ContactItem c : smsMessage.getAddresseeList()) {
	// // For every phone number in our list
	// builder.append(delim).append(c.getPhoneNumber().toString());
	// delim = ";";
	// if (((smsMessage.getAddresseeList().indexOf(c) + 1) %
	// AddresseesPerMessage) == 0
	// || smsMessage.getAddresseeList().indexOf(c) + 1 == smsMessage
	// .getAddresseeList().size()) {
	// // using +1 because index 0 mod 9 == 0
	// for (String text : smsMessageText) {
	// // Send 160 bytes of the total message until all parts are
	// // sent
	// smsMgr.sendTextMessage(builder.toString(), null, text,
	// sentPI, deliveredPI);
	// }
	// builder.setLength(0);
	// delim = "";
	// }
	// }
	// }

	// public void registerAutoRelayReceiver() {
	// AppLog.Log(Const.TAG, "registerAutoRelayReceiver");
	// IntentFilter intentFilter = new IntentFilter(
	// "android.provider.Telephony.SMS_RECEIVED");
	// registerReceiver(autoRelayReceiver, intentFilter);
	// }
	//
	// public void unRegisterAutoRelayReceiver() {
	// AppLog.Log(Const.TAG, "unRegisterAutoRelayReceiver");
	// try {
	// unregisterReceiver(autoRelayReceiver);
	// } catch (IllegalArgumentException e) {
	// e.printStackTrace();
	// }
	//
	// }

	public String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);

		if (cursor == null) { // Source is Dropbox or other similar local file
			// path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}

	// public void registerSMSReceiver() {
	// AppLog.Log("registerSMSReceiver", "registerSMSReceiver");
	// registerReceiver(sendReceiver, new IntentFilter(Const.SENDSMS));
	// registerReceiver(deliveredReceiver,
	// new IntentFilter(Const.DELIVEREDSMS));
	// }
	//
	// public void unRegisterSMSReceiver() {
	// AppLog.Log("unRegisterSMSReceiver", "unRegisterSMSReceiver");
	// unregisterReceiver(sendReceiver);
	// unregisterReceiver(deliveredReceiver);
	// }

	public void SetBackgroundColor(View view) {
		view.setBackgroundColor(preferenceHelper.getColor());
	}

	public void setFontColor(Button button) {
		button.setTextColor(preferenceHelper.getColor());
	}

	public void setFontColor(TextView textView) {
		textView.setTextColor(preferenceHelper.getColor());
	}

	// public void setBackGround(View view) {
	// if (preferenceHelper.getStyleFromGallary() != null) {
	// BitmapDrawable drawable = new BitmapDrawable(getResources(),
	// preferenceHelper.getStyleFromGallary());
	// view.setBackgroundDrawable(drawable);
	// } else {
	// view.setBackgroundDrawable(getResources().getDrawable(
	// preferenceHelper.getStyle()));
	// }
	//
	// }

	public void setFonts(TextView textView) {
		textView.setTypeface(Typeface.createFromAsset(getAssets(), preferenceHelper.getFonts()),
				preferenceHelper.getStyle());
		textView.setText(textView.getText().toString().trim());
		if (!textView.getText().toString().endsWith(" ") && (preferenceHelper.getStyle() == Typeface.BOLD_ITALIC
				|| preferenceHelper.getStyle() == Typeface.ITALIC)) {
			textView.append(" ");
		}
	}

	public void setFonts(Button button) {
		button.setTypeface(Typeface.createFromAsset(getAssets(), preferenceHelper.getFonts()),
				preferenceHelper.getStyle());
		if (!button.getText().toString().endsWith(" ") && (preferenceHelper.getStyle() == Typeface.BOLD_ITALIC
				|| preferenceHelper.getStyle() == Typeface.ITALIC)) {
			button.append(" ");
		}
	}

	public void setFonts(EditText editText) {
		editText.setTypeface(Typeface.createFromAsset(getAssets(), preferenceHelper.getFonts()),
				preferenceHelper.getStyle());
	}

	public void setBackground(View view) {
		// view.setBackground(getResources().getDrawable(
		// preferenceHelper.getBackground()));

		view.setBackgroundResource(preferenceHelper.getBackground());
	}

	// public void setFontSize(TextView textView) {
	// textView.setTextSize(preferenceHelper.getSize());
	//
	// }
	//
	// public void setFontSize(Button button) {
	// button.setTextSize(preferenceHelper.getSize());
	//
	// }
	//
	// public void setFontSize(EditText editText) {
	// editText.setTextSize(preferenceHelper.getSize());
	// }

	public void setSendDelvereIntents(boolean isBroadcast, long broadcastId, long broadcastPartId, int msgStatus,
			String phoneNo, String msg) {
		AppLog.Log(Const.TAG, "setSendDelvereIntents ID: " + broadcastId);
		Intent sendIntent = new Intent(Const.SENDSMS);
		sendIntent.putExtra(Const.SENDSMS, broadcastId);
		sendIntent.putExtra(Const.ISBROADCAST, isBroadcast);
		sendIntent.putExtra(Const.BROADCASTPARTID, broadcastPartId);
		sendIntent.putExtra(Const.MSGSTATUS, msgStatus);
		sendIntent.putExtra(Const.PHONENO, phoneNo);
		sendIntent.putExtra(Const.MSG, msg);
		Intent deliveredIntent = new Intent(Const.DELIVEREDSMS);
		deliveredIntent.putExtra(Const.DELIVEREDSMS, broadcastId);
		deliveredIntent.putExtra(Const.ISBROADCAST, isBroadcast);
		deliveredIntent.putExtra(Const.BROADCASTPARTID, broadcastPartId);
		deliveredIntent.putExtra(Const.MSGSTATUS, msgStatus);
		deliveredIntent.putExtra(Const.PHONENO, phoneNo);
		deliveredIntent.putExtra(Const.MSG, msg);

		AppLog.Log(Const.TAG, "msg" + msg);
		sendPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, sendIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		deliveredPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, deliveredIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public void setSendDelvereIntentsFeedback(long feedbackId, long feedbackPartId) {
		AppLog.Log(Const.TAG, "setSendDelvereIntents ID: " + feedbackId);
		Intent sendIntent = new Intent(Const.SENDSMSFEEDBACK);
		sendIntent.putExtra(Const.SENDSMSFEEDBACK, feedbackId);
		sendIntent.putExtra(Const.FEEDBACKPARTID, feedbackPartId);
		Intent deliveredIntent = new Intent(Const.DELIVEREDSMSFEEDBACK);
		deliveredIntent.putExtra(Const.DELIVEREDSMSFEEDBACK, feedbackId);
		deliveredIntent.putExtra(Const.FEEDBACKPARTID, feedbackPartId);
		sendPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, sendIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		deliveredPendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, deliveredIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public void showCountryCodeDialog(OnItemClickListener listener) {
		countryCodeDialog = new Dialog(this);
		countryCodeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		countryCodeDialog.setContentView(R.layout.dialog_country_code);

		list = parseContent.parseCountryCodes();
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
		adapter.addAll(list);
		lvCountryCode = (ListView) countryCodeDialog.findViewById(R.id.lvCountryCode);
		lvCountryCode.setAdapter(adapter);
		lvCountryCode.setOnItemClickListener(listener);
		tvCountryCode = (TextView) countryCodeDialog.findViewById(R.id.tvCountryCode);

		// setFontSize(tvCountryCode);
		setFonts(tvCountryCode);
		setFontColor(tvCountryCode);
		countryCodeDialog.show();
	}

	public void startSMSIntentService() {
		AppLog.Log(Const.TAG, "startSMSIntentService");
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(System.currentTimeMillis());
		Intent intent = new Intent(this, SMSIntentService.class);
		PendingIntent pintent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(pintent);
		alarm.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), 1000 * 60 * 1, pintent);

	}

	protected void stopSMSIntentService() {
		Intent intent = new Intent(this, SMSIntentService.class);
		PendingIntent pintent = PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		alarm.cancel(pintent);

	}

	public void schedualMessageBroadcast(boolean isBroadcast, Date date, Group selectedGroup, String msg, int msgStatus,
			long feedbackId) {
		AppLog.Log(Const.TAG, "startSMSIntentService");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		AppLog.Log(Const.TAG, "time:" + cal.getTime());
		// cal.setTimeInMillis(System.currentTimeMillis());
		Intent intent;
		if (isBroadcast) {
			intent = new Intent(this, SchedualMessageReceiver.class);
			Broadcast broadcast = getSchedualedBroadcast(selectedGroup, msg, msgStatus, date);
			intent.putExtra(Const.BROADCAST, broadcast);
		} else {
			intent = new Intent(this, SchedualMessageFeedbackReceiver.class);
			dbHelper.addTimeInFeedback(formatDateTime(date), feedbackId);
			Feedback feedback = dbHelper.getFeedback(feedbackId);
			intent.putExtra(Const.FEEDBACK, feedback);
		}
		PendingIntent pintent = PendingIntent.getBroadcast(getApplicationContext(), (int) cal.getTimeInMillis(), intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		AlarmManager alarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
		alarm.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), pintent);
	}

	public void showSchedualMessageDialog(final Boolean isTemplate, final Group selectedGroup, final String msg,
			final int msgStatus, final boolean isBroadcast, final long feedbackId) {
		if (schedualMessageDialog == null) {
			schedualMessageDialog = new Dialog(this);
			schedualMessageDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			schedualMessageDialog.setContentView(R.layout.dialog_schedual_message);
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(schedualMessageDialog.getWindow().getAttributes());
			lp.width = WindowManager.LayoutParams.MATCH_PARENT;
			lp.height = WindowManager.LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;
			schedualMessageDialog.getWindow().setAttributes(lp);
			dpSchedualMsg = (DatePicker) schedualMessageDialog.findViewById(R.id.dpSchedualMsg);
			tpSchedualMsg = (TimePicker) schedualMessageDialog.findViewById(R.id.tpSchedualMsg);
			btnOkSchedual = (Button) schedualMessageDialog.findViewById(R.id.btnOkSchedual);

			btnOkSchedual.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String date = dpSchedualMsg.getYear() + "-" + (dpSchedualMsg.getMonth() + 1) + "-"
							+ dpSchedualMsg.getDayOfMonth() + " " + tpSchedualMsg.getCurrentHour() + ":"
							+ tpSchedualMsg.getCurrentMinute() + ":00";
					AppLog.Log(Const.TAG, "date:" + date);
					schedualMessageBroadcast(isBroadcast, parseDateTime(date), selectedGroup, msg, msgStatus,
							feedbackId);
					schedualMessageDialog.dismiss();
					onBackPressed();
					if (isTemplate) {
						onBackPressed();
					}

					AppLog.Log(Const.TAG, "btnok" + date);

				}
			});

			schedualMessageDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					schedualMessageDialog = null;
				}
			});
			schedualMessageDialog.show();

		}
	}
}