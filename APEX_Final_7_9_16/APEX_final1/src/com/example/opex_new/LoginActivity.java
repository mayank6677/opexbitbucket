package com.example.opex_new;

import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.User;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

public class LoginActivity extends ActionBarBaseActivity {

	private EditText etEmail, etPassword;
	private Button btnSignIn, btnForgetPassword, btnRegisterNow;


    CheckBox show_hide_password;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
		actionBar.hide();
		if (preferenceHelper.getUserId() != 0) {
			startActivity(new Intent(LoginActivity.this, DashBoardActivity.class));
			finish();
		}

		etEmail = (EditText) findViewById(R.id.etEmail);
		etPassword = (EditText) findViewById(R.id.etPassword);
		btnSignIn = (Button) findViewById(R.id.btnSignInLogin);
		show_hide_password = (CheckBox) findViewById(R.id.showPassword);
		btnForgetPassword = (Button) findViewById(R.id.btnForgetPassword);
	
		btnSignIn.setOnClickListener(this);
		btnRegisterNow = (Button) findViewById(R.id.btnRegisterNow);
	
		btnRegisterNow.setOnClickListener(this);
		btnForgetPassword.setOnClickListener(this);
		
		  show_hide_pass();
    
		
		
	}

	 public void show_hide_pass(){
	        show_hide_password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
	            @Override
	            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
	                if (!b){
	                    // hide password
	                	etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

	                }else{
	                    // show password
	                	etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
	                }
	            }
	        });
	 }
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		setFonts(btnForgetPassword);
		// setFontSize(btnForgetPassword);
	//	SetBackgroundColor(btnForgetPassword);

		setFonts(btnSignIn);
		// setFontSize(btnSignIn);
		//SetBackgroundColor(btnSignIn);

		setFonts(btnRegisterNow);
		// setFontSize(btnRegisterNow);
		//SetBackgroundColor(btnRegisterNow);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnRegisterNow:
			startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
			break;

		case R.id.btnForgetPassword:
			forgetPassword();
			break;

		case R.id.btnSignInLogin:
			if (TextUtils.isEmpty(etEmail.getText())) {
				AndyUtils.showToast(getString(R.string.enter_user_name), this);
			} else if (TextUtils.isEmpty(etPassword.getText())) {
				AndyUtils.showToast(getString(R.string.enter_pass), this);
			} else {
				login();
			}
			break;

		default:
			break;
		}
	}

	private void forgetPassword() {
		Intent startLoginActivity = new Intent(this, ForgetPasswordActivity.class);
		// startLoginActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
		// | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startLoginActivity);
	}

	private void login() {
		User user = dbHelper.login(etEmail.getText().toString(), etPassword.getText().toString());
		if (user != null) {
			preferenceHelper.putUserId(user.getUserId());
			AppLog.Log(Const.TAG, "user id" + user.getUserId());
			preferenceHelper.putUserName(user.getName());
			Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
			startActivity(intent);
			finish();
		}
	}
}