package com.example.opex_new.service_receiver;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Feedback;
import com.example.opex_new.model.Group;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.telephony.SmsManager;

public class SchedualMessageFeedbackReceiver extends BroadcastReceiver {

	private Feedback feedback;
	private Group selectedGroup;
	private DBHelper dbHelper;
	private long[] ids = new long[101];
	public PendingIntent sendPendingIntent, deliveredPendingIntent;
	private Context context;
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		AppLog.Log(Const.TAG, "SchedualMessageReceiver onReceive");
		this.context = context;
		dbHelper = new DBHelper(context);
		feedback = (Feedback) intent.getSerializableExtra(Const.FEEDBACK);
		selectedGroup = feedback.getGroup();
		AppLog.Log(Const.TAG, "selectedGroup onReceive" + selectedGroup);
		Calendar cal = Calendar.getInstance();
		Date date = cal.getTime();

		dbHelper.addTimeInFeedbackPart(dateFormat.format(date), feedback.getId());

		// new SendMessageAsyncTask(selectedGroup.getMemberList(),
		// feedback.getMessgae(), sendPendingIntent,
		// deliveredPendingIntent);

		/*
		 * for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
		 * setSendDelvereIntentsFeedback(feedback.getId(), ids[i + 1]); try {
		 * sendSMS(selectedGroup.getMemberList().get(i).getPhone(),
		 * feedback.getMessgae(), sendPendingIntent, deliveredPendingIntent);
		 * Thread.sleep(3000); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); } }
		 */
		new SchedualFeedbackAsyncTask().execute();
	}

	public class SchedualFeedbackAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
				setSendDelvereIntentsFeedback(feedback.getId(), ids[i + 1]);
				try {
					sendSMS(selectedGroup.getMemberList().get(i).getPhone(), feedback.getMessgae(), sendPendingIntent,
							deliveredPendingIntent);
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return null;
		}
	}

	public void setSendDelvereIntentsFeedback(long feedbackId, long feedbackPartId) {
		AppLog.Log(Const.TAG, "setSendDelvereIntents ID: " + feedbackId);
		Intent sendIntent = new Intent(Const.SENDSMSFEEDBACK);
		sendIntent.putExtra(Const.SENDSMSFEEDBACK, feedbackId);
		sendIntent.putExtra(Const.FEEDBACKPARTID, feedbackPartId);
		Intent deliveredIntent = new Intent(Const.DELIVEREDSMSFEEDBACK);
		deliveredIntent.putExtra(Const.DELIVEREDSMSFEEDBACK, feedbackId);
		deliveredIntent.putExtra(Const.FEEDBACKPARTID, feedbackPartId);

		// AppLog.Log(Const.TAG, "msg" + msg);
		sendPendingIntent = PendingIntent.getBroadcast(context, 0, sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		deliveredPendingIntent = PendingIntent.getBroadcast(context, 0, deliveredIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public void sendSMS(final String phoneNo, final String message, final PendingIntent sendPendingIntent,
			final PendingIntent deliveredPendingIntent) {
		AppLog.Log("", "sendSMS");
		// AndyUtils.showSimpleProgressDialog(this);
		SmsManager smsManager = SmsManager.getDefault();
		try {
			AppLog.Log("", "try");

			if (message.length() > 160) {
				ArrayList<String> messageList = smsManager.divideMessage(message);
				for (int i = 0; i < messageList.size(); i++) {
					if (i == 0) {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), sendPendingIntent,
								deliveredPendingIntent);
					} else {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), null, null);
					}
				}
			} else {
				smsManager.sendTextMessage(phoneNo, null, message, sendPendingIntent, deliveredPendingIntent);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
