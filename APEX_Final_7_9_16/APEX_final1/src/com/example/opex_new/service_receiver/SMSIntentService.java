package com.example.opex_new.service_receiver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.FeedbackPart;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.AsyncTask;
import android.telephony.SmsManager;

public class SMSIntentService extends IntentService {

	private DBHelper dbHelper;
	private ArrayList<FeedbackPart> feedbackPartList;

	// public PendingIntent sendPendingIntent, deliveredPendingIntent;

	public SMSIntentService() {
		this("");
		
		// TODO Auto-generated constructor stub
	}

	public SMSIntentService(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// TODO Auto-generated method stub

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		AppLog.Log(Const.TAG, "onStartCommand");
		dbHelper = new DBHelper(getApplicationContext());
		feedbackPartList = new ArrayList<FeedbackPart>();
		dbHelper.getFeedbackPartList(feedbackPartList);

		new SMSIntentAsyncTask().execute();

		/*
		 * Calendar calendar = Calendar.getInstance(); Date date =
		 * calendar.getTime(); SimpleDateFormat dateFormat = new
		 * SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
		 * 
		 * for (int i = 0; i < feedbackPartList.size(); i++) { //
		 * AppLog.Log(Const.TAG, "feedbackPartList"); FeedbackPart feedbackPart
		 * = feedbackPartList.get(i);
		 * 
		 * // AppLog.Log(Const.TAG, // "feedbackPartList msg" +
		 * feedbackPart.getMsg()); if (feedbackPart.getMsgStatus() ==
		 * Const.URGENT) { AppLog.Log(Const.TAG, "urgent"); if
		 * (feedbackPart.getRetry() < 3) { // AppLog.Log(Const.TAG,
		 * "retry "+feedbackPart.getRetry()); try { // AppLog.Log(Const.TAG, //
		 * "feedback" + feedbackPart.getTime()); // AppLog.Log(Const.TAG,
		 * "current" + date.getTime()); long diff = (date.getTime() -
		 * dateFormat.parse( feedbackPart.getTime()).getTime()) / (60 * 1000);
		 * // AppLog.Log(Const.TAG, "diff" + diff); if (diff >= 10 && diff < 15)
		 * { // Intent sendIntent = new Intent( // Const.SENDSMSFEEDBACK); //
		 * sendIntent.putExtra(Const.SENDSMSFEEDBACK, //
		 * feedbackPart.getFeedbackId()); //
		 * sendIntent.putExtra(Const.FEEDBACKPARTID, // feedbackPart.getId());
		 * // Intent deliveredIntent = new Intent( //
		 * Const.DELIVEREDSMSFEEDBACK); // deliveredIntent.putExtra( //
		 * Const.DELIVEREDSMSFEEDBACK, // feedbackPart.getFeedbackId()); //
		 * deliveredIntent.putExtra(Const.FEEDBACKPARTID, //
		 * feedbackPart.getId()); // sendPendingIntent =
		 * PendingIntent.getBroadcast( // getApplicationContext(), 0,
		 * sendIntent, // PendingIntent.FLAG_UPDATE_CURRENT); //
		 * deliveredPendingIntent = PendingIntent //
		 * .getBroadcast(getApplicationContext(), 0, // deliveredIntent, //
		 * PendingIntent.FLAG_UPDATE_CURRENT);
		 * 
		 * try { sendSMS(feedbackPart.getMember().getPhone(),
		 * feedbackPart.getMsg(), null, null
		 * 
		 * sendPendingIntent, deliveredPendingIntent ); Thread.sleep(3000);
		 * dbHelper.retryFeedbackPart( feedbackPart.getId(),
		 * dateFormat.format(date)); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } } catch (ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * } } else if (feedbackPart.getMsgStatus() == Const.EXPRESS) {
		 * AppLog.Log(Const.TAG, "express" + feedbackPart.getRetry()); if
		 * (feedbackPart.getRetry() < 1) { // AppLog.Log(Const.TAG, "retry 1");
		 * try { long diff = (date.getTime() - dateFormat.parse(
		 * feedbackPart.getTime()).getTime()) / (60 * 1000); //
		 * AppLog.Log(Const.TAG, "diff" + diff); if (diff >= 20 && diff < 25) {
		 * // Intent sendIntent = new Intent( // Const.SENDSMSFEEDBACK); //
		 * sendIntent.putExtra(Const.SENDSMSFEEDBACK, //
		 * feedbackPart.getFeedbackId()); //
		 * sendIntent.putExtra(Const.FEEDBACKPARTID, // feedbackPart.getId());
		 * // Intent deliveredIntent = new Intent( //
		 * Const.DELIVEREDSMSFEEDBACK); // deliveredIntent.putExtra( //
		 * Const.DELIVEREDSMSFEEDBACK, // feedbackPart.getFeedbackId()); //
		 * deliveredIntent.putExtra(Const.FEEDBACKPARTID, //
		 * feedbackPart.getId()); // sendPendingIntent =
		 * PendingIntent.getBroadcast( // getApplicationContext(), 0,
		 * sendIntent, // PendingIntent.FLAG_UPDATE_CURRENT); //
		 * deliveredPendingIntent = PendingIntent //
		 * .getBroadcast(getApplicationContext(), 0, // deliveredIntent, //
		 * PendingIntent.FLAG_UPDATE_CURRENT);
		 * 
		 * try { // AppLog.Log(Const.TAG, // "" + feedbackPart.getMember()); //
		 * AppLog.Log(Const.TAG, "" // + feedbackPart.getMember().getPhone());
		 * // AppLog.Log(Const.TAG, // "" + feedbackPart.getMsg()); //
		 * AppLog.Log(Const.TAG, "" + // sendPendingIntent); //
		 * AppLog.Log(Const.TAG, "" // + deliveredPendingIntent);
		 * sendSMS(feedbackPart.getMember().getPhone(), feedbackPart.getMsg(),
		 * null, null
		 * 
		 * sendPendingIntent, deliveredPendingIntent ); Thread.sleep(3000);
		 * dbHelper.retryFeedbackPart( feedbackPart.getId(),
		 * dateFormat.format(date)); } catch (InterruptedException e) { // TODO
		 * Auto-generated catch block e.printStackTrace(); }
		 * 
		 * } } catch (ParseException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); }
		 * 
		 * } }
		 */
		// }
		return START_STICKY;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		AppLog.Log(Const.TAG, "onCreate smsintentService");
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		AppLog.Log(Const.TAG, "onDestroy smsIntentService");
	}

	public void sendSMS(final String phoneNo, final String message, final PendingIntent sendPendingIntent,
			final PendingIntent deliveredPendingIntent) {
		AppLog.Log("", "sendSMS");

		SmsManager smsManager = SmsManager.getDefault();
		try {
			AppLog.Log("", "try");

			if (message.length() > 160) {
				ArrayList<String> messageList = smsManager.divideMessage(message);
				for (int i = 0; i < messageList.size(); i++) {
					if (i == 0) {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), sendPendingIntent,
								deliveredPendingIntent);
					} else {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), null, null);
					}
				}
			} else {
				smsManager.sendTextMessage(phoneNo, null, message, sendPendingIntent, deliveredPendingIntent);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public class SMSIntentAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			Calendar calendar = Calendar.getInstance();
			Date date = calendar.getTime();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			for (int i = 0; i < feedbackPartList.size(); i++) {
				// AppLog.Log(Const.TAG, "feedbackPartList");
				FeedbackPart feedbackPart = feedbackPartList.get(i);

				// AppLog.Log(Const.TAG,
				// "feedbackPartList msg" + feedbackPart.getMsg());
				if (feedbackPart.getMsgStatus() == Const.URGENT) {
					AppLog.Log(Const.TAG, "urgent");
					if (feedbackPart.getRetry() < 3) {
						// AppLog.Log(Const.TAG,
						// "retry "+feedbackPart.getRetry());
						try {
							// AppLog.Log(Const.TAG,
							// "feedback" + feedbackPart.getTime());
							// AppLog.Log(Const.TAG, "current" +
							// date.getTime());
							long diff = (date.getTime() - dateFormat.parse(feedbackPart.getTime()).getTime())
									/ (60 * 1000);
							// AppLog.Log(Const.TAG, "diff" + diff);
							if (diff >= 10 && diff < 15) {
								// Intent sendIntent = new Intent(
								// Const.SENDSMSFEEDBACK);
								// sendIntent.putExtra(Const.SENDSMSFEEDBACK,
								// feedbackPart.getFeedbackId());
								// sendIntent.putExtra(Const.FEEDBACKPARTID,
								// feedbackPart.getId());
								// Intent deliveredIntent = new Intent(
								// Const.DELIVEREDSMSFEEDBACK);
								// deliveredIntent.putExtra(
								// Const.DELIVEREDSMSFEEDBACK,
								// feedbackPart.getFeedbackId());
								// deliveredIntent.putExtra(Const.FEEDBACKPARTID,
								// feedbackPart.getId());
								// sendPendingIntent =
								// PendingIntent.getBroadcast(
								// getApplicationContext(), 0, sendIntent,
								// PendingIntent.FLAG_UPDATE_CURRENT);
								// deliveredPendingIntent = PendingIntent
								// .getBroadcast(getApplicationContext(), 0,
								// deliveredIntent,
								// PendingIntent.FLAG_UPDATE_CURRENT);

								try {
									sendSMS(feedbackPart.getMember().getPhone(), feedbackPart.getMsg(), null, null
									/*
									 * sendPendingIntent, deliveredPendingIntent
									 */);
									Thread.sleep(3000);
									dbHelper.retryFeedbackPart(feedbackPart.getId(), dateFormat.format(date));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
				} else if (feedbackPart.getMsgStatus() == Const.EXPRESS) {
					AppLog.Log(Const.TAG, "express" + feedbackPart.getRetry());
					if (feedbackPart.getRetry() < 1) {
						// AppLog.Log(Const.TAG, "retry 1");
						try {
							long diff = (date.getTime() - dateFormat.parse(feedbackPart.getTime()).getTime())
									/ (60 * 1000);
							// AppLog.Log(Const.TAG, "diff" + diff);
							if (diff >= 20 && diff < 25) {
								// Intent sendIntent = new Intent(
								// Const.SENDSMSFEEDBACK);
								// sendIntent.putExtra(Const.SENDSMSFEEDBACK,
								// feedbackPart.getFeedbackId());
								// sendIntent.putExtra(Const.FEEDBACKPARTID,
								// feedbackPart.getId());
								// Intent deliveredIntent = new Intent(
								// Const.DELIVEREDSMSFEEDBACK);
								// deliveredIntent.putExtra(
								// Const.DELIVEREDSMSFEEDBACK,
								// feedbackPart.getFeedbackId());
								// deliveredIntent.putExtra(Const.FEEDBACKPARTID,
								// feedbackPart.getId());
								// sendPendingIntent =
								// PendingIntent.getBroadcast(
								// getApplicationContext(), 0, sendIntent,
								// PendingIntent.FLAG_UPDATE_CURRENT);
								// deliveredPendingIntent = PendingIntent
								// .getBroadcast(getApplicationContext(), 0,
								// deliveredIntent,
								// PendingIntent.FLAG_UPDATE_CURRENT);

								try {
									// AppLog.Log(Const.TAG,
									// "" + feedbackPart.getMember());
									// AppLog.Log(Const.TAG, ""
									// + feedbackPart.getMember().getPhone());
									// AppLog.Log(Const.TAG,
									// "" + feedbackPart.getMsg());
									// AppLog.Log(Const.TAG, "" +
									// sendPendingIntent);
									// AppLog.Log(Const.TAG, ""
									// + deliveredPendingIntent);
									sendSMS(feedbackPart.getMember().getPhone(), feedbackPart.getMsg(), null, null
									/*
									 * sendPendingIntent, deliveredPendingIntent
									 */);
									Thread.sleep(3000);
									dbHelper.retryFeedbackPart(feedbackPart.getId(), dateFormat.format(date));
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}

							}
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				}
			}
			return null;
		}

	}

}
