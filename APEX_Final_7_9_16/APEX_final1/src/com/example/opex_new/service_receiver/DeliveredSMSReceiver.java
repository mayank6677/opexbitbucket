package com.example.opex_new.service_receiver;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.db.DBHelper;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DeliveredSMSReceiver extends BroadcastReceiver {
	private DBHelper dbHelper;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		AppLog.Log("deliveredReceiver", "deliveredReceiver");
		dbHelper = new DBHelper(context);
		long BroadcattId = intent.getLongExtra(Const.DELIVEREDSMS, -1);
		long BroadcattPartId = intent.getLongExtra(Const.BROADCASTPARTID, -1);
		// int msgStatus = intent.getIntExtra(Const.MSGSTATUS, Const.NORMAL);
		switch (getResultCode()) {
		case Activity.RESULT_OK:
			AppLog.Log("deliveredReceiver", "RESULT_OK" + intent.getLongExtra(Const.DELIVEREDSMS, -1));
			if (intent.getBooleanExtra(Const.ISBROADCAST, true)) {
				AppLog.Log(Const.TAG, "broadcastId onreceived" + BroadcattId);
				dbHelper.addDeliverd(BroadcattId);
				dbHelper.addDeliveredBroadcastPart(BroadcattPartId);
				// } else {
				// AppLog.Log(Const.TAG, "feedbackId onreceived" + Id);
				// dbHelper.addDeliverdFeedback(Id);
			}

			break;
		case Activity.RESULT_CANCELED:
			AppLog.Log("deliveredReceiver", "RESULT_CANCELED");
			break;
		default:
			break;
		}
	}

}
