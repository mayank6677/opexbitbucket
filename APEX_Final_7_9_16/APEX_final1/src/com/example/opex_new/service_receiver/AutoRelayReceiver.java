package com.example.opex_new.service_receiver;

import java.util.ArrayList;

import com.example.opex_new.SendMessageAsyncTask;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Relay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.telephony.SmsMessage;

public class AutoRelayReceiver extends BroadcastReceiver {

	private DBHelper dbHelper;
	private Bundle bundle;
	private String fromAdd = "", fromAddWithoutCountryCode = "", prefixForAutoRelay;
	private SmsMessage messageEncoded = null;
	private StringBuilder originalMessage;
	private Object[] pdus;
	private ArrayList<Relay> relayList;
	private Relay relay;
	private Group group;

	// for feedback
	private int feedbackPartIdFromMessage = -1;
	private String answer;

	// public AutoRelayReceiver(ActionBarBaseActivity activity) {
	// // TODO Auto-generated constructor stub
	//
	// }

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		AppLog.Log(Const.TAG, "AutoRelayReceiver onReceive");

		dbHelper = new DBHelper(context);
		// relay = new Relay();
		relayList = new ArrayList<Relay>();

		originalMessage = new StringBuilder();
		bundle = intent.getExtras();

		if (bundle != null) {
			pdus = (Object[]) bundle.get("pdus");
			for (int i = 0; i < pdus.length; i++) {
				messageEncoded = SmsMessage.createFromPdu((byte[]) pdus[i]);
				originalMessage.append(messageEncoded.getDisplayMessageBody());
				fromAdd = messageEncoded.getOriginatingAddress();

				if (fromAdd.length() > 10) {
					fromAddWithoutCountryCode = fromAdd.substring(fromAdd.length() - 10);
				}

				dbHelper.getRelay(fromAdd, fromAddWithoutCountryCode, relayList);
				prefixForAutoRelay = originalMessage.toString().split(" ")[0];

				AppLog.Log(Const.TAG, "message:" + messageEncoded);
				AppLog.Log(Const.TAG, "text:" + originalMessage);
				AppLog.Log(Const.TAG, "fromAdd:" + fromAdd);
				AppLog.Log(Const.TAG, "fromAddWithoutCountryCode:" + fromAddWithoutCountryCode);
				AppLog.Log(Const.TAG, "prefix" + prefixForAutoRelay);

				for (int k = 0; k < relayList.size(); k++) {
					relay = relayList.get(k);
					AppLog.Log(Const.TAG, "relay prefix:" + relay.getPrefix());

					if (prefixForAutoRelay.equals(relay.getPrefix())) {
						int prefixLength = relay.getPrefix().length();
						AppLog.Log(Const.TAG, "if");
						group = relay.getGroup();
						AppLog.Log(Const.TAG, "grpup" + group);
						AppLog.Log(Const.TAG, "members" + group.getMemberList());

						new SendMessageAsyncTask(group.getMemberList(), originalMessage.substring(prefixLength + 1),
								null, null).execute();

						/*
						 * for (int j = 0; j < group.getMemberList().size();
						 * j++) { AppLog.Log("Receiver", "sendSMS"); SmsManager
						 * smsManager = SmsManager.getDefault(); try {
						 * smsManager .sendTextMessage(
						 * group.getMemberList().get(j) .getPhone(), null,
						 * originalMessage .substring(prefixLength + 1), null,
						 * null); Thread.sleep(3000); } catch (Exception e) {
						 * e.printStackTrace(); } }
						 */
					}
				}
				// receiving feedback
				try {
					if(originalMessage.substring(originalMessage.indexOf("-") + 1,
							originalMessage.length()-1).trim().contains(" ")){
						//String after -
						String afterHyp=originalMessage.substring(originalMessage.indexOf("-") + 1,
								originalMessage.length()).trim();
						feedbackPartIdFromMessage = Integer
								.parseInt(afterHyp.substring(0,
										afterHyp.indexOf(" ")).trim());
					}else{
						feedbackPartIdFromMessage = Integer
								.parseInt(originalMessage.substring(originalMessage.indexOf("-") + 1,
										originalMessage.length()).trim());
					}
					

					// long feedbackId = Integer.parseInt(originalMessage
					// .substring(0, originalMessage.indexOf(".")).trim());

					// long feedbackId =
					// dbHelper.getFeedbackId(feedbackPartIdFromMessage);
					answer = originalMessage.substring(0,
							originalMessage.indexOf("-"));
					AppLog.Log(Const.TAG, "AutoRelayReceiver MessageID:"+feedbackPartIdFromMessage);
					AppLog.Log(Const.TAG, "AutoRelayReceiver Answer:"+answer);
					
					// int messageLength = originalMessage.length();
					// String string =
					// originalMessage.toString().replaceAll("<",
					// "");
					// int j = string.length();
					// int answerNumber = messageLength - j;
					//
					// AppLog.Log(Const.TAG, "answerNumber" + answerNumber);

					// for (int k = 0; k < answerNumber; k++) {
					// String questionNo = originalMessage.substring(
					// originalMessage.indexOf("<") + 1,
					// originalMessage.indexOf(">"));
					// answer = originalMessage.substring(
					// originalMessage.indexOf(">-") + 2,
					// originalMessage.indexOf(">-") + 3);
					// originalMessage = originalMessage.replace(
					// originalMessage.indexOf("<"),
					// originalMessage.indexOf("<") + 1, "");
					// originalMessage = originalMessage.replace(
					// originalMessage.indexOf(">-"),
					// originalMessage.indexOf(">-") + 2, "");
					//
					// AppLog.Log(Const.TAG, "answerNo:" + questionNo);

					// feedback id added
					// dbHelper.addAnswer(answer, feedbackId,
					// Integer.parseInt(questionNo));

					// if (answer.equalsIgnoreCase("a")) {
					// dbHelper.addReceivedQuestion(feedbackId,
					// Integer.parseInt(questionNo),
					// Const.answerA, true);
					// } else if (answer.equalsIgnoreCase("b")) {
					// dbHelper.addReceivedQuestion(feedbackId,
					// Integer.parseInt(questionNo),
					// Const.answerB, true);
					// } else if (answer.equalsIgnoreCase("c")) {
					// dbHelper.addReceivedQuestion(feedbackId,
					// Integer.parseInt(questionNo),
					// Const.answerC, true);
					// }

					// if (answer.equalsIgnoreCase("a")) {
					// dbHelper.addReceivedFeedbackPart(
					// feedbackIdFromMessage, Const.answerA);
					// } else if (answer.equalsIgnoreCase("b")) {
					// dbHelper.addReceivedFeedbackPart(
					// feedbackIdFromMessage, Const.answerB);
					// } else if (answer.equalsIgnoreCase("c")) {
					// dbHelper.addReceivedFeedbackPart(
					// feedbackIdFromMessage, Const.answerC);
					// }

					// if (k == 0) {

					long receivedCount;
					if (answer.equalsIgnoreCase("a")) {
						receivedCount = dbHelper.addReceivedFeedbackPart(feedbackPartIdFromMessage, Const.answerA, true,
								/* Integer.parseInt(questionNo) */fromAdd, fromAddWithoutCountryCode);
						System.out.println("feedback received count is"+receivedCount);
						if (receivedCount != -1) {
							dbHelper.addReceivedFeedback(feedbackPartIdFromMessage, Const.answerA, true);
							AppLog.Log(Const.TAG, "AutoRelayReceiver Receive:"+answer);
						}
					} else if (answer.equalsIgnoreCase("b")) {
						receivedCount = dbHelper.addReceivedFeedbackPart(feedbackPartIdFromMessage, Const.answerB, true,
								/* Integer.parseInt(questionNo) */fromAdd, fromAddWithoutCountryCode);
						if (receivedCount != -1) {
							dbHelper.addReceivedFeedback(feedbackPartIdFromMessage, Const.answerB, true);
						}
					} else if (answer.equalsIgnoreCase("c")) {
						receivedCount = dbHelper.addReceivedFeedbackPart(feedbackPartIdFromMessage, Const.answerC, true,
								/* Integer.parseInt(questionNo) */fromAdd, fromAddWithoutCountryCode);
						if (receivedCount != -1) {
							dbHelper.addReceivedFeedback(feedbackPartIdFromMessage, Const.answerC, true);
						}
					} else if (answer.equalsIgnoreCase("d")) {
						receivedCount = dbHelper.addReceivedFeedbackPart(feedbackPartIdFromMessage, Const.answerD, true,
								/* Integer.parseInt(questionNo) */fromAdd, fromAddWithoutCountryCode);
						if (receivedCount != -1) {
							dbHelper.addReceivedFeedback(feedbackPartIdFromMessage, Const.answerD, true);
						}
					} else if (answer.equalsIgnoreCase("e")) {
						receivedCount = dbHelper.addReceivedFeedbackPart(feedbackPartIdFromMessage, Const.answerE, true,
								/* Integer.parseInt(questionNo) */fromAdd, fromAddWithoutCountryCode);
						if (receivedCount != -1) {
							dbHelper.addReceivedFeedback(feedbackPartIdFromMessage, Const.answerE, true);
						}
					}
					// } else {
					// if (answer.equalsIgnoreCase("a")) {
					// dbHelper.addReceivedFeedbackPart(
					// feedbackPartIdFromMessage,
					// Const.answerA, false,
					// Integer.parseInt(questionNo));
					// dbHelper.addReceivedFeedback(feedbackId,
					// Const.answerA, false);
					// } else if (answer.equalsIgnoreCase("b")) {
					// dbHelper.addReceivedFeedbackPart(
					// feedbackPartIdFromMessage,
					// Const.answerB, false,
					// Integer.parseInt(questionNo));
					// dbHelper.addReceivedFeedback(feedbackId,
					// Const.answerB, false);
					// } else if (answer.equalsIgnoreCase("c")) {
					// dbHelper.addReceivedFeedbackPart(
					// feedbackPartIdFromMessage,
					// Const.answerC, false,s
					// Integer.parseInt(questionNo));
					// dbHelper.addReceivedFeedback(feedbackId,
					// Const.answerC, false);
					// }
					// }
					AppLog.Log(Const.TAG, "answer" + answer);
					// }

				} catch (Exception e) {
					AppLog.Log(Const.TAG, "Error in AutoRelayReceiver" + e.toString());
					// TODO: handle exception
				}

				// answer = originalMessage.substring(
				// originalMessage.indexOf(".") + 1,
				// originalMessage.indexOf(".",
				// originalMessage.indexOf(".") + 1) - 1).trim();
				// if (answer.equalsIgnoreCase("a")) {
				// dbHelper.addReceivedFeedback(feedbackIdFromMessage,
				// Const.answerA);
				// } else if (answer.equalsIgnoreCase("b")) {
				// dbHelper.addReceivedFeedback(feedbackIdFromMessage,
				// Const.answerB);
				// } else if (answer.equalsIgnoreCase("c")) {
				// dbHelper.addReceivedFeedback(feedbackIdFromMessage,
				// Const.answerC);
				// } else if (answer.equalsIgnoreCase("d")) {
				// dbHelper.addReceivedFeedback(feedbackIdFromMessage,
				// Const.answerD);
				// }
				//
			}
			// }
		}

	}
}
