package com.example.opex_new.service_receiver;

import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.db.DBHelper;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.telephony.SmsManager;

public class SendSMSReceiver extends BroadcastReceiver {
	private DBHelper dbHelper;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		AppLog.Log("sendReceiver", "sendReceiver");
		dbHelper = new DBHelper(context);
		AndyUtils.removeSimpleProgressDialog();
		long BroadcastId = intent.getLongExtra(Const.SENDSMS, -1);
		long BroadcastPartId = intent.getLongExtra(Const.BROADCASTPARTID, -1);
		// boolean isBroadcast = intent.getBooleanExtra(Const.ISBROADCAST,
		// true);
		// int msgStatus = intent.getIntExtra(Const.MSGSTATUS, Const.NORMAL);
		// String phoneNo = intent.getStringExtra(Const.PHONENO);
		// String msg = intent.getStringExtra(Const.MSG);
		// AppLog.Log(Const.TAG, "receiver msg" +
		// intent.getStringExtra(Const.MSG));
		switch (getResultCode()) {
		case Activity.RESULT_OK:
			AppLog.Log("sendReceiver", "RESULT_OK" + intent.getLongExtra(Const.SENDSMS, -1));
			AppLog.Log("sendReceiver", "broadcastPartId" + intent.getLongExtra(Const.BROADCASTPARTID, -1));
			// if (isBroadcast) {
			AppLog.Log(Const.TAG, "broadcastId onreceived" + BroadcastId);
			dbHelper.addSent(BroadcastId);
			dbHelper.addSentBroadcastPart(BroadcastPartId);
			// } else {
			// AppLog.Log(Const.TAG, "feedbackId onreceived" + BroadcattId);
			// dbHelper.addSentFeedback(BroadcattId);
			// }

			break;
		case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
			AppLog.Log("sendReceiver", "RESULT_ERROR_GENERIC_FAILURE");
			// checkStatus(context, BroadcastId, BroadcastPartId, isBroadcast,
			// msgStatus, phoneNo, msg);
			break;
		case SmsManager.RESULT_ERROR_NO_SERVICE:
			AppLog.Log("sendReceiver", "RESULT_ERROR_NO_SERVICE");
			// AppLog.Log(Const.TAG,
			// "receiver msg inner" + intent.getStringExtra(Const.MSG));
			// AppLog.Log(Const.TAG, "receiver msg" + msg);
			// checkStatus(context, BroadcastId, BroadcastPartId, isBroadcast,
			// msgStatus, phoneNo, msg);
			break;
		case SmsManager.RESULT_ERROR_NULL_PDU:
			AppLog.Log("sendReceiver", "RESULT_ERROR_NULL_PDU");
			// checkStatus(context, BroadcastId, BroadcastPartId, isBroadcast,
			// msgStatus, phoneNo, msg);
			break;
		case SmsManager.RESULT_ERROR_RADIO_OFF:
			AppLog.Log("sendReceiver", "RESULT_ERROR_RADIO_OFF");
			// checkStatus(context, BroadcastId, BroadcastPartId, isBroadcast,
			// msgStatus, phoneNo, msg);
			break;
		default:
			break;
		}
	}

	private void checkStatus(Context context, long broadcastId, long broadcastPartId, boolean isBroadcast,
			int msgStatus, final String phoneNo, final String msg) {
		AppLog.Log(Const.TAG, "checkStatus EXPRESS" + broadcastPartId);
		if (msgStatus == Const.EXPRESS) {
			AppLog.Log(Const.TAG, "checkStatus EXPRESS");
			Intent sendIntent = new Intent(Const.SENDSMS);
			sendIntent.putExtra(Const.SENDSMS, broadcastId);
			sendIntent.putExtra(Const.ISBROADCAST, isBroadcast);
			sendIntent.putExtra(Const.BROADCASTPARTID, broadcastPartId);
			sendIntent.putExtra(Const.MSGSTATUS, msgStatus);
			sendIntent.putExtra(Const.PHONENO, phoneNo);
			sendIntent.putExtra(Const.MSG, msg);
			Intent deliveredIntent = new Intent(Const.DELIVEREDSMS);
			deliveredIntent.putExtra(Const.DELIVEREDSMS, broadcastId);
			deliveredIntent.putExtra(Const.ISBROADCAST, isBroadcast);
			deliveredIntent.putExtra(Const.BROADCASTPARTID, broadcastPartId);
			deliveredIntent.putExtra(Const.MSGSTATUS, msgStatus);
			deliveredIntent.putExtra(Const.PHONENO, phoneNo);
			deliveredIntent.putExtra(Const.MSG, msg);

			final PendingIntent sendPendingIntent = PendingIntent.getBroadcast(context, 0, sendIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			final PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(context, 0, deliveredIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			try {

				final Handler handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						AppLog.Log(Const.TAG, "sleep");
						// SmsManager smsManager = SmsManager.getDefault();
						// smsManager.sendTextMessage(phoneNo, null, "abc",
						// sendPendingIntent, deliveredPendingIntent);
						//
						// if (msg.length() > 160) {
						// ArrayList<String> messageList = smsManager
						// .divideMessage(msg);
						// for (int i = 0; i < messageList.size(); i++) {
						// smsManager.sendTextMessage(phoneNo, null,
						// messageList.get(i), sendPendingIntent,
						// deliveredPendingIntent);
						// }
						// }
					}
				}, 10000);
				// Thread.sleep(Const.EXPRESSDELAY);
				// int repeatCount = 0;
				// while (repeatCount <= 30) {
				// AppLog.Log(Const.TAG, "sleep");
				// Thread.sleep(10000);
				// repeatCount++;
				// }

				// SmsManager smsManager = SmsManager.getDefault();
				// smsManager.sendTextMessage(phoneNo, null, "abc",
				// sendPendingIntent, deliveredPendingIntent);
				//
				// if (msg.length() > 160) {
				// ArrayList<String> messageList = smsManager
				// .divideMessage(msg);
				// for (int i = 0; i < messageList.size(); i++) {
				// smsManager.sendTextMessage(phoneNo, null,
				// messageList.get(i), sendPendingIntent,
				// deliveredPendingIntent);
				// }
				// }

			} catch (Exception e) {
				// TODO Auto-generated catch blocks
				e.printStackTrace();
			}

		} else if (msgStatus == Const.URGENT) {

		}
	}

	// private void sendSmsAfterSomeTIme(Context context, int requestCode) {
	// Calendar cal = Calendar.getInstance();
	// cal.setTimeInMillis(System.currentTimeMillis());
	// Intent intent = new Intent(context, SMSIntentService.class);
	// PendingIntent pendingIntent = PendingIntent.getService(context,
	// requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
	// AlarmManager alarmManager = (AlarmManager) context
	// .getSystemService(Context.ALARM_SERVICE);
	// alarmManager.set(type, cal.getTimeInMillis(), operation)
	// }

}
