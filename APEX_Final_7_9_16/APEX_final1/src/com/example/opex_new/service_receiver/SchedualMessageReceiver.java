package com.example.opex_new.service_receiver;

import java.util.ArrayList;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.Group;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.telephony.SmsManager;

public class SchedualMessageReceiver extends BroadcastReceiver {

	private Broadcast broadcast;
	private Group selectedGroup;
	private DBHelper dbHelper;
	private long[] ids = new long[101];
	public PendingIntent sendPendingIntent, deliveredPendingIntent;
	private Context context;

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		AppLog.Log(Const.TAG, "SchedualMessageReceiver onReceive");
		this.context = context;
		dbHelper = new DBHelper(context);
		broadcast = (Broadcast) intent.getSerializableExtra(Const.BROADCAST);
		selectedGroup = broadcast.getGroup();
		dbHelper.addBroadcast(broadcast, ids);

		// for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
		// setSendDelvereIntents(true, ids[0], ids[i + 1],
		// broadcast.getMsgStatus(), selectedGroup.getMemberList()
		// .get(i).getPhone(), broadcast.getMessage());
		// try {
		// sendSMS(selectedGroup.getMemberList().get(i).getPhone(),
		// broadcast.getMessage(), sendPendingIntent,
		// deliveredPendingIntent);
		// Thread.sleep(3000);
		// } catch (InterruptedException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// }

		// new SendMessageAsyncTask(selectedGroup.getMemberList(),
		// broadcast.getMessage(), sendPendingIntent,
		// deliveredPendingIntent);

		new SchedualAsyncTask().execute();
	}

	public class SchedualAsyncTask extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
				setSendDelvereIntents(true, ids[0], ids[i + 1], broadcast.getMsgStatus(),
						selectedGroup.getMemberList().get(i).getPhone(), broadcast.getMessage());
				// try {
				sendSMS(selectedGroup.getMemberList().get(i).getPhone(), broadcast.getMessage(), sendPendingIntent,
						deliveredPendingIntent);
				// Thread.sleep(3000);
				// } catch (InterruptedException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
			}

			return null;
		}

	}

	public void setSendDelvereIntents(boolean isBroadcast, long broadcastId, long broadcastPartId, int msgStatus,
			String phoneNo, String msg) {
		AppLog.Log(Const.TAG, "setSendDelvereIntents ID: " + broadcastId);
		Intent sendIntent = new Intent(Const.SENDSMS);
		sendIntent.putExtra(Const.SENDSMS, broadcastId);
		sendIntent.putExtra(Const.ISBROADCAST, isBroadcast);
		sendIntent.putExtra(Const.BROADCASTPARTID, broadcastPartId);
		sendIntent.putExtra(Const.MSGSTATUS, msgStatus);
		sendIntent.putExtra(Const.PHONENO, phoneNo);
		sendIntent.putExtra(Const.MSG, msg);
		Intent deliveredIntent = new Intent(Const.DELIVEREDSMS);
		deliveredIntent.putExtra(Const.DELIVEREDSMS, broadcastId);
		deliveredIntent.putExtra(Const.ISBROADCAST, isBroadcast);
		deliveredIntent.putExtra(Const.BROADCASTPARTID, broadcastPartId);
		deliveredIntent.putExtra(Const.MSGSTATUS, msgStatus);
		deliveredIntent.putExtra(Const.PHONENO, phoneNo);
		deliveredIntent.putExtra(Const.MSG, msg);

		AppLog.Log(Const.TAG, "msg" + msg);
		sendPendingIntent = PendingIntent.getBroadcast(context, 0, sendIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		deliveredPendingIntent = PendingIntent.getBroadcast(context, 0, deliveredIntent,
				PendingIntent.FLAG_UPDATE_CURRENT);
	}

	public void sendSMS(final String phoneNo, final String message, final PendingIntent sendPendingIntent,
			final PendingIntent deliveredPendingIntent) {
		AppLog.Log("", "sendSMS");
		// AndyUtils.showSimpleProgressDialog(this);
		SmsManager smsManager = SmsManager.getDefault();
		try {
			AppLog.Log("", "try");

			if (message.length() > 160) {
				ArrayList<String> messageList = smsManager.divideMessage(message);
				for (int i = 0; i < messageList.size(); i++) {
					if (i == 0) {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), sendPendingIntent,
								deliveredPendingIntent);
					} else {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), null, null);
					}
				}
			} else {
				smsManager.sendTextMessage(phoneNo, null, message, sendPendingIntent, deliveredPendingIntent);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
