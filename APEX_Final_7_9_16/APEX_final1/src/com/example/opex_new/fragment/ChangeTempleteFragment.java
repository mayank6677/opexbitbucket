package com.example.opex_new.fragment;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Templete;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ChangeTempleteFragment extends Fragment implements OnClickListener {

	private GroupActivity activity;
	private EditText etTempleteChange;
	private Button btnEditTemplete, btnSaveTempleteChange, btnDeleteTemplete, btnSendTemplete, btnScheduleTemplete;
	private TextView tvSizeChangeTemplate;
	private Templete templete;
	private boolean isForMessaging, isBroadcast;
	// private ArrayList<Member> memberListForBroadcast;
	private Group selectedGroup;
	private int msgStatus;

	// private String SEND = "send", DELIVERED = "delivered";
	// private BroadcastReceiver sendReceiver, deliveredReceiver;
	// private PendingIntent sendPendingIntent, deliveredPendingIntent;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		isForMessaging = getArguments().getBoolean(Const.ISFORMESSAGING);
		isBroadcast = getArguments().getBoolean(Const.ISBROADCAST);
		msgStatus = getArguments().getInt(Const.MSGSTATUS);
		AppLog.Log(Const.TAG, "msgStatus" + msgStatus);

		if (isForMessaging) {
			// memberListForBroadcast = getArguments().getParcelableArrayList(
			// Const.Params.MEMBER);
			AppLog.Log(Const.TAG, "group" + getArguments().getSerializable(Const.Params.GROUP));
			selectedGroup = (Group) getArguments().getSerializable(Const.Params.GROUP);
		}

		// Intent sendIntent = new Intent(SEND);
		// Intent deliveredIntent = new Intent(DELIVERED);
		//
		// sendPendingIntent = PendingIntent.getBroadcast(activity, 0,
		// sendIntent,
		// 0);
		// deliveredPendingIntent = PendingIntent.getBroadcast(activity, 0,
		// deliveredIntent, 0);
		//
		// sendReceiver = new BroadcastReceiver() {
		//
		// @Override
		// public void onReceive(Context context, Intent intent) {
		// // TODO Auto-generated method stub
		// switch (getResultCode()) {
		// case Activity.RESULT_OK:
		// AppLog.Log(Const.TAG, "sendReceiver RESULT_OK");
		// break;
		// case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
		// AppLog.Log(Const.TAG,
		// "sendReceiver RESULT_ERROR_GENERIC_FAILURE");
		// break;
		// case SmsManager.RESULT_ERROR_NO_SERVICE:
		// AppLog.Log(Const.TAG,
		// "sendReceiver RESULT_ERROR_NO_SERVICE");
		// break;
		// case SmsManager.RESULT_ERROR_NULL_PDU:
		// AppLog.Log(Const.TAG, "sendReceiver RESULT_ERROR_NULL_PDU");
		// break;
		// case SmsManager.RESULT_ERROR_RADIO_OFF:
		// AppLog.Log(Const.TAG, "sendReceiver RESULT_ERROR_RADIO_OFF");
		// break;
		// default:
		// break;
		// }
		// }
		// };
		//
		// deliveredReceiver = new BroadcastReceiver() {
		//
		// @Override
		// public void onReceive(Context context, Intent intent) {
		// // TODO Auto-generated method stub
		// switch (getResultCode()) {
		// case Activity.RESULT_OK:
		// AppLog.Log(Const.TAG, "deliveredReceiver RESULT_OK");
		// break;
		// case Activity.RESULT_CANCELED:
		// AppLog.Log(Const.TAG, "deliveredReceiver RESULT_CANCELED");
		// break;
		// default:
		// break;
		// }
		// }
		// };

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_change_templete, container, false);
		etTempleteChange = (EditText) view.findViewById(R.id.etTempleteChange);
		btnEditTemplete = (Button) view.findViewById(R.id.btnEditTemplete);
		btnSaveTempleteChange = (Button) view.findViewById(R.id.btnSaveTempleteChange);
		btnDeleteTemplete = (Button) view.findViewById(R.id.btnDeleteTemplete);
		btnSendTemplete = (Button) view.findViewById(R.id.btnSendTemplete);
		btnScheduleTemplete = (Button) view.findViewById(R.id.btnScheduleTemplete);
		tvSizeChangeTemplate = (TextView) view.findViewById(R.id.tvSizeChangeTemplate);

		btnEditTemplete.setOnClickListener(this);
		btnSaveTempleteChange.setOnClickListener(this);
		btnDeleteTemplete.setOnClickListener(this);
		btnSendTemplete.setOnClickListener(this);
		btnScheduleTemplete.setOnClickListener(this);
		etTempleteChange.setEnabled(false);

		if (isForMessaging) {
			btnDeleteTemplete.setVisibility(View.GONE);
			btnSaveTempleteChange.setVisibility(View.GONE);
			btnSendTemplete.setVisibility(View.VISIBLE);
			btnScheduleTemplete.setVisibility(View.VISIBLE);
		} else {
			btnDeleteTemplete.setVisibility(View.VISIBLE);
			btnSaveTempleteChange.setVisibility(View.VISIBLE);
			btnSendTemplete.setVisibility(View.GONE);
			btnScheduleTemplete.setVisibility(View.GONE);
		}

		etTempleteChange.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				tvSizeChangeTemplate.setText(Const.MAX_MSG_SIZE - (s.length() % Const.MAX_MSG_SIZE) + "/"
						+ ((s.length() / Const.MAX_MSG_SIZE) + 1));
			}
		});
		return view;
	}


	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//activity.SetBackgroundColor(btnEditTemplete);
		// activity.setFontSize(btnEditTemplete);
		activity.setFonts(btnEditTemplete);

		//activity.SetBackgroundColor(btnDeleteTemplete);
		// activity.setFontSize(btnDeleteTemplete);
		activity.setFonts(btnDeleteTemplete);

		//activity.SetBackgroundColor(btnSaveTempleteChange);
		// activity.setFontSize(btnSaveTempleteChange);
		activity.setFonts(btnSaveTempleteChange);
		activity.setFontColor(btnSaveTempleteChange);

		//activity.SetBackgroundColor(btnSendTemplete);
		// activity.setFontSize(btnSendTemplete);
		activity.setFonts(btnSendTemplete);

		//activity.SetBackgroundColor(btnScheduleTemplete);
		// activity.setFontSize(btnScheduleTemplete);
		activity.setFonts(btnScheduleTemplete);

		//activity.setFontColor(etTempleteChange);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(etTempleteChange);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.actionBar.hide();
		templete = (Templete) getArguments().getSerializable(Const.TEMPLETE);
		etTempleteChange.setText(templete.getData());
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnEditTemplete:
			etTempleteChange.setEnabled(true);
			break;
		case R.id.btnSaveTempleteChange:
			if (isValidate()) {
				templete.setData(etTempleteChange.getText().toString());
				activity.dbHelper.updateTempleteName(templete);
				activity.removeThisFragment();
			}
			break;
		case R.id.btnDeleteTemplete:
			activity.dbHelper.deleteTemplete(templete.getId());
			activity.removeThisFragment();
			break;
		case R.id.btnSendTemplete:
			if (isValidate()) {
				AppLog.Log("btnSendTemplete", "btnSendTemplete");
				if (/* memberListForBroadcast */selectedGroup != null) {
					AppLog.Log("btnSendTemplete", "btnSendTemplete");
					long[] ids = new long[101];
					if (isBroadcast) {
						AndyUtils.showSimpleProgressDialog(activity);
						AppLog.Log(Const.TAG, "msgStatus" + msgStatus);
						activity.addBroadcast(selectedGroup, etTempleteChange.getText().toString(), ids, msgStatus);
						AppLog.Log(Const.TAG, "ID: " + ids);
						long broadcastId = ids[0];

						for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
							activity.setSendDelvereIntents(isBroadcast, broadcastId, ids[i + 1], msgStatus,
									selectedGroup.getMemberList().get(i).getPhone(),
									etTempleteChange.getText().toString());
							AppLog.Log(Const.TAG, "msg template" + etTempleteChange.getText().toString());
							// try {
							// activity.sendSMS(selectedGroup.getMemberList()
							// .get(i).getPhone(), etTempleteChange
							// .getText().toString(),
							// activity.sendPendingIntent,
							// activity.deliveredPendingIntent);
							// Thread.sleep(3000);

							/*
							 * final String phoneNo = selectedGroup
							 * .getMemberList().get(i).getPhone(); Handler
							 * handler = new Handler(); handler.postDelayed(new
							 * Runnable() {
							 * 
							 * @Override public void run() { // TODO
							 * Auto-generated method stub
							 * activity.sendSMS(phoneNo, etTempleteChange
							 * .getText().toString(),
							 * activity.sendPendingIntent,
							 * activity.deliveredPendingIntent); } }, 3000);
							 */
							// } catch (Exception e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
						}
						// activity.setSendDelvereIntents(isBroadcast,
						// broadcastId,);
						activity.sendMultipleSMS(selectedGroup.getMemberList(), etTempleteChange.getText().toString(),
								activity.sendPendingIntent, activity.deliveredPendingIntent);
					} else {
						// Id = activity.addFeedback(selectedGroup);
						// AppLog.Log(Const.TAG, "ID: " + Id);
						// activity.setSendDelvereIntents(isBroadcast, Id);
						// activity.sendMultipleSMS(
						// selectedGroup.getMemberList(),
						// Id
						// + ". "
						// + etTempleteChange.getText().toString()
						// + "\nPlease reply in the format like this: "
						// + Id + ". A.",
						// activity.sendPendingIntent,
						// activity.deliveredPendingIntent);
					}
					// if (isBroadcast) {
					// AppLog.Log("btnSendTemplete", "btnSendTemplete");
					// long broadcastId = activity.addBroadcast(selectedGroup);
					// AppLog.Log(Const.TAG, "btnSendTemplete broadcastId"
					// + broadcastId);
					//
					// // Intent sendIntent = new Intent(SEND);
					// // sendIntent.putExtra(SEND, broadcastId);
					// // Intent deliveredIntent = new Intent(DELIVERED);
					// // deliveredIntent.putExtra(DELIVERED, broadcastId);
					// //
					// // sendPendingIntent =
					// // PendingIntent.getBroadcast(activity, 0, sendIntent,
					// // 0);
					// // deliveredPendingIntent =
					// // PendingIntent.getBroadcast(activity, 0,
					// // deliveredIntent, 0);
					//
					// activity.setSendDelvereIntents(broadcastId);
					// activity.sendMultipleSMS(selectedGroup.getMemberList(),
					// etTempleteChange.getText().toString(),
					// activity.sendPendingIntent,
					// activity.deliveredPendingIntent);
					// } else {
					// // activity.sendMultipleSMS(memberListForBroadcast,
					// // etTempleteChange.getText().toString(),
					// // activity.sendPendingIntent,
					// // activity.deliveredPendingIntent);
					// }
					activity.onBackPressed();
					activity.onBackPressed();

				}
			}
			break;
		case R.id.btnScheduleTemplete:
			if (isValidate()) {
				if (selectedGroup != null) {
					activity.showSchedualMessageDialog(true, selectedGroup, etTempleteChange.getText().toString(),
							msgStatus, true, -1);
				}
			}
			break;
		default:
			break;
		}
	}

	private boolean isValidate() {
		String msg = null;

		if (TextUtils.isEmpty(etTempleteChange.getText().toString())) {
			msg = activity.getString(R.string.toast_enter_msg);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}
		return true;
	}

}
