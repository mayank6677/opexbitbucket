package com.example.opex_new.fragment;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.Group;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class WriteMessageFragment extends Fragment implements OnClickListener {

	private GroupActivity activity;
	private EditText etMsg;
	private Button btnSaveMsg, btnScheduleMsg;
	private TextView tvSizeMsg;
	// private ArrayList<Member> memberListForBroadcast;
	private boolean isBroadcast;
	private Group selectedGroup;
	private int msgStatus;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// memberListForBroadcast = getArguments().getParcelableArrayList(
		// Const.Params.MEMBER);
		selectedGroup = (Group) getArguments().getSerializable(Const.Params.GROUP);
		isBroadcast = getArguments().getBoolean(Const.ISBROADCAST);
		msgStatus = getArguments().getInt(Const.MSGSTATUS);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.setActionBarTitle(activity.getString(R.string.title_write_message));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_write_message, container, false);
		etMsg = (EditText) view.findViewById(R.id.etMsg);
		tvSizeMsg = (TextView) view.findViewById(R.id.tvSizeMsg);
		btnSaveMsg = (Button) view.findViewById(R.id.btnSaveMsg);
		btnScheduleMsg = (Button) view.findViewById(R.id.btnScheduleMsg);
		tvSizeMsg.setText(Const.MAX_MSG_SIZE + "/" + 1);
		btnSaveMsg.setOnClickListener(this);
		btnScheduleMsg.setOnClickListener(this);

		etMsg.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				tvSizeMsg.setText(Const.MAX_MSG_SIZE - (s.length() % Const.MAX_MSG_SIZE) + "/"
						+ ((s.length() / Const.MAX_MSG_SIZE) + 1));
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//activity.SetBackgroundColor(btnSaveMsg);
		// activity.setFontSize(btnSaveMsg);
		activity.setFonts(btnSaveMsg);

		//activity.SetBackgroundColor(btnScheduleMsg);
		// activity.setFontSize(btnScheduleMsg);
		activity.setFonts(btnScheduleMsg);

	//	activity.setFontColor(etMsg);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(etMsg);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnSaveMsg:
			if (isValidate()) {
				if (/* memberListForBroadcast */selectedGroup != null) {
					long[] ids = new long[101];
					if (isBroadcast) {
						AndyUtils.showSimpleProgressDialog(activity);

						activity.addBroadcast(selectedGroup, etMsg.getText().toString(), ids, msgStatus);
						AppLog.Log(Const.TAG, "ID: " + ids);
						long broadcastId = ids[0];

						for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
							activity.setSendDelvereIntents(isBroadcast, broadcastId, ids[i + 1], msgStatus,
									selectedGroup.getMemberList().get(i).getPhone(), etMsg.getText().toString());
							// try {
							// activity.sendSMS(selectedGroup.getMemberList()
							// .get(i).getPhone(), etMsg.getText()
							// .toString(),
							// activity.sendPendingIntent,
							// activity.deliveredPendingIntent);

							/*
							 * final String phoneNo = selectedGroup
							 * .getMemberList().get(i).getPhone(); Handler
							 * handler = new Handler(); handler.postDelayed(new
							 * Runnable() {
							 * 
							 * @Override public void run() { // TODO
							 * Auto-generated method stub
							 * activity.sendSMS(phoneNo, etMsg.getText()
							 * .toString(), activity.sendPendingIntent,
							 * activity.deliveredPendingIntent); } }, 3000);
							 */
							// Thread.sleep(3000);
							// } catch (Exception e) {
							// // TODO Auto-generated catch block
							// e.printStackTrace();
							// }
						}
						activity.sendMultipleSMS(selectedGroup.getMemberList(), etMsg.getText().toString(),
								activity.sendPendingIntent, activity.deliveredPendingIntent);
					}
					// if (isBroadcast) {
					// Id = activity.addBroadcast(selectedGroup, etMsg.getText()
					// .toString());
					// activity.setSendDelvereIntents(isBroadcast, Id);
					// activity.sendMultipleSMS(selectedGroup.getMemberList(),
					// etMsg.getText().toString(),
					// activity.sendPendingIntent,
					// activity.deliveredPendingIntent);
					// } else {
					// Id = activity.addFeedback(selectedGroup);
					// activity.setSendDelvereIntents(isBroadcast, Id);
					// activity.sendMultipleSMS(selectedGroup.getMemberList(),
					// Id
					// + ". " + etMsg.getText().toString()
					// + "\nPlease reply in the format like this: " + Id
					// + ". A.", activity.sendPendingIntent,
					// activity.deliveredPendingIntent);
					// }

					activity.onBackPressed();
				}
			}
			break;
		case R.id.btnScheduleMsg:
			if (isValidate()) {
				if (selectedGroup != null) {
					activity.showSchedualMessageDialog(false, selectedGroup, etMsg.getText().toString(), msgStatus,
							true, -1);
				}
			}
			break;
		default:
			break;
		}
	}

	private boolean isValidate() {
		String msg = null;

		if (TextUtils.isEmpty(etMsg.getText().toString())) {
			msg = activity.getString(R.string.toast_enter_msg);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}
		return true;
	}
}
