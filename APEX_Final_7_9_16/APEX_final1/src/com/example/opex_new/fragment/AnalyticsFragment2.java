package com.example.opex_new.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.AnalyticsAdapter;
import com.example.opex_new.adapter.AnalyticsFeedbackAdapter;
import com.example.opex_new.adapter.AnalyticsFeedbackAdapter2;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.Feedback;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class AnalyticsFragment2 extends Fragment implements OnCheckedChangeListener, OnItemClickListener {

	private RadioGroup rgAnalytics;
	private RadioButton  rbFeedback;

	private ListView lvAnalytics1;
	private GroupActivity activity;
	private String time;
	private CheckBox cbLast12Hr1;

	private ArrayList<Feedback> feedbackList;
	// private ArrayList<FeedbackPart> feedbackPartList;
	// private ArrayList<Question> questionList;
	// private ArrayList<BroadcastPart> broadcastPartList;
	// private ArrayList<FeedbackPart> feedbackPartList;
	private AnalyticsAdapter analyticsAdapter;
	private AnalyticsFeedbackAdapter2 analyticsFeedbackAdapter2;
	private LinearLayout llAnalytics;
	private TextView tvQId, tvQuestion, tvDateTime,ivDeletePolls;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_analytics2, container, false);
		rgAnalytics = (RadioGroup) view.findViewById(R.id.rgAnalytics);
	
		rbFeedback = (RadioButton) view.findViewById(R.id.rbFeedback1);
	
		lvAnalytics1 = (ListView) view.findViewById(R.id.lvAnalytics1);
		llAnalytics = (LinearLayout) view.findViewById(R.id.llAnalytics);
		tvQId = (TextView) view.findViewById(R.id.tvQId);
		tvQuestion = (TextView) view.findViewById(R.id.tvQuestion);
		tvDateTime = (TextView) view.findViewById(R.id.tvDateTime);
		cbLast12Hr1 = (CheckBox) view.findViewById(R.id.cbLast12Hr1);
		rbFeedback.setChecked(true);
		
		time = getTime(1);
	
		loadData();
		// new LoadData().execute();

		
		rbFeedback.setOnCheckedChangeListener(this);
	
		lvAnalytics1.setOnItemClickListener(this);
		cbLast12Hr1.setOnCheckedChangeListener(this);
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity = (GroupActivity) getActivity();

	
		feedbackList = new ArrayList<Feedback>();
		// feedbackPartList = new ArrayList<FeedbackPart>();
		// questionList = new ArrayList<Question>();
		// broadcastPartList = new ArrayList<BroadcastPart>();
		// feedbackPartList = new ArrayList<FeedbackPart>();
		
		// analyticsFeedbackAdapter = new AnalyticsFeedbackAdapter(activity,
		// questionList);
		analyticsFeedbackAdapter2 = new  AnalyticsFeedbackAdapter2(activity, feedbackList);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity.setActionBarTitle(activity.getString(R.string.title_reports));
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// activity.setFontSize(rbBroadcast);
	

		activity.setBackground(llAnalytics);

		//activity.setFontColor(tvQId);
		activity.setFonts(tvQId);

		//activity.setFontColor(tvQuestion);
		activity.setFonts(tvQuestion);

	//	activity.setFontColor(tvDateTime);
		activity.setFonts(tvDateTime);

	}

	public String getTime(int lastHour) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, -lastHour);
		Date date = calendar.getTime();
		return activity.formatDateTime(date);
	}

	public String getLastWeekTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		Date date = calendar.getTime();
		return activity.formatDateTime(date);
	}

	public String getLastMonthTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Date date = calendar.getTime();
		return activity.formatDateTime(date);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			switch (buttonView.getId()) {
		
			case R.id.rbFeedback:
			
				break;
			
			case R.id.cbLast12Hr1:
				// time = getTime(12);
				time = null;
			
				break;

			default:
				break;
			}
			loadData();
			// new LoadData().execute();
		}
	}

	private class LoadData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
			feedbackList.clear();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

				activity.dbHelper.getFeedbackList(time, feedbackList);
	
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
		
				lvAnalytics1.setAdapter(analyticsFeedbackAdapter2);
		
		}

	}

	private void loadData() {

		
		feedbackList.clear();
		// questionList.clear();
	
	
			// try {
			// new Thread(new Runnable() {
			//
			// @Override
			// public void run() {
			// TODO Auto-generated method stub
			activity.dbHelper.getFeedbackList(time, feedbackList);
			// activity.dbHelper.getFeedbackPartList(time,
			// feedbackPartList);
			// feedback id added
			// activity.dbHelper.getQuestionList(feedbackList,
			// questionList);

			AppLog.Log(Const.TAG, "isBroadcast false");

			// }
			// }).join();
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			lvAnalytics1.setAdapter(analyticsFeedbackAdapter2);

		
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		// TODO Auto-generated method stub
		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		        switch (which){
		        case DialogInterface.BUTTON_POSITIVE:
		        	Feedback feedback;
		    		feedback=feedbackList.get(position);
		    	
		    		activity.dbHelper.deletefeedback(feedback.getId());
		    		loadData();
		            //Yes button clicked
		            break;

		        case DialogInterface.BUTTON_NEGATIVE:
		            //No button clicked
		        	
		            break;
		        }
		    }
		};

		AlertDialog.Builder  builder = new AlertDialog.Builder(activity);
		builder.setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
		    .setNegativeButton("No", dialogClickListener).show();
		
		 
			// bundle.putSerializable(Const.QUESTION,
			// questionList.get(position));
		
		
	
		
	}
}
