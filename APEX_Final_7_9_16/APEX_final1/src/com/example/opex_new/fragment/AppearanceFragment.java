package com.example.opex_new.fragment;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AppearanceFragment extends Fragment implements OnClickListener {
	private GroupActivity activity;
	private PreferenceHelper preferenceHelper;
	private Button tvChangeColor, tvChangeFont, tvChangeSize, tvChangeTextStyle, tvChangeBackGround;
	private Dialog colorDialog, sizeDialog, styleDialog, fontsDialog, backgroundDialog;
	private TextView tvColor1, tvColor2, tvColor3, tvColor4, tvColor5, tvColor6, tvColor7, tvColor8, tvColor9,
			tvColor10, tvColor11, tvSize8, tvSize9, tvSize10, tvSize12, tvSize14, tvFont1, tvFont2, tvFont3, tvFont4,
			tvFont5, tvFont6, tvFont7, tvFont8, tvFont9, tvFont10, tvStyle1, tvStyle2, tvStyle3, tvStyle4;
	private ImageView ivBg1, ivBg2, ivBg3, ivBg4, ivBg5, ivBg6, ivBg7, ivBg8, ivBg9;
	private LinearLayout llAppearance;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_appearance, container, false);

		tvChangeColor = (Button) view.findViewById(R.id.tvChangeColor);
		tvChangeFont = (Button) view.findViewById(R.id.tvChangeFont);
		tvChangeSize = (Button) view.findViewById(R.id.tvChangeSize);
		tvChangeTextStyle = (Button) view.findViewById(R.id.tvChangeTextStyle);
		tvChangeBackGround = (Button) view.findViewById(R.id.tvChangeBackGround);
		llAppearance = (LinearLayout) view.findViewById(R.id.llAppearance);

		tvChangeColor.setOnClickListener(this);
		tvChangeFont.setOnClickListener(this);
		tvChangeSize.setOnClickListener(this);
		tvChangeTextStyle.setOnClickListener(this);
		tvChangeBackGround.setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.setActionBarTitle(getString(R.string.title_apperance));
		preferenceHelper = new PreferenceHelper(activity);
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		changeFonts();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.tvChangeColor:
			showColorDialog();
			break;
		case R.id.tvChangeFont:
			showFontsDialog();
			break;
		case R.id.tvChangeSize:
			showSizeDialog();
			break;
		case R.id.tvChangeTextStyle:
			showStyleDialog();
			break;
		case R.id.tvChangeBackGround:
			showBackgroundDialog();
			break;
		case R.id.tvColor1:
			preferenceHelper.putColor(getResources().getColor(R.color.color_text));
			colorDialog.dismiss();
			break;
		case R.id.tvColor2:
			preferenceHelper.putColor(getResources().getColor(R.color.skyblue));
			colorDialog.dismiss();
			break;
		case R.id.tvColor3:
			preferenceHelper.putColor(getResources().getColor(R.color.red));
			colorDialog.dismiss();
			break;
		case R.id.tvColor4:
			preferenceHelper.putColor(getResources().getColor(R.color.green));
			colorDialog.dismiss();
			break;
		case R.id.tvColor5:
			preferenceHelper.putColor(getResources().getColor(R.color.yellow));
			colorDialog.dismiss();
			break;
		case R.id.tvColor6:
			preferenceHelper.putColor(getResources().getColor(R.color.pink));
			colorDialog.dismiss();
			break;
		case R.id.tvColor7:
			preferenceHelper.putColor(getResources().getColor(R.color.color_text_menu));
			colorDialog.dismiss();
			break;
		case R.id.tvColor8:
			preferenceHelper.putColor(getResources().getColor(R.color.facebook));
			colorDialog.dismiss();
			break;
		case R.id.tvColor9:
			preferenceHelper.putColor(getResources().getColor(R.color.black));
			colorDialog.dismiss();
			break;
		case R.id.tvColor10:
			preferenceHelper.putColor(getResources().getColor(R.color.dark_blue));
			colorDialog.dismiss();
			break;
		case R.id.tvColor11:
			preferenceHelper.putColor(getResources().getColor(R.color.maroon));
			colorDialog.dismiss();
			break;

		case R.id.tvSize8:
			preferenceHelper.putSize(activity.getResources().getDimension(R.dimen.size_8));
			sizeDialog.dismiss();
			break;
		case R.id.tvSize9:
			preferenceHelper.putSize(activity.getResources().getDimension(R.dimen.size_9));
			sizeDialog.dismiss();
			break;
		case R.id.tvSize10:
			preferenceHelper.putSize(activity.getResources().getDimension(R.dimen.size_10));
			sizeDialog.dismiss();
			break;
		case R.id.tvSize12:
			preferenceHelper.putSize(activity.getResources().getDimension(R.dimen.size_12));
			sizeDialog.dismiss();
			break;
		case R.id.tvSize14:
			preferenceHelper.putSize(activity.getResources().getDimension(R.dimen.size_14));
			sizeDialog.dismiss();
			break;
		case R.id.tvStyle1:
			preferenceHelper.putStyle(Typeface.NORMAL);
			preferenceHelper.putStyleFromGallary(null);
			styleDialog.dismiss();
			break;
		case R.id.tvStyle2:
			preferenceHelper.putStyle(Typeface.BOLD);
			preferenceHelper.putStyleFromGallary(null);
			styleDialog.dismiss();
			break;
		case R.id.tvStyle3:
			preferenceHelper.putStyle(Typeface.ITALIC);
			preferenceHelper.putStyleFromGallary(null);
			styleDialog.dismiss();
			break;
		case R.id.tvStyle4:
			preferenceHelper.putStyle(Typeface.BOLD_ITALIC);
			preferenceHelper.putStyleFromGallary(null);
			styleDialog.dismiss();
			break;
		case R.id.tvFont1:
			preferenceHelper.putFonts("fonts/BASKVILL.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont2:
			preferenceHelper.putFonts("fonts/BATANG.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont3:
			preferenceHelper.putFonts("fonts/BRADHITC.TTF");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont4:
			preferenceHelper.putFonts("fonts/CANDARA.TTF");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont5:
			preferenceHelper.putFonts("fonts/Century Gothic.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont6:
			preferenceHelper.putFonts("fonts/ClarendonBTRoman.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont7:
			preferenceHelper.putFonts("fonts/chinese.simfang.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont8:
			preferenceHelper.putFonts("fonts/HARNGTON.TTF");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont9:
			preferenceHelper.putFonts("fonts/lucida calligraphy italic.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.tvFont10:
			preferenceHelper.putFonts("fonts/Monotype Corsiva.ttf");
			fontsDialog.dismiss();
			break;
		case R.id.ivBg1:
			// preferenceHelper.putBackground(R.drawable.img1);
			preferenceHelper.putBackground(R.drawable.backimagewithoutlogo);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg2:
			preferenceHelper.putBackground(R.drawable.img2);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg3:
			preferenceHelper.putBackground(R.drawable.img3);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg4:
			preferenceHelper.putBackground(R.drawable.img4);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg5:
			preferenceHelper.putBackground(R.drawable.img5);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg6:
			preferenceHelper.putBackground(R.drawable.img_6);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg7:
			// preferenceHelper.putBackground(R.drawable.img_7);
			preferenceHelper.putBackground(R.color.black);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg8:
			preferenceHelper.putBackground(R.drawable.img_8);
			backgroundDialog.dismiss();
			break;
		case R.id.ivBg9:
			preferenceHelper.putBackground(R.drawable.img_9);
			backgroundDialog.dismiss();
			break;
		default:
			break;
		}
	}

	private void showColorDialog() {
		if (colorDialog == null) {

			colorDialog = new Dialog(activity);
			colorDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			colorDialog.setContentView(R.layout.dialog_color);
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(colorDialog.getWindow().getAttributes());
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;

			colorDialog.getWindow().setAttributes(lp);

			tvColor1 = (TextView) colorDialog.findViewById(R.id.tvColor1);
			tvColor2 = (TextView) colorDialog.findViewById(R.id.tvColor2);
			tvColor3 = (TextView) colorDialog.findViewById(R.id.tvColor3);
			tvColor4 = (TextView) colorDialog.findViewById(R.id.tvColor4);
			tvColor5 = (TextView) colorDialog.findViewById(R.id.tvColor5);
			tvColor6 = (TextView) colorDialog.findViewById(R.id.tvColor6);
			tvColor7 = (TextView) colorDialog.findViewById(R.id.tvColor7);
			tvColor8 = (TextView) colorDialog.findViewById(R.id.tvColor8);
			tvColor9 = (TextView) colorDialog.findViewById(R.id.tvColor9);
			tvColor10 = (TextView) colorDialog.findViewById(R.id.tvColor10);
			tvColor11 = (TextView) colorDialog.findViewById(R.id.tvColor11);

			tvColor1.setOnClickListener(this);
			tvColor2.setOnClickListener(this);
			tvColor3.setOnClickListener(this);
			tvColor4.setOnClickListener(this);
			tvColor5.setOnClickListener(this);
			tvColor6.setOnClickListener(this);
			tvColor7.setOnClickListener(this);
			tvColor8.setOnClickListener(this);
			tvColor9.setOnClickListener(this);
			tvColor10.setOnClickListener(this);
			tvColor11.setOnClickListener(this);

			colorDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					changeFonts();
					colorDialog = null;
				}
			});
			colorDialog.show();
		}
	}

	private void showSizeDialog() {
		if (sizeDialog == null) {
			sizeDialog = new Dialog(activity);
			sizeDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			sizeDialog.setContentView(R.layout.dialog_size);

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(sizeDialog.getWindow().getAttributes());
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;

			sizeDialog.getWindow().setAttributes(lp);

			tvSize8 = (TextView) sizeDialog.findViewById(R.id.tvSize8);
			tvSize9 = (TextView) sizeDialog.findViewById(R.id.tvSize9);
			tvSize10 = (TextView) sizeDialog.findViewById(R.id.tvSize10);
			tvSize12 = (TextView) sizeDialog.findViewById(R.id.tvSize12);
			tvSize14 = (TextView) sizeDialog.findViewById(R.id.tvSize14);

			tvSize8.setOnClickListener(this);
			tvSize9.setOnClickListener(this);
			tvSize10.setOnClickListener(this);
			tvSize12.setOnClickListener(this);
			tvSize14.setOnClickListener(this);

			sizeDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					changeFonts();
					sizeDialog = null;
				}
			});
			sizeDialog.show();
		}
	}

	private void showStyleDialog() {
		if (styleDialog == null) {
			styleDialog = new Dialog(activity);
			styleDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			styleDialog.setContentView(R.layout.dialog_style);

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(styleDialog.getWindow().getAttributes());
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;

			styleDialog.getWindow().setAttributes(lp);

			tvStyle1 = (TextView) styleDialog.findViewById(R.id.tvStyle1);
			tvStyle2 = (TextView) styleDialog.findViewById(R.id.tvStyle2);
			tvStyle3 = (TextView) styleDialog.findViewById(R.id.tvStyle3);
			tvStyle4 = (TextView) styleDialog.findViewById(R.id.tvStyle4);
			tvStyle2.setTypeface(null, Typeface.BOLD);
			tvStyle3.setTypeface(null, Typeface.ITALIC);
			tvStyle4.setTypeface(null, Typeface.BOLD_ITALIC);

			tvStyle1.setOnClickListener(this);
			tvStyle2.setOnClickListener(this);
			tvStyle3.setOnClickListener(this);
			tvStyle4.setOnClickListener(this);

			styleDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					changeFonts();
					styleDialog = null;
				}
			});
			styleDialog.show();
		}
	}

	private void showBackgroundDialog() {
		if (backgroundDialog == null) {
			backgroundDialog = new Dialog(activity);
			backgroundDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			backgroundDialog.setContentView(R.layout.dialog_background);

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(backgroundDialog.getWindow().getAttributes());
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;

			backgroundDialog.getWindow().setAttributes(lp);

			ivBg1 = (ImageView) backgroundDialog.findViewById(R.id.ivBg1);
			ivBg2 = (ImageView) backgroundDialog.findViewById(R.id.ivBg2);
			ivBg3 = (ImageView) backgroundDialog.findViewById(R.id.ivBg3);
			ivBg4 = (ImageView) backgroundDialog.findViewById(R.id.ivBg4);
			ivBg5 = (ImageView) backgroundDialog.findViewById(R.id.ivBg5);
			ivBg6 = (ImageView) backgroundDialog.findViewById(R.id.ivBg6);
			ivBg7 = (ImageView) backgroundDialog.findViewById(R.id.ivBg7);
			ivBg8 = (ImageView) backgroundDialog.findViewById(R.id.ivBg8);
			ivBg9 = (ImageView) backgroundDialog.findViewById(R.id.ivBg9);

			ivBg1.setOnClickListener(this);
			ivBg2.setOnClickListener(this);
			ivBg3.setOnClickListener(this);
			ivBg4.setOnClickListener(this);
			ivBg5.setOnClickListener(this);
			ivBg6.setOnClickListener(this);
			ivBg7.setOnClickListener(this);
			ivBg8.setOnClickListener(this);
			ivBg9.setOnClickListener(this);

			backgroundDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					changeFonts();
					backgroundDialog = null;
				}
			});
			backgroundDialog.show();
		}
	}

	private void choosePhotoFromGallary() {
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		activity.startActivityForResult(i, Const.CHOOSE_PHOTO, Const.FRAGMENT_APPEARANCE);

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case Const.CHOOSE_PHOTO:
			if (data != null) {
				if (data.getData() != null) {
					activity.setFbTag(Const.FRAGMENT_APPEARANCE);
					String filePath = activity.getRealPathFromURI(data.getData());
					preferenceHelper.putStyleFromGallary(filePath);
					// preferenceHelper.putStyle(0);
					styleDialog.dismiss();
				} else {
					Toast.makeText(activity, "unable to select image", Toast.LENGTH_LONG).show();
				}
			}
			break;

		default:
			break;
		}
	}

	private void showFontsDialog() {
		if (fontsDialog == null) {
			fontsDialog = new Dialog(activity);
			fontsDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			fontsDialog.setContentView(R.layout.dialog_fonts);

			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(fontsDialog.getWindow().getAttributes());
			lp.height = LayoutParams.MATCH_PARENT;
			lp.width = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;
			fontsDialog.getWindow().setAttributes(lp);

			tvFont1 = (TextView) fontsDialog.findViewById(R.id.tvFont1);
			tvFont2 = (TextView) fontsDialog.findViewById(R.id.tvFont2);
			tvFont3 = (TextView) fontsDialog.findViewById(R.id.tvFont3);
			tvFont4 = (TextView) fontsDialog.findViewById(R.id.tvFont4);
			tvFont5 = (TextView) fontsDialog.findViewById(R.id.tvFont5);
			tvFont6 = (TextView) fontsDialog.findViewById(R.id.tvFont6);
			tvFont7 = (TextView) fontsDialog.findViewById(R.id.tvFont7);
			tvFont8 = (TextView) fontsDialog.findViewById(R.id.tvFont8);
			tvFont9 = (TextView) fontsDialog.findViewById(R.id.tvFont9);
			tvFont10 = (TextView) fontsDialog.findViewById(R.id.tvFont10);

			tvFont1.setOnClickListener(this);
			tvFont2.setOnClickListener(this);
			tvFont3.setOnClickListener(this);
			tvFont4.setOnClickListener(this);
			tvFont5.setOnClickListener(this);
			tvFont6.setOnClickListener(this);
			tvFont7.setOnClickListener(this);
			tvFont8.setOnClickListener(this);
			tvFont9.setOnClickListener(this);
			tvFont10.setOnClickListener(this);

			tvFont1.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/BASKVILL.ttf"));
			tvFont2.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/BATANG.ttf"));
			tvFont3.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/BRADHITC.TTF"));
			tvFont4.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/CANDARA.TTF"));
			tvFont5.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/Century Gothic.ttf"));
			tvFont6.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/ClarendonBTRoman.ttf"));
			tvFont7.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/chinese.simfang.ttf"));
			tvFont8.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/HARNGTON.TTF"));
			tvFont9.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/lucida calligraphy italic.ttf"));
			tvFont10.setTypeface(Typeface.createFromAsset(activity.getAssets(), "fonts/Monotype Corsiva.ttf"));

			fontsDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					changeFonts();
					fontsDialog = null;
				}
			});

			fontsDialog.show();
		}
	}

	private void changeFonts() {
		//activity.setFontColor(tvChangeColor);
		// activity.setFontSize(tvChangeColor);
		activity.setFonts(tvChangeColor);

		//activity.setFontColor(tvChangeFont);
		// activity.setFontSize(tvChangeFont);
		activity.setFonts(tvChangeFont);

		//activity.setFontColor(tvChangeSize);
		// activity.setFontSize(tvChangeSize);
		activity.setFonts(tvChangeSize);

		//activity.setFontColor(tvChangeTextStyle);
		// activity.setFontSize(tvChangeTextStyle);
		activity.setFonts(tvChangeTextStyle);

		//activity.setFontColor(tvChangeBackGround);
		// activity.setFontSize(tvChangeBackGround);
		activity.setFonts(tvChangeBackGround);

		activity.setFonts(activity.tvTitle);
		// activity.setFontSize(activity.tvTitle);
		//activity.SetBackgroundColor(activity.relMain);

	//	activity.setBackground(llAppearance);
	}
}
