package com.example.opex_new.fragment;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.MemberListAdapter;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class GroupDetailFragment extends Fragment implements OnClickListener, OnItemClickListener {

	private GroupActivity activity;
	private TextView tvAddMemberGroupDetail, tvNumberOfMembers;
	private ListView lvMembers;
	private Button btnBack;
	private ArrayList<Member> memberList;
	private Group group;
	private MemberListAdapter memberListAdapter;
	private EditText etSearchMember;
	private LinearLayout llGroupDetail;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();

		group = (Group) getArguments().getSerializable(Const.GROUP);
		memberList = group.getMemberList();
		AppLog.Log("memberList", "memberList" + memberList.size());
		tvNumberOfMembers.setText(activity.getString(R.string.text_total_members) + memberList.size());
		activity.setActionBarTitle(group.getName());
		memberListAdapter = new MemberListAdapter(activity, memberList, true);

		lvMembers.setAdapter(memberListAdapter);
		lvMembers.setOnItemClickListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.fragment_group_detail, container, false);
		tvAddMemberGroupDetail = (TextView) view.findViewById(R.id.tvAddMemberGroupDetail);
		tvNumberOfMembers = (TextView) view.findViewById(R.id.tvNumberOfMembers);
		lvMembers = (ListView) view.findViewById(R.id.lvMembers);
		btnBack = (Button) view.findViewById(R.id.btnBack);
		etSearchMember = (EditText) view.findViewById(R.id.etSearchMember);
		llGroupDetail = (LinearLayout) view.findViewById(R.id.llGroupDetail);

		etSearchMember.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				memberListAdapter.filterMember().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		tvAddMemberGroupDetail.setOnClickListener(this);

		btnBack.setOnClickListener(this);

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	//	activity.SetBackgroundColor(etSearchMember);
		// activity.setFontSize(etSearchMember);
		activity.setFonts(etSearchMember);

		//activity.SetBackgroundColor(btnBack);
		// activity.setFontSize(btnBack);
		activity.setFonts(btnBack);

	activity.setBackground(llGroupDetail);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.tvAddMemberGroupDetail:
			// activity.dbHelper.addMember(member, group.getId());
			if (isValidate()) {
				activity.showAddMemberDialogGroupDetails(this, memberList);
			}
			break;

		case R.id.btnBack:
			if (isVisible()) {
				activity.removeThisFragment();
			}
			break;

		default:
			break;
		}
	}

	private boolean isValidate() {
		String msg = null;

		if (memberList.size() >= 25) {
			msg = activity.getString(R.string.toast_maximum_25);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}

		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		switch (parent.getId()) {
		case R.id.lvContact:
			activity.dbHelper.addMember((Member) parent.getItemAtPosition(position), group.getId());
			memberList.add((Member) parent.getItemAtPosition(position));
		
			memberListAdapter.notifyDataSetChanged();
			if (activity.addMemberDialog != null) {
				activity.addMemberDialog.dismiss();
			}
			tvNumberOfMembers.setText(activity.getString(R.string.text_total_members) + memberList.size());
	
		case R.id.lvMembers:

			break;

		default:
			break;
		}

	}
}
