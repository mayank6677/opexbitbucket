package com.example.opex_new.fragment;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.GroupListAdapter;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

public class CreateMessageFragment extends Fragment implements OnClickListener, OnItemClickListener {

	private GroupActivity activity;
	private Button btnBroadcastMsg, btnFeedbackMsg, btnTempMsg, btnWriteMsg, btnsendMsg, btnscheduledMsg, btnNormalMsg,
			btnExpressMsg, btnUrgentMsg, btnSendFeedback;
	private LinearLayout llFeedbackStatus, llBroadcast;
	private EditText etSearchGroupMsg;
	private ListView lvGroupListMsg;
	private ArrayList<Member> memberListForBroadcast;
	private ArrayList<Group> groupList/* , selectedGroupList */;
	private GroupListAdapter groupListAdapter;
	private Group selectedGroup;
	private boolean isBroadcast = true;
	private int msgStatus = Const.NORMAL;
	private LinearLayout llCreateMessage;
	private AlertDialog.Builder confirmationDialogBuilder;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		activity = (GroupActivity) getActivity();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.fragment_create_message, container, false);
		btnBroadcastMsg = (Button) view.findViewById(R.id.btnBroadcastMsg);
		btnFeedbackMsg = (Button) view.findViewById(R.id.btnFeedbackMsg);
		etSearchGroupMsg = (EditText) view.findViewById(R.id.etSearchGroupMsg);
		lvGroupListMsg = (ListView) view.findViewById(R.id.lvGroupListMsg);
		btnTempMsg = (Button) view.findViewById(R.id.btnTempMsg);
		btnWriteMsg = (Button) view.findViewById(R.id.btnWriteMsg);
		btnsendMsg = (Button) view.findViewById(R.id.btnsendMsg);
		btnscheduledMsg = (Button) view.findViewById(R.id.btnscheduledMsg);
		btnNormalMsg = (Button) view.findViewById(R.id.btnNormalMsg);
		btnExpressMsg = (Button) view.findViewById(R.id.btnExpressMsg);
		btnUrgentMsg = (Button) view.findViewById(R.id.btnUrgentMsg);
		btnSendFeedback = (Button) view.findViewById(R.id.btnSendFeedback);
		llFeedbackStatus = (LinearLayout) view.findViewById(R.id.llFeedbackStatus);
		llBroadcast = (LinearLayout) view.findViewById(R.id.llBroadcast);
		llCreateMessage = (LinearLayout) view.findViewById(R.id.llCreateMessage);

		etSearchGroupMsg.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				groupListAdapter.filterGroups().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		btnBroadcastMsg.setOnClickListener(this);
		btnFeedbackMsg.setOnClickListener(this);
		btnTempMsg.setOnClickListener(this);
		btnWriteMsg.setOnClickListener(this);
		btnsendMsg.setOnClickListener(this);
		btnscheduledMsg.setOnClickListener(this);
		btnNormalMsg.setOnClickListener(this);
		btnExpressMsg.setOnClickListener(this);
		btnUrgentMsg.setOnClickListener(this);
		btnSendFeedback.setOnClickListener(this);

		// btnBroadcastMsg.setSelected(true);
		// btnFeedbackMsg.setSelected(false);
//		btnBroadcastMsg.setTextColor(activity.getResources().getColor(R.color.white));
//		btnFeedbackMsg.setTextColor(activity.getResources().getColor(R.color.black));
	//	btnBroadcastMsg.setBackgroundColor(activity.preferenceHelper.getColor());
//		btnFeedbackMsg.setBackgroundColor(activity.getResources().getColor(R.color.white));

		// btnNormalMsg.setSelected(true);
		// btnExpressMsg.setSelected(false);
		// btnUrgentMsg.setSelected(false);
		msgStatus = Const.NORMAL;
//		btnNormalMsg.setTextColor(activity.getResources().getColor(R.color.white));
//		btnExpressMsg.setTextColor(activity.getResources().getColor(R.color.black));
//		btnUrgentMsg.setTextColor(activity.getResources().getColor(R.color.black));
	//	activity.SetBackgroundColor(btnNormalMsg);
		btnNormalMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
		selectedGroup = null;

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		//activity.SetBackgroundColor(etSearchGroupMsg);
		// activity.setFontSize(etSearchGroupMsg);
		activity.setFonts(etSearchGroupMsg);

		//activity.SetBackgroundColor(btnTempMsg);
		// activity.setFontSize(btnTempMsg);
		activity.setFonts(btnTempMsg);

		//activity.SetBackgroundColor(btnWriteMsg);
		// activity.setFontSize(btnWriteMsg);
		activity.setFonts(btnWriteMsg);
	
	
activity.setFontColor(btnSendFeedback);
activity.setFontColor(btnscheduledMsg);
activity.setFontColor(btnsendMsg);
activity.setFontColor(btnTempMsg);
activity.setFontColor(btnWriteMsg);


		//activity.SetBackgroundColor(btnSendFeedback);
		// activity.setFontSize(btnSendFeedback);
		activity.setFonts(btnSendFeedback);
		btnBroadcastMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
		btnNormalMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
		btnFeedbackMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
	activity.setBackground(llCreateMessage);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity.actionBar.hide();
		//activity.setActionBarTitle(activity.getString(R.string.title_create_message));
		memberListForBroadcast = new ArrayList<Member>();
		groupList = new ArrayList<Group>();
		// selectedGroupList = new ArrayList<Group>();
		groupListAdapter = new GroupListAdapter(activity, groupList, false);
		lvGroupListMsg.setAdapter(groupListAdapter);
		lvGroupListMsg.setOnItemClickListener(this);
		getGroups();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.btnBroadcastMsg:
			// btnBroadcastMsg.setSelected(true);
			// btnFeedbackMsg.setSelected(false);
//			btnBroadcastMsg.setTextColor(activity.getResources().getColor(R.color.white));
//			btnFeedbackMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			btnBroadcastMsg.setBackgroundColor(activity.preferenceHelper.getColor());
			btnBroadcastMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
		btnFeedbackMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));

			isBroadcast = true;
			btnsendMsg.setVisibility(View.VISIBLE);
			llBroadcast.setVisibility(View.VISIBLE);
			// btnWriteMsg.setVisibility(View.VISIBLE);
			btnSendFeedback.setVisibility(View.GONE);
			llFeedbackStatus.setVisibility(View.GONE);

			// for (int i = 0; i < memberListForBroadcast.size(); i++) {
			// activity.sendSMS(memberListForBroadcast.get(i).getPhone(), "");
			// }
			break;
		case R.id.btnFeedbackMsg:
			// btnBroadcastMsg.setSelected(false);
			// btnFeedbackMsg.setSelected(true);
//			btnBroadcastMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			btnFeedbackMsg.setTextColor(activity.getResources().getColor(R.color.white));
//			btnFeedbackMsg.setBackgroundColor(activity.preferenceHelper.getColor());
	btnBroadcastMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
			btnFeedbackMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
			isBroadcast = false;
			btnsendMsg.setVisibility(View.GONE);
			llBroadcast.setVisibility(View.GONE);
			// btnWriteMsg.setVisibility(View.GONE);
			btnSendFeedback.setVisibility(View.VISIBLE);
			llFeedbackStatus.setVisibility(View.VISIBLE);
			break;
		case R.id.btnTempMsg:
			if (isValidate()) {
				memberListForBroadcast.clear();
				// for (int i = 0; i < selectedGroupList.size(); i++) {
				// activity.dbHelper.getMembers(memberListForBroadcast,
				// selectedGroupList.get(i).getId());
				// AppLog.Log(Const.TAG, "" + memberListForBroadcast.size());
				// }
				activity.dbHelper.getMembers(memberListForBroadcast, selectedGroup.getId());
				activity.addTempleteFragment(true, selectedGroup, isBroadcast, msgStatus);
			}
			break;
		case R.id.btnWriteMsg:
			if (isValidate()) {
				memberListForBroadcast.clear();
				// for (int i = 0; i < selectedGroupList.size(); i++) {
				// activity.dbHelper.getMembers(memberListForBroadcast,
				// selectedGroupList.get(i).getId());
				// AppLog.Log(Const.TAG, "" + memberListForBroadcast.size());
				// }
				activity.dbHelper.getMembers(memberListForBroadcast, selectedGroup.getId());
				WriteMessageFragment fragment = new WriteMessageFragment();
				Bundle bundle = new Bundle();
				// bundle.putParcelableArrayList(Const.Params.MEMBER,
				// memberListForBroadcast);
				bundle.putSerializable(Const.Params.GROUP, selectedGroup);
				bundle.putBoolean(Const.ISBROADCAST, isBroadcast);
				bundle.putInt(Const.MSGSTATUS, msgStatus);
				fragment.setArguments(bundle);
				activity.addFragment(fragment, true, Const.FRAGMENT_WRITE_MESSAGE);
			}
			break;
		case R.id.btnsendMsg:

			break;
		case R.id.btnscheduledMsg:

			break;
		case R.id.btnNormalMsg:
			// btnNormalMsg.setSelected(true);
			// btnExpressMsg.setSelected(false);
//			// btnUrgentMsg.setSelected(false);
//			btnNormalMsg.setTextColor(activity.getResources().getColor(R.color.white));
//			btnExpressMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			btnUrgentMsg.setTextColor(activity.getResources().getColor(R.color.black));
			//activity.SetBackgroundColor(btnNormalMsg);
			btnNormalMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
		btnExpressMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
		btnUrgentMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
			msgStatus = Const.NORMAL;
			break;
		case R.id.btnExpressMsg:
			// btnNormalMsg.setSelected(false);
			// btnExpressMsg.setSelected(true);
			// btnUrgentMsg.setSelected(false);
//			btnNormalMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			btnExpressMsg.setTextColor(activity.getResources().getColor(R.color.white));
//			btnUrgentMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			activity.SetBackgroundColor(btnExpressMsg);
			btnExpressMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
		btnNormalMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
			btnUrgentMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
			msgStatus = Const.EXPRESS;
			break;
		case R.id.btnUrgentMsg:
			// btnNormalMsg.setSelected(false);
			// btnExpressMsg.setSelected(false);
			// btnUrgentMsg.setSelected(true);
//			btnNormalMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			btnExpressMsg.setTextColor(activity.getResources().getColor(R.color.black));
//			btnUrgentMsg.setTextColor(activity.getResources().getColor(R.color.white));
		//	activity.SetBackgroundColor(btnUrgentMsg);
			btnUrgentMsg.setBackgroundColor(activity.getResources().getColor(R.color.black_trans));
			btnNormalMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
			btnExpressMsg.setBackgroundColor(activity.getResources().getColor(R.color.skyblue));
			msgStatus = Const.URGENT;
			break;

		case R.id.btnSendFeedback:
			if (isValidate()) {
				if (msgStatus == Const.NORMAL) {
					goToFeedbackFragment();
				} else {
					showConfirmationDialog();
				}
			}
			break;
		default:
			break;
		}

	}

	private void goToFeedbackFragment() {
		FeedbackFragment feedbackFragment = new FeedbackFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(Const.Params.GROUP, selectedGroup);
		bundle.putInt(Const.MSGSTATUS, msgStatus);
		feedbackFragment.setArguments(bundle);
		activity.addFragment(feedbackFragment, true, Const.FRAGMENT_FEEDBACk);
	}

	private void showConfirmationDialog() {
		if (confirmationDialogBuilder == null) {
			confirmationDialogBuilder = new AlertDialog.Builder(activity);
			confirmationDialogBuilder.setCancelable(false);

			if (msgStatus == Const.EXPRESS) {
				confirmationDialogBuilder.setMessage(activity.getString(R.string.alert_express));
			} else if (msgStatus == Const.URGENT) {
				confirmationDialogBuilder.setMessage(activity.getString(R.string.alert_urgent));
			}

			confirmationDialogBuilder.setPositiveButton(activity.getString(R.string.text_ok),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							confirmationDialogBuilder = null;
							goToFeedbackFragment();
						}
					});
			confirmationDialogBuilder.setNegativeButton(activity.getString(R.string.text_cancel),
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							confirmationDialogBuilder = null;
						}
					});
			confirmationDialogBuilder.show();
		}
	}

	private void getGroups() {
		activity.dbHelper.getGroups(groupList);
		AppLog.Log("groupList", "groupList" + groupList.size());
		groupListAdapter.notifyDataSetChanged();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		selectedGroup = (Group) parent.getItemAtPosition(position);

		for (int i = 0; i < groupList.size(); i++) {
			groupList.get(i).isSelected = false;
			// view.setBackgroundColor(getResources().getColor(R.color.white));
		}

		// selectedGroupList.clear();
		// selectedGroupList.add(group);
		selectedGroup.isSelected = true;
		// view.setBackgroundColor(getResources().getColor(R.color.skyblue));
		groupListAdapter.notifyDataSetChanged();
	}

	private boolean isValidate() {
		String msg = null;

		if (/* selectedGroupList.size() <= 0 */selectedGroup == null) {
			msg = activity.getString(R.string.toast_selct_min_one_group);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}
		return true;
	}
}
