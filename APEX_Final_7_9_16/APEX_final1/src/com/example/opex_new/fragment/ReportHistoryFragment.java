package com.example.opex_new.fragment;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.ReportHistoryAdapter;
import com.example.opex_new.model.FeedbackPart;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ReportHistoryFragment extends Fragment implements OnClickListener {

	private GroupActivity activity;
	private Button btnOkReportHistory;
	private ListView lvReportHistory;
	private ArrayList<FeedbackPart> feedbackPartList;
	private ReportHistoryAdapter reportHistoryAdapter;
	LinearLayout  linear;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_report_history, container, false);
		lvReportHistory = (ListView) view.findViewById(R.id.lvReportHistory);
		btnOkReportHistory = (Button) view.findViewById(R.id.btnOkReportHistory);
linear=(LinearLayout) view.findViewById(R.id.llReporthistory);
		btnOkReportHistory.setOnClickListener(this);
		AppLog.Log(Const.TAG, "" + activity);
		reportHistoryAdapter = new ReportHistoryAdapter(activity, feedbackPartList);
		lvReportHistory.setAdapter(reportHistoryAdapter);
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//activity.SetBackgroundColor(btnOkReportHistory);
		// activity.setFontSize(btnOkReportHistory);
		activity.setFonts(btnOkReportHistory);
		activity.setBackground(linear);

		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		activity.onBackPressed();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.setActionBarTitle(getString(R.string.title_report_history));
		feedbackPartList = getArguments().getParcelableArrayList(Const.FEEDBACKPARTLIST);
	}
}
