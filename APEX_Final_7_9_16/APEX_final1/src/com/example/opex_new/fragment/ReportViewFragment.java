package com.example.opex_new.fragment;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PieChart;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;
import org.w3c.dom.NameList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.Feedback;

import android.app.ActionBar.LayoutParams;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ReportViewFragment extends Fragment implements OnClickListener {

	private GroupActivity activity;
	private TextView tvMsgTemplete, tvTotalSent, tvDelivered, tvFeedbackReceived, tvFeedbackPending;
	private Button btnOkReport, btnHistoryReport;
	private boolean isBroadcast;
	private Broadcast broadcast;
	private Feedback feedback;
	public TextView answerA,answerB,answerC,answerD,answerE;
	// private Question question;
	private LinearLayout llReport;
	private TextView tvFeedback, tvMessage;

	// pi chart

	private static int[] COLORS = new int[] { Color.RED, Color.BLUE, Color.GREEN, Color.YELLOW,
			Color.rgb(128, 0, 128) };

	private static int[] VALUES = new int[5];

	
	private static String[] NAME_LIST = new String[] { "A", "B", "C", "D", "E" };

	private CategorySeries mSeries = new CategorySeries("");

	private DefaultRenderer mRenderer = new DefaultRenderer();

	private GraphicalView mChartView;
	private LinearLayout layout;
	int piChartLength=0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	try{
		AppLog.Log("onCreateView", "onCreateView");
		View view = inflater.inflate(R.layout.fragment_report_view, container, false);
		tvMsgTemplete = (TextView) view.findViewById(R.id.tvMsgTemplete);
		tvTotalSent = (TextView) view.findViewById(R.id.tvTotalSent);
		tvDelivered = (TextView) view.findViewById(R.id.tvDelivered);
		tvFeedbackReceived = (TextView) view.findViewById(R.id.tvFeedbackReceived);
		tvFeedbackPending = (TextView) view.findViewById(R.id.tvFeedbackPending);
		btnOkReport = (Button) view.findViewById(R.id.btnOkReport);
		btnHistoryReport = (Button) view.findViewById(R.id.btnHistoryReport);
		llReport = (LinearLayout) view.findViewById(R.id.llReport);
		tvFeedback = (TextView) view.findViewById(R.id.tvFeedback);
		tvMessage = (TextView) view.findViewById(R.id.tvMessage);
	

		btnOkReport.setOnClickListener(this);
		btnHistoryReport.setOnClickListener(this);
		tvMsgTemplete.setMovementMethod(new ScrollingMovementMethod());

	
	

		// tvMsgTemplete.setText(getString(R.string.text_msg_templete) + " ");

		// tvTotalSent.setText(getString(R.string.text_total_msg_sent)
		// + broadcast.getSent());
		// tvDelivered.setText(getString(R.string.text_delevered)
		// + broadcast.getDelivered());
		// tvFeedbackReceived.setText(getString(R.string.text_Feedback_received)
		// + " ");
		// tvFeedbackPending.setText(getString(R.string.text_feedback_pending)
		// + " ");

		layout = (LinearLayout) view.findViewById(R.id.piChart);
		answerA = (TextView) view.findViewById(R.id.ansA);
		answerB = (TextView) view.findViewById(R.id.ansB);

		answerC = (TextView)view.findViewById(R.id.ansC);

		answerD = (TextView) view.findViewById(R.id.ansD);

		answerE = (TextView) view.findViewById(R.id.ansE);

		
		if (isBroadcast) {
			tvFeedbackPending.setVisibility(View.GONE);
			tvFeedbackReceived.setVisibility(View.GONE);
			tvFeedback.setVisibility(View.GONE);
			btnHistoryReport.setVisibility(View.GONE);
			tvTotalSent.setText(getString(R.string.text_total_msg_sent) + broadcast.getSent());
			tvDelivered.setText(getString(R.string.text_delevered) + broadcast.getDelivered());
			tvMsgTemplete
					.setText(/* getString(R.string.text_msg_templete) + " " + */broadcast.getMessage());

		} else {
			// tvTotalSent.setText(getString(R.string.text_total_msg_sent)
			// + question.getSend());
			// tvDelivered.setText(getString(R.string.text_delevered)
			// + question.getDelivered());
			// tvFeedbackReceived
			// .setText(getString(R.string.text_Feedback_received)
			// + question.getReceived());
			// tvMsgTemplete.setText(getString(R.string.text_msg_templete) + " "
			// + question.getMsg());
			// if (question.getReceived() == 0) {
			// layout.setVisibility(View.INVISIBLE);
			// }
			//
			// if (question.getSend() - question.getReceived() < 0) {
			// // tvFeedbackReceived
			// // .setText(getString(R.string.text_Feedback_received)
			// // + feedback.getSent());
			// tvFeedbackPending
			// .setText(getString(R.string.text_feedback_pending)
			// + "0");
			// } else {
			// tvFeedbackPending
			// .setText(getString(R.string.text_feedback_pending)
			// + (question.getSend() - question.getReceived()));
			// }

			btnHistoryReport.setVisibility(View.VISIBLE);
			layout.setVisibility(View.VISIBLE);
			tvTotalSent.setText(getString(R.string.text_total_msg_sent) + feedback.getSent());
			tvDelivered.setText(getString(R.string.text_delevered) + feedback.getDelivered());
			tvFeedbackReceived.setText(getString(R.string.text_Feedback_received) + feedback.getReceived());
			tvMsgTemplete.setText(getString(R.string.text_msg_templete) + " " + feedback.getMessgae().substring(5));

			tvMsgTemplete.setGravity(Gravity.CENTER_VERTICAL);
			if (feedback.getReceived() == 0) {
				layout.setVisibility(View.INVISIBLE);
			}

			if (feedback.getSent() - feedback.getReceived() < 0) {
				// tvFeedbackReceived
				// .setText(getString(R.string.text_Feedback_received)
				// + feedback.getSent());
				tvFeedbackPending.setText(getString(R.string.text_feedback_pending) + "0");
			} else {
				tvFeedbackPending.setText(
						getString(R.string.text_feedback_pending) + (feedback.getSent() - feedback.getReceived()));
			}

		}
		for (int i = 0; i < piChartLength; i++) {
			if(i == 0)
			{
				//answerA.append(""+feedback.getId());
				answerA.setText(NAME_LIST[i] + "-" + VALUES[i]);
				answerA.setVisibility(View.VISIBLE);
				
			}
			else if(i == 1)
			{
				//answerA.append(""+feedback.getId());
				answerB.setText(NAME_LIST[i] + "-" + VALUES[i]);
				answerB.setVisibility(View.VISIBLE);
			}
		
			else if(i == 2)
			{
				//answerA.append(""+feedback.getId());
				answerC.setText(NAME_LIST[i] + "-" + VALUES[i]);
				answerC.setVisibility(View.VISIBLE);
			}
				
			else if(i == 3)
			{
				//answerA.append(""+feedback.getId());
				answerD.setText(NAME_LIST[i] + "-" + VALUES[i]);
				answerD.setVisibility(View.VISIBLE);
			
				
			}
			else if(i == 4)
			{
				//answerA.append(""+feedback.getId());
				answerE.setText(NAME_LIST[i] + "-" + VALUES[i]);
				answerE.setVisibility(View.VISIBLE);
				
			}
			
		}
		return view;
	}catch (Exception e) {
		
		
		e.printStackTrace();
		// TODO: handle exception
	}
	return container;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		AppLog.Log("onCreate", "onCreate");
		try{
		super.onCreate(savedInstanceState);
		isBroadcast = getArguments().getBoolean(Const.ISBROADCAST);
		if (isBroadcast) {
			broadcast = (Broadcast) getArguments().getSerializable(Const.BROADCASTPART);
		} else {
			// question = (Question) getArguments()
			// .getSerializable(Const.QUESTION);

			feedback = (Feedback) getArguments().getSerializable(Const.FEEDBACK);
			AppLog.Log(Const.TAG, "FeedbackReport Feed:"+feedback);
			// VALUES[0] = question.getAnswerA();
			// VALUES[1] = question.getAnswerB();
			// VALUES[2] = question.getAnswerC();

			VALUES[0] = feedback.getAnswerA();
			VALUES[1] = feedback.getAnswerB();
			VALUES[2] = feedback.getAnswerC();
			VALUES[3] = feedback.getAnswerD();
			VALUES[4] = feedback.getAnswerE();
			AppLog.Log(Const.TAG, "FeedbackReport VALUES[0]:"+VALUES[0]);
			AppLog.Log(Const.TAG, "FeedbackReport VALUES[1]:"+VALUES[1]);
			AppLog.Log(Const.TAG, "FeedbackReport VALUES[2]:"+VALUES[2]);
			// if (question.getAnswer() != null) {
			// if (question.getAnswer().equals("a")) {
			// VALUES[0] = 1;
			// } else if (question.getAnswer().equals("b")) {
			// VALUES[1] = 1;
			// } else if (question.getAnswer().equals("c")) {
			// VALUES[2] = 1;
			// }
			// }
			// VALUES[0] = feedback.getAnswerA();
			// VALUES[1] = feedback.getAnswerB();
			// VALUES[2] = feedback.getAnswerC();
			//
			// mRenderer.setApplyBackgroundColor(true);
			// mRenderer.setBackgroundColor(Color.argb(100, 50, 50, 50));
			// mRenderer.setChartTitleTextSize(20);
			// mRenderer.setLabelsTextSize(15);
			// mRenderer.setLegendTextSize(15);
			// mRenderer.setMargins(new int[] { 20, 30, 15, 0 });
			// mRenderer.setZoomButtonsVisible(true);
			// mRenderer.setStartAngle(90);

			 piChartLength = 2;
				if (feedback.getMessgae().contains("E.")) {
				piChartLength = 5;
			} else if (feedback.getMessgae().contains("D.")) {
				piChartLength = 4;
			} else if (feedback.getMessgae().contains("C.")) {
				piChartLength = 3;
			}
			AppLog.Log(Const.TAG, "FeedbackReport feedback.getMessgae()"+feedback.getMessgae());
			AppLog.Log(Const.TAG, "FeedbackReport VALUES[3]:"+feedback.getReceived());
			AppLog.Log(Const.TAG, "FeedbackReport VALUES[4]:"+VALUES[4]);
			
			for (int i = 0; i < piChartLength; i++) {
				
					
				
				mSeries.add(NAME_LIST[i] + "-" + VALUES[i], VALUES[i]);
		
				SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
				renderer.setColor(
						COLORS[i
						       
//								 * (mSeries.getItemCount() - 1) % piChartLength
								]);
				
				mRenderer.addSeriesRenderer(renderer);
			}

			if (mChartView != null) {
				mChartView.repaint();
			}
		}
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
	
		activity = (GroupActivity) getActivity();
activity.actionBar.hide();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnOkReport:
			activity.onBackPressed();
			break;
		case R.id.btnHistoryReport:
			ReportHistoryFragment reportHistoryFragment = new ReportHistoryFragment();
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(Const.FEEDBACKPARTLIST, feedback.getFeedbackPartList());
			reportHistoryFragment.setArguments(bundle);
			activity.addFragment(reportHistoryFragment, true, Const.FRAGMENT_REPORT_HISTORY);
			break;
		default:
			break;
		}

	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		AppLog.Log("onResume", "FeedbackReport onResume");
		super.onResume();

	//	activity.SetBackgroundColor(tvMessage);
		// activity.setFontSize(tvMessage);
		activity.setFonts(tvMessage);

		//activity.SetBackgroundColor(tvFeedback);
		// activity.setFontSize(tvFeedback);
		activity.setFonts(tvFeedback);

		//activity.setFontColor(tvMsgTemplete);
		// activity.setFontSize(tvMsgTemplete);
		activity.setFonts(tvMsgTemplete);

	//	activity.SetBackgroundColor(tvTotalSent);
		// activity.setFontSize(tvTotalSent);
		activity.setFonts(tvTotalSent);

	//	activity.SetBackgroundColor(tvDelivered);
		// activity.setFontSize(tvDelivered);
		activity.setFonts(tvDelivered);

	//	activity.SetBackgroundColor(tvFeedbackReceived);
		// activity.setFontSize(tvFeedbackReceived);
		activity.setFonts(tvFeedbackReceived);

		//activity.SetBackgroundColor(tvFeedbackPending);
		// activity.setFontSize(tvFeedbackPending);
		activity.setFonts(tvFeedbackPending);

	//	activity.SetBackgroundColor(btnOkReport);
		// activity.setFontSize(btnOkReport);
		activity.setFonts(btnOkReport);
activity.setFontColor(btnOkReport);
activity.setFontColor(btnHistoryReport);
		//activity.SetBackgroundColor(btnHistoryReport);
		// activity.setFontSize(btnHistoryReport);
		activity.setFonts(btnHistoryReport);
		
		activity.setBackground(llReport);

		if (!isBroadcast) {
		
			if (mChartView == null) {
				AppLog.Log("mChartView", "null");
				mChartView = ChartFactory.getPieChartView(activity, mSeries, mRenderer);
				// mRenderer.setClickEnabled(true);
				mRenderer.setSelectableBuffer(10);
				mRenderer.setPanEnabled(false);
			//	mRenderer.setLabelsColor(new PreferenceHelper(activity).getColor());
				mRenderer.setLabelsTextSize(activity.getResources().getDimension(R.dimen.size_12));
			//	mRenderer.setLabelsColor(new PreferenceHelper(activity).getColor());
				mRenderer.setLabelsColor(Color.BLACK);
				mRenderer.setShowLegend(false);
				mRenderer.setFitLegend(false);
			
		
				mRenderer.setLegendHeight(50);
				
				//mRenderer.getLegend().setIconHeight(20);
			
				mRenderer.setZoomEnabled(false);
				mRenderer.setLegendTextSize(activity.getResources().getDimension(R.dimen.size_14));

				layout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			} else {
				AppLog.Log("mChartView", "not null");
				mChartView.repaint();
			}
		}
		
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mChartView = null;
	}
}
