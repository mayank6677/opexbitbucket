package com.example.opex_new.fragment;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.GroupListAdapter;
import com.example.opex_new.model.Group;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ViewGroupFragment extends Fragment implements OnClickListener, OnItemClickListener {

	private ListView lvGroups;
	private Button btnNewGroup;
	private ArrayList<Group> groupList;
	// private ArrayList<String> groupNameList;
	private ImageView ivNoItem;
	// private ArrayAdapter<String> groupAdapter;
	private GroupListAdapter groupListAdapter;
	private GroupActivity activity;
	private EditText etSearchGroup;
	private LinearLayout llViewGroup;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	
		View view = inflater.inflate(R.layout.fragment_view_group, container, false);

		lvGroups = (ListView) view.findViewById(R.id.lvGroups);
		btnNewGroup = (Button) view.findViewById(R.id.btnNewGroup);
		llViewGroup = (LinearLayout) view.findViewById(R.id.llViewGroup);
		btnNewGroup.setOnClickListener(this);
		ivNoItem = (ImageView) view.findViewById(R.id.ivNoItem);
		etSearchGroup = (EditText) view.findViewById(R.id.etSearchGroup);

		etSearchGroup.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				groupListAdapter.filterGroups().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		lvGroups.setOnItemClickListener(this);

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	//	activity.SetBackgroundColor(etSearchGroup);
		// activity.setFontSize(etSearchGroup);
		activity.setFonts(etSearchGroup);

		//activity.SetBackgroundColor(btnNewGroup);
		// activity.setFontSize(btnNewGroup);
		activity.setFontColor(etSearchGroup);
		activity.setFontColor(btnNewGroup);
		activity.setFonts(btnNewGroup);
		
	
		activity.setBackground(llViewGroup);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		//activity.setActionBarTitle(getString(R.string.title_view_groups));
		groupList = new ArrayList<Group>();
		// groupNameList = new ArrayList<String>();
		// groupAdapter = new ArrayAdapter<String>(activity,
		// android.R.layout.simple_list_item_1, groupNameList);
		groupListAdapter = new GroupListAdapter(activity, groupList, true);
		lvGroups.setAdapter(groupListAdapter);
		getGroups();
		activity.addMemberDialog=null;
		
		View searchBar = activity.findViewById(R.id.etSearchContact);
		if(searchBar!=null)
		((ViewGroup) searchBar.getParent()).removeView(searchBar);

	}

	private void getGroups() {
		activity.dbHelper.getGroups(groupList);
		AppLog.Log("groupList", "groupList" + groupList.size());
		groupListAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		activity.addFragment(new CreateGroupFragment(), true, Const.FRAGMENT_CREATE_GROUP);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		GroupDetailFragment groupDetailFragment = new GroupDetailFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(Const.GROUP, (Group) parent.getItemAtPosition(position));
		groupDetailFragment.setArguments(bundle);
		activity.addFragment(groupDetailFragment, true, Const.FRAGMENT_GROUP_DETAIL);
	}
}
