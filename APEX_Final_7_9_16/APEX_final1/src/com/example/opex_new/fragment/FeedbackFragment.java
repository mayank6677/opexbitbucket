package com.example.opex_new.fragment;

import java.util.Calendar;
import java.util.Date;

import com.example.opex_new.DashBoardActivity;
import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.Group;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FeedbackFragment extends Fragment implements OnClickListener {
	private GroupActivity activity;
	private LinearLayout llFeedback;
	private EditText etQuestion, etOptionA, etOptionB, etOptionC, etOptionD, etOptionE;
	private Button btnAddMore, btnSendFeedback, btnSchedualFeedback, btnAddMoreOption;
	private LinearLayout llOptionC, llOptionD, llOptionE;
	private TextView questionNo;
	private String message = "";
	private int msgStatus;
	private Group selectedGroup;
	private TextView tvSizeMsg;
	private AlertDialog.Builder confirmationDialogBuilder;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_feedback, container, false);
		// tvSizeMsg = (TextView) view.findViewById(R.id.tvSizeMsg1);

		llFeedback = (LinearLayout) view.findViewById(R.id.llFeedback);
		btnAddMore = (Button) view.findViewById(R.id.btnAddMore);
		btnSendFeedback = (Button) view.findViewById(R.id.btnSendFeedback);
		btnSchedualFeedback = (Button) view.findViewById(R.id.btnSchedualFeedback);
		btnSendFeedback.setOnClickListener(this);
		btnSchedualFeedback.setOnClickListener(this);
		btnAddMore.setOnClickListener(this);

		// tvSizeMsg.setText(Const.MAX_MSG_SIZE + "/" + 1);

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	//	activity.SetBackgroundColor(btnSendFeedback);
		// activity.setFontSize(etSearchGroupMsg);
		activity.setFonts(btnSendFeedback);
		activity.setFontColor(btnSendFeedback);

	//	activity.SetBackgroundColor(btnSchedualFeedback);
		// activity.setFontSize(etSearchGroupMsg);
		activity.setFonts(btnSchedualFeedback);
		activity.setFontColor(btnSchedualFeedback);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		addQuestion();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		selectedGroup = (Group) getArguments().getSerializable(Const.Params.GROUP);
		msgStatus = getArguments().getInt(Const.MSGSTATUS);
		
	}

	private void addQuestion() {
		LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View childView = inflater.inflate(R.layout.item_feedback, null);
		tvSizeMsg = (TextView) childView.findViewById(R.id.tvSizeMsg1);
		etQuestion = (EditText) childView.findViewById(R.id.etQuestion);
		etOptionA = (EditText) childView.findViewById(R.id.etOptionA);
		etOptionB = (EditText) childView.findViewById(R.id.etOptionB);
		etOptionC = (EditText) childView.findViewById(R.id.etOptionC);
		etOptionD = (EditText) childView.findViewById(R.id.etOptionD);
		etOptionE = (EditText) childView.findViewById(R.id.etOptionE);
		btnAddMoreOption = (Button) childView.findViewById(R.id.btnAddMoreOption);
		llOptionC = (LinearLayout) childView.findViewById(R.id.llOptionC);
		llOptionD = (LinearLayout) childView.findViewById(R.id.llOptionD);
		llOptionE = (LinearLayout) childView.findViewById(R.id.llOptionE);
		etQuestion.requestFocus();
		etQuestion.setSelection(etQuestion.length());

		btnAddMoreOption.setOnClickListener(this);

		questionNo = (TextView) childView.findViewById(R.id.questionNo);
		tvSizeMsg.setText(Const.MAX_MSG_SIZE + "/" + 1);
		etQuestion.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

				tvSizeMsg.setText(Const.MAX_MSG_SIZE - (s.length() % Const.MAX_MSG_SIZE) + "/"
						+ ((s.length() / Const.MAX_MSG_SIZE) + 1));
			}
		});

		// questionNo.setText((llFeedback.getChildCount() + 1) + "");
		llFeedback.addView(childView);
	}

	private boolean isValidate() {
		String msg = null;

		if (TextUtils.isEmpty(etQuestion.getText().toString())) {
			msg = getString(R.string.enter_question);
			etQuestion.requestFocus();
		} else if (TextUtils.isEmpty(etOptionA.getText().toString())
				|| TextUtils.isEmpty(etOptionB.getText().toString())) {
			msg = getString(R.string.enter_answer);
		} else if (TextUtils.isEmpty(etOptionC.getText().toString())
				&& (!TextUtils.isEmpty(etOptionD.getText().toString())
						|| !TextUtils.isEmpty(etOptionE.getText().toString()))) {
			msg = getString(R.string.enter_previous_answer);
		} else if (TextUtils.isEmpty(etOptionD.getText().toString())
				&& !TextUtils.isEmpty(etOptionE.getText().toString())) {
			msg = getString(R.string.enter_previous_answer);
		}

		if (msg != null) {
			Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
			return false;

		}
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.btnSendFeedback:
			if (isValidate()) {

				// if (msgStatus == Const.NORMAL) {
				sendFeedbackAndStore();
				Intent intent=new Intent(activity,DashBoardActivity.class);
				activity.startActivity(intent);

				// } else {
				// showConfirmationDialog(false);
				// }
			}
			break;
		case R.id.btnSchedualFeedback:
			if (isValidate()) {
				// if (msgStatus == Const.NORMAL) {
				schedualFeedbackAndStore();
				
				// } else {
				// showConfirmationDialog(true);
				// }
			}
			break;
		case R.id.btnAddMore:
			addQuestion();
			break;
		case R.id.btnAddMoreOption:
			AppLog.Log(Const.TAG, "btnAddMoreOption" + etOptionC.getVisibility() + View.VISIBLE);
			if (llOptionC.getVisibility() == View.GONE) {
				llOptionC.requestFocus();
			
				llOptionC.setVisibility(View.VISIBLE);
				return;
			} else if (llOptionD.getVisibility() == View.GONE) {
				llOptionD.requestFocus();
				llOptionD.setVisibility(View.VISIBLE);
				return;
			} else if (llOptionE.getVisibility() == View.GONE) {
				llOptionE.requestFocus();
				
				llOptionE.setVisibility(View.VISIBLE);
				btnAddMoreOption.setVisibility(View.GONE);
			}

			break;
		default:
			break;
		}
	}

	private void makeMessage(long[] ids) {
		for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
			message = "";
			for (int j = 0; j < llFeedback.getChildCount(); j++) {
				etQuestion = (EditText) llFeedback.getChildAt(j).findViewById(R.id.etQuestion);
				etOptionA = (EditText) llFeedback.getChildAt(j).findViewById(R.id.etOptionA);
				etOptionB = (EditText) llFeedback.getChildAt(j).findViewById(R.id.etOptionB);
				etOptionC = (EditText) llFeedback.getChildAt(j).findViewById(R.id.etOptionC);
				etOptionD = (EditText) llFeedback.getChildAt(j).findViewById(R.id.etOptionD);
				etOptionE = (EditText) llFeedback.getChildAt(j).findViewById(R.id.etOptionE);

				message = message.concat(/* (j + 1) + */"Que. " + etQuestion.getText().toString() + "\n" + "A. "
						+ etOptionA.getText().toString() + "\n" + "B. " + etOptionB.getText().toString() + "\n");

				if (!TextUtils.isEmpty(etOptionE.getText().toString())) {
					message = message.concat("C. " + etOptionC.getText().toString() + "\n" + "D. "
							+ etOptionD.getText().toString() + "\n" + "E. " + etOptionE.getText().toString() + "\n");
				} else if (!TextUtils.isEmpty(etOptionD.getText().toString())) {
					message = message.concat("C. " + etOptionC.getText().toString() + "\n" + "D. "
							+ etOptionD.getText().toString() + "\n");
				} else if (!TextUtils.isEmpty(etOptionC.getText().toString())) {
					message = message.concat("C. " + etOptionC.getText().toString() + "\n");
				}

				// if (i == 0) {
				// Question question = new Question();
				//
				// // feedback id added
				// question.setFeedbackId(ids[0]);
				// question.setMsg((j + 1) + ". "
				// + etQuestion.getText().toString() + "\n" + "A. "
				// + etOptionA.getText().toString() + "\n" + "B. "
				// + etOptionB.getText().toString() + "\n" + "C. "
				// + etOptionC.getText().toString());
				// question.setQuestionNo(j + 1);
				// Calendar calendar = Calendar.getInstance();
				// Date date = calendar.getTime();
				// String time = activity.formatDateTime(date);
				// question.setTime(time);
				// activity.dbHelper.addQuestion(question);
				// }

			}

			message = message.concat("Reply as: " + "A-" +  ids[0]);
			// for (int j = 0; j < llFeedback.getChildCount() - 1; j++) {
			// message = message.concat("<" + (j + 1) + ">-A#");
			// }
			// message = message.concat("<" + llFeedback.getChildCount() +
			// ">-A");
			activity.dbHelper.addMessageInFeedbackPart(message, ids[i + 1]);
		}
		activity.dbHelper.addMessageInFeedback(message, ids[0]);
	}

	private void sendFeedbackAndStore() {
		long[] ids = new long[101];
		AndyUtils.showSimpleProgressDialog(activity);
		activity.addFeedback(selectedGroup, ids, msgStatus);
		for (int i = 0; i < selectedGroup.getMemberList().size(); i++) {
			message = "";
			for (int j = 0; j < llFeedback.getChildCount(); j++) {
				// etQuestion = (EditText) llFeedback.getChildAt(j)
				// .findViewById(R.id.etQuestion);
				// etOptionA = (EditText) llFeedback.getChildAt(j)
				// .findViewById(R.id.etOptionA);
				// etOptionB = (EditText) llFeedback.getChildAt(j)
				// .findViewById(R.id.etOptionB);
				// etOptionC = (EditText) llFeedback.getChildAt(j)
				// .findViewById(R.id.etOptionC);

				message = message.concat(/* (j + 1) + */"Que. " + etQuestion.getText().toString() + "\n" + "A. "
						+ etOptionA.getText().toString() + "\n" + "B. " + etOptionB.getText().toString() + "\n");

				if (!TextUtils.isEmpty(etOptionE.getText().toString())) {
					message = message.concat("C. " + etOptionC.getText().toString() + "\n" + "D. "
							+ etOptionD.getText().toString() + "\n" + "E. " + etOptionE.getText().toString() + "\n");
				} else if (!TextUtils.isEmpty(etOptionD.getText().toString())) {
					message = message.concat("C. " + etOptionC.getText().toString() + "\n" + "D. "
							+ etOptionD.getText().toString() + "\n");
				} else if (!TextUtils.isEmpty(etOptionC.getText().toString())) {
					message = message.concat("C. " + etOptionC.getText().toString() + "\n");
				}

				AppLog.Log(Const.TAG,
						"msg:" + (j + 1) + ". " + etQuestion.getText().toString() + "\n" + "A. "
								+ etOptionA.getText().toString() + "\n" + "B. " + etOptionB.getText().toString() + "\n"
								+ "C. " + etOptionC.getText().toString() + "\n");

				// if (i == 0) {
				// Question question = new Question();
				//
				// // feedback id added
				// question.setFeedbackId(ids[0]);
				// question.setMsg((j + 1) + ". "
				// + etQuestion.getText().toString() + "\n" + "A. "
				// + etOptionA.getText().toString() + "\n" + "B. "
				// + etOptionB.getText().toString() + "\n" + "C. "
				// + etOptionC.getText().toString());
				// question.setQuestionNo(j + 1);
				// Calendar calendar = Calendar.getInstance();
				// Date date = calendar.getTime();
				// String time = activity.formatDateTime(date);
				// question.setTime(time);
				// activity.dbHelper.addQuestion(question);
				// }
			}

		//	message = message.concat("Reply as: " + ids[0] + "-A");
			message = message.concat("Reply as: " + "A-" +  ids[0]);
			// for (int j = 0; j < llFeedback.getChildCount() - 1; j++)
			// {
			// message = message.concat("<" + (j + 1) + ">-A#");
			// }
			// message = message.concat("<" + llFeedback.getChildCount()
			// + ">-A");
			AppLog.Log(Const.TAG, "message" + message);

			activity.setSendDelvereIntentsFeedback(ids[0], ids[i + 1]);
			activity.dbHelper.addMessageInFeedbackPart(message, ids[i + 1]);

			// try {
			// activity.sendSMS(selectedGroup.getMemberList().get(i)
			// .getPhone(), message, activity.sendPendingIntent,
			// activity.deliveredPendingIntent);
			// Thread.sleep(3000);

			/*
			 * final String phoneNo = selectedGroup.getMemberList().get(i)
			 * .getPhone(); final String finalMessage = message; Handler handler
			 * = new Handler(); handler.postDelayed(new Runnable() {
			 * 
			 * @Override public void run() { // TODO Auto-generated method stub
			 * activity.sendSMS(phoneNo, finalMessage,
			 * activity.sendPendingIntent, activity.deliveredPendingIntent); }
			 * }, 3000);
			 */
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
		
			
		}
		activity.sendMultipleSMS(selectedGroup.getMemberList(), message, activity.sendPendingIntent,
				activity.deliveredPendingIntent);

		activity.dbHelper.addMessageInFeedback(message, ids[0]);
		//activity.onBackPressed();
		
		

	}
	
	private void schedualFeedbackAndStore() {
		long[] ids = new long[101];
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
		Date date = calendar.getTime();
		AppLog.Log(Const.TAG, "btnSchedualFeedback" + date);
		activity.addSchedualFeedback(selectedGroup, ids, msgStatus, date);
		makeMessage(ids);
		AppLog.Log(Const.TAG, "selectedGroup" + selectedGroup);
		System.out.println("the dialog box appers here");
		
		activity.showSchedualMessageDialog(false, selectedGroup, message, msgStatus, false, ids[0]);
		
	}

	// private void showConfirmationDialog(final boolean isSchedual) {
	// if (confirmationDialogBuilder == null) {
	// confirmationDialogBuilder = new AlertDialog.Builder(activity);
	// confirmationDialogBuilder.setCancelable(false);
	//
	// if (msgStatus == Const.EXPRESS) {
	// confirmationDialogBuilder.setMessage(activity
	// .getString(R.string.alert_express));
	// } else if (msgStatus == Const.URGENT) {
	// confirmationDialogBuilder.setMessage(activity
	// .getString(R.string.alert_urgent));
	// }
	//
	// confirmationDialogBuilder.setPositiveButton(
	// activity.getString(R.string.text_ok),
	// new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// if (isSchedual) {
	// schedualFeedbackAndStore();
	// } else {
	// sendFeedbackAndStore();
	// }
	// dialog.dismiss();
	// confirmationDialogBuilder = null;
	// }
	// });
	// confirmationDialogBuilder.setNegativeButton(
	// activity.getString(R.string.text_cancel),
	// new DialogInterface.OnClickListener() {
	//
	// @Override
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// confirmationDialogBuilder = null;
	// }
	// });
	// confirmationDialogBuilder.show();
	// }
	// }
}
