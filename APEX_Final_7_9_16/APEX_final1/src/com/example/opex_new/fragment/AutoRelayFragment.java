package com.example.opex_new.fragment;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.GroupListAdapter;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;
import com.example.opex_new.model.Relay;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AutoRelayFragment extends Fragment implements OnClickListener, OnItemClickListener {

	private GroupActivity activity;
	private TextView etNumberAutoRelay, tvAddGroupAutoRelay;
	private EditText etPrefixAutoRelay;
	private ListView lvGroupsAutoRelay;
	private Button btnSubmitAutoRelay, btnCancelAutoRelay;
	private GroupListAdapter groupListAdapter;
	private ArrayList<Group> groupList;
	private Group selectedGroup;
	ImageView back;
	// private Group selectedGroup;
	private Member member;
	private LinearLayout llAutoRelay;
	private Dialog dialog;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_auto_relay, container, false);
		etNumberAutoRelay = (TextView) view.findViewById(R.id.etNumberAutoRelay);
		etPrefixAutoRelay = (EditText) view.findViewById(R.id.etPrefixAutoRelay);
		tvAddGroupAutoRelay = (TextView) view.findViewById(R.id.tvAddGroupAutoRelay);
		lvGroupsAutoRelay = (ListView) view.findViewById(R.id.lvGroupsAutoRelay);
		btnSubmitAutoRelay = (Button) view.findViewById(R.id.btnSubmitAutoRelay);
		btnCancelAutoRelay = (Button) view.findViewById(R.id.btnCancelAutoRelay);
		llAutoRelay = (LinearLayout) view.findViewById(R.id.llAutoRelay);
		

		tvAddGroupAutoRelay.setOnClickListener(this);
		etNumberAutoRelay.setOnClickListener(this);
		lvGroupsAutoRelay.setAdapter(groupListAdapter);
		lvGroupsAutoRelay.setOnItemClickListener(this);
		btnSubmitAutoRelay.setOnClickListener(this);
		btnCancelAutoRelay.setOnClickListener(this);
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		activity = (GroupActivity) getActivity();
		groupList = new ArrayList<Group>();

		groupListAdapter = new GroupListAdapter(activity, groupList, false);
		

		getGroups();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		//activity.SetBackgroundColor(etNumberAutoRelay);
		// activity.setFontSize(etNumberAutoRelay);
		activity.setFonts(etNumberAutoRelay);

		//activity.SetBackgroundColor(etPrefixAutoRelay);
		// activity.setFontSize(etPrefixAutoRelay);
		activity.setFonts(etPrefixAutoRelay);

	//	activity.SetBackgroundColor(btnSubmitAutoRelay);
		// activity.setFontSize(btnSubmitAutoRelay);
		activity.setFonts(btnSubmitAutoRelay);
activity.setFontColor(btnSubmitAutoRelay);
activity.setFontColor(btnCancelAutoRelay);
	//	activity.SetBackgroundColor(btnCancelAutoRelay);
		// activity.setFontSize(btnCancelAutoRelay);
		activity.setFonts(btnCancelAutoRelay);

	//	activity.SetBackgroundColor(tvAddGroupAutoRelay);
		activity.setFonts(tvAddGroupAutoRelay);

	activity.setBackground(llAutoRelay);

	}

	private void getGroups() {
		activity.dbHelper.getGroups(groupList);
		AppLog.Log("groupList", "groupList" + groupList.size());
		groupListAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.etNumberAutoRelay:
			activity.showAddMemberDialog(this, null);
			break;
		case R.id.btnSubmitAutoRelay:
			if (isValidate()) {
				addAutoRelay();
			}
			break;
		case R.id.btnCancelAutoRelay:
			activity.onBackPressed();
			break;
		case R.id.tvAddGroupAutoRelay:
			showSelectGroupDialog();
			break;

		default:
			break;
		}

	}

	private boolean isValidate() {
		String msg = null;
		if (TextUtils.isEmpty(etNumberAutoRelay.getText().toString())) {
			msg = activity.getString(R.string.toast_enter_number);
		} else if (TextUtils.isEmpty(etPrefixAutoRelay.getText().toString())) {
			msg = activity.getString(R.string.toast_enter_preix);
		} else if (selectedGroup == null) {
			msg = activity.getString(R.string.toast_selct_group);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}
		return true;
	}

	public void addAutoRelay() {
		Relay relay = new Relay();
		relay.setNumber(member.getPhone());
		relay.setName(member.getName());
		relay.setPrefix(etPrefixAutoRelay.getText().toString());
		relay.setGroup(selectedGroup);
		activity.dbHelper.addAutoRelay(relay);

		activity.onBackPressed();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub

		switch (parent.getId()) {
		case R.id.lvGroupsAutoRelay:
			AppLog.Log(Const.TAG, "onItemClick" + groupList.size());
			for (int i = 0; i < groupList.size(); i++) {
				groupList.get(i).isSelected = false;
			}
			selectedGroup = groupList.get(position);
			groupList.get(position).isSelected = true;
			groupListAdapter.notifyDataSetChanged();
			break;

		case R.id.lvContact:
			member = (Member) parent.getItemAtPosition(position);
			etNumberAutoRelay.setText(member.getName().toString());
			if (activity.addMemberDialog != null) {
				activity.addMemberDialog.dismiss();
			}
			break;
		case R.id.lvSelectGroup:
			selectedGroup = groupList.get(position);
			tvAddGroupAutoRelay.setText(selectedGroup.getName());
			dialog.dismiss();
			break;
		default:
			break;
		}

	}

	private void showSelectGroupDialog() {
		dialog = new Dialog(activity);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.dialog_select_group);
		ListView lvSelectGroup = (ListView) dialog.findViewById(R.id.lvSelectGroup);
		lvSelectGroup.setAdapter(groupListAdapter);
		lvSelectGroup.setOnItemClickListener(this);

		dialog.show();
	}

	// public void receiveMessage(Context context, Intent intent) {
	// Bundle bundle = intent.getExtras();
	//
	// String recMsg = "", fromAdd = "";
	// SmsMessage message = null;
	// byte[] data = null;
	//
	// if (bundle != null) {
	// Object[] pdus = (Object[]) bundle.get("pdus");
	// for (int i = 0; i < pdus.length; i++) {
	// message = SmsMessage.createFromPdu((byte[]) pdus[i]);
	//
	// try {
	// data = message.getUserData();
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// }
	//
	// if (data != null) {
	// for (int j = 0; j < data.length; ++j) {
	// recMsg += Character.toString((char) data[j]);
	// }
	// fromAdd = message.getOriginatingAddress();
	//
	// Relay relay = new Relay();
	// activity.dbHelper.getRelay(fromAdd, relay);
	// String prefix = recMsg.split(" ")[0];
	// if (prefix.equals(relay.getPrefix())) {
	//
	// Group group = relay.getGroup();
	// for (int j = 0; j < group.getMemberList().size(); j++) {
	// activity.sendSMS(group.getMemberList().get(j)
	// .getPhone(), recMsg, null, null);
	// }
	// }
	// }
	// }
	// }
	//
	// }
}
