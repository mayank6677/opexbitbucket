package com.example.opex_new.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.AnalyticsAdapter;
import com.example.opex_new.adapter.AnalyticsFeedbackAdapter;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.Feedback;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class AnalyticsFragment extends Fragment implements OnCheckedChangeListener, OnItemClickListener {

	private RadioGroup rgAnalytics;
	private RadioButton rbBroadcast, rbFeedback;
	private CheckBox cbLast1Hr, cbLast2Hr, cbLast12Hr, cbLast24Hr, cbLastWeek, cbLastMonth;
	private ListView lvAnalytics;
	private GroupActivity activity;
	private String time;
	private boolean isBroadcast;
	private ArrayList<Broadcast> broadcastList;
	private ArrayList<Feedback> feedbackList;
	// private ArrayList<FeedbackPart> feedbackPartList;
	// private ArrayList<Question> questionList;
	// private ArrayList<BroadcastPart> broadcastPartList;
	// private ArrayList<FeedbackPart> feedbackPartList;
	private AnalyticsAdapter analyticsAdapter;
	private AnalyticsFeedbackAdapter analyticsFeedbackAdapter;
	private LinearLayout llAnalytics;
	private TextView tvQId, tvQuestion, tvDateTime;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_analytics, container, false);
		rgAnalytics = (RadioGroup) view.findViewById(R.id.rgAnalytics);
		rbBroadcast = (RadioButton) view.findViewById(R.id.rbBroadcast);
		rbFeedback = (RadioButton) view.findViewById(R.id.rbFeedback);
		cbLast1Hr = (CheckBox) view.findViewById(R.id.cbLast1Hr);
		cbLast2Hr = (CheckBox) view.findViewById(R.id.cbLast2Hr);
		cbLast12Hr = (CheckBox) view.findViewById(R.id.cbLast12Hr);
		cbLast24Hr = (CheckBox) view.findViewById(R.id.cbLast24Hr);
		cbLastWeek = (CheckBox) view.findViewById(R.id.cbLastWeek);
		cbLastMonth = (CheckBox) view.findViewById(R.id.cbLastMonth);
		lvAnalytics = (ListView) view.findViewById(R.id.lvAnalytics);
		llAnalytics = (LinearLayout) view.findViewById(R.id.llAnalytics);
		tvQId = (TextView) view.findViewById(R.id.tvQId);
		tvQuestion = (TextView) view.findViewById(R.id.tvQuestion);
		tvDateTime = (TextView) view.findViewById(R.id.tvDateTime);

		rbBroadcast.setChecked(true);
		cbLast1Hr.setChecked(true);
		time = getTime(1);
		isBroadcast = true;
		loadData();
		// new LoadData().execute();

		rbBroadcast.setOnCheckedChangeListener(this);
		rbFeedback.setOnCheckedChangeListener(this);
		cbLast1Hr.setOnCheckedChangeListener(this);
		cbLast2Hr.setOnCheckedChangeListener(this);
		cbLast12Hr.setOnCheckedChangeListener(this);
		cbLast24Hr.setOnCheckedChangeListener(this);
		cbLastWeek.setOnCheckedChangeListener(this);
		cbLastMonth.setOnCheckedChangeListener(this);
		lvAnalytics.setOnItemClickListener(this);

		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity = (GroupActivity) getActivity();

		broadcastList = new ArrayList<Broadcast>();
		feedbackList = new ArrayList<Feedback>();
		// feedbackPartList = new ArrayList<FeedbackPart>();
		// questionList = new ArrayList<Question>();
		// broadcastPartList = new ArrayList<BroadcastPart>();
		// feedbackPartList = new ArrayList<FeedbackPart>();
		analyticsAdapter = new AnalyticsAdapter(activity, broadcastList);
		// analyticsFeedbackAdapter = new AnalyticsFeedbackAdapter(activity,
		// questionList);
		analyticsFeedbackAdapter = new AnalyticsFeedbackAdapter(activity, feedbackList);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity.actionBar.hide();
		//activity.setActionBarTitle(activity.getString(R.string.title_reports));
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// activity.setFontSize(rbBroadcast);
		activity.setFonts(rbBroadcast);

		// activity.setFontSize(rbFeedback);
		activity.setFonts(rbFeedback);

		// activity.setFontSize(cbLast1Hr);
		activity.setFonts(cbLast1Hr);

		// activity.setFontSize(cbLast2Hr);
		activity.setFonts(cbLast2Hr);

		// activity.setFontSize(cbLast12Hr);
		activity.setFonts(cbLast12Hr);

		// activity.setFontSize(cbLast24Hr);
		activity.setFonts(cbLast24Hr);

		// activity.setFontSize(cbLastWeek);
		activity.setFonts(cbLastWeek);

		// activity.setFontSize(cbLastMonth);
		activity.setFonts(cbLastMonth);

	activity.setBackground(llAnalytics);

		//activity.setFontColor(tvQId);
		activity.setFonts(tvQId);

		//activity.setFontColor(tvQuestion);
		activity.setFonts(tvQuestion);

	//	activity.setFontColor(tvDateTime);
		activity.setFonts(tvDateTime);

	}

	public String getTime(int lastHour) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.HOUR, -lastHour);
		Date date = calendar.getTime();
		return activity.formatDateTime(date);
	}

	public String getLastWeekTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, -7);
		Date date = calendar.getTime();
		return activity.formatDateTime(date);
	}

	public String getLastMonthTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Date date = calendar.getTime();
		return activity.formatDateTime(date);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if (isChecked) {
			switch (buttonView.getId()) {
			case R.id.rbBroadcast:
				isBroadcast = true;
				break;
			case R.id.rbFeedback:
				isBroadcast = false;
				break;
			case R.id.cbLast1Hr:
				time = getTime(1);
				cbLast2Hr.setChecked(false);
				cbLast12Hr.setChecked(false);
				cbLast24Hr.setChecked(false);
				cbLastWeek.setChecked(false);
				cbLastMonth.setChecked(false);
				break;
			case R.id.cbLast2Hr:
				time = getTime(2);
				cbLast1Hr.setChecked(false);
				cbLast12Hr.setChecked(false);
				cbLast24Hr.setChecked(false);
				cbLastWeek.setChecked(false);
				cbLastMonth.setChecked(false);
				break;
			case R.id.cbLast12Hr:
				// time = getTime(12);
				time = null;
				cbLast1Hr.setChecked(false);
				cbLast2Hr.setChecked(false);
				cbLast24Hr.setChecked(false);
				cbLastWeek.setChecked(false);
				cbLastMonth.setChecked(false);
				break;
			case R.id.cbLast24Hr:
				time = getTime(24);
				cbLast1Hr.setChecked(false);
				cbLast2Hr.setChecked(false);
				cbLast12Hr.setChecked(false);
				cbLastWeek.setChecked(false);
				cbLastMonth.setChecked(false);
				break;
			case R.id.cbLastWeek:
				time = getLastWeekTime();
				cbLast1Hr.setChecked(false);
				cbLast2Hr.setChecked(false);
				cbLast12Hr.setChecked(false);
				cbLast24Hr.setChecked(false);
				cbLastMonth.setChecked(false);
				break;
			case R.id.cbLastMonth:
				time = getLastMonthTime();
				cbLast1Hr.setChecked(false);
				cbLast2Hr.setChecked(false);
				cbLast12Hr.setChecked(false);
				cbLast24Hr.setChecked(false);
				cbLastWeek.setChecked(false);
				break;

			default:
				break;
			}
			loadData();
			// new LoadData().execute();
		}
	}

	private class LoadData extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			broadcastList.clear();
			feedbackList.clear();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub

			if (isBroadcast) {
				activity.dbHelper.getBroadcastList(time, broadcastList);
			} else {
				activity.dbHelper.getFeedbackList(time, feedbackList);
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if (isBroadcast) {
				lvAnalytics.setAdapter(analyticsAdapter);
			} else {
				lvAnalytics.setAdapter(analyticsFeedbackAdapter);
			}
		}

	}

	private void loadData() {

		broadcastList.clear();
		feedbackList.clear();
		// questionList.clear();
		AppLog.Log(Const.TAG, "loadData" + isBroadcast);
		if (isBroadcast) {
			// activity.dbHelper.getBroadcastPartList(getTime(1),
			// broadcastPartList);
			// new Thread(new Runnable() {
			//
			// @Override
			// public void run() {
			// TODO Auto-generated method stub
			activity.dbHelper.getBroadcastList(time, broadcastList);
			lvAnalytics.setAdapter(analyticsAdapter);
			AppLog.Log(Const.TAG, "isBroadcast isBroadcast");
			// }
			// }).start();

		} else {
			// try {
			// new Thread(new Runnable() {
			//
			// @Override
			// public void run() {
			// TODO Auto-generated method stub
			activity.dbHelper.getFeedbackList(time, feedbackList);
			// activity.dbHelper.getFeedbackPartList(time,
			// feedbackPartList);
			// feedback id added
			// activity.dbHelper.getQuestionList(feedbackList,
			// questionList);

			AppLog.Log(Const.TAG, "isBroadcast false");

			// }
			// }).join();
			// } catch (InterruptedException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			lvAnalytics.setAdapter(analyticsFeedbackAdapter);

		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		ReportViewFragment reportViewFragment = new ReportViewFragment();
		Bundle bundle = new Bundle();
		bundle.putBoolean(Const.ISBROADCAST, isBroadcast);
		if (isBroadcast) {
			bundle.putSerializable(Const.BROADCASTPART, broadcastList.get(position));
		} else {
			// bundle.putSerializable(Const.QUESTION,
			// questionList.get(position));

			bundle.putSerializable(Const.FEEDBACK, feedbackList.get(position));
		}
		reportViewFragment.setArguments(bundle);
		activity.addFragment(reportViewFragment, true, Const.FRAGMENT_REPORT_VIEW);
	}
}
