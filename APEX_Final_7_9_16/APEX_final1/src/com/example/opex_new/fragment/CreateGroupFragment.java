package com.example.opex_new.fragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import com.example.opex_new.ContactListActivity;
import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.ContactListAdapter;
import com.example.opex_new.adapter.MemberListAdapter;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;
import com.example.opex_new.parse.AsyncTaskCompleteListener;
import com.soundcloud.android.crop.Crop;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class CreateGroupFragment extends Fragment
		implements AsyncTaskCompleteListener, OnClickListener, OnItemClickListener {

	public GroupActivity activity;
	private EditText etGroupName;
	private ImageView /* ivGroupImage, */ btnSelectImage;
	private TextView tvAddMember;
	private LinearLayout llNewGroup;
	public Dialog selectPictureDialog/* , addMemberDialog */;
	private TextView tvClose, btnNewGroupSave, btnNewGroupCancel, tvNoMembers, tvNoImage, btnSubmitImage;
	private ImageView ivGroupPicture, ivAddMEmber;
	private Button btnGallary, btnCamera, btnRemovePhoto;
	private ListView lvMembers;
	private Uri uriTemp = null, uri = null;
	private String filePath = null;
	private final int PICK_CONTACT = 1;
	// private ListView lvContact;
	private ArrayList<Member> /* contactList, */ selectedMemberList;
	private ArrayList<String>/* phoneNumberList, */ nameList;
	// private Button btnOkContact;
	private MemberListAdapter memberAdapter;
	private ContactListAdapter listAdapter;
	private LinearLayout llCreateGroup;
	  public static final int  MAX_PICK_CONTACT= 25;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// contactList = new ArrayList<Member>();
		activity = (GroupActivity) getActivity();
		
		AppLog.Log(Const.TAG, "activity" + activity);
		// activity.setActionBarTitle(activity
		// .getString(R.string.title_create_group));
		selectedMemberList = new ArrayList<Member>();
		nameList = new ArrayList<String>();
		// selectedPhoneNumberList = new ArrayList<String>();
		// listAdapter = new ContactListAdapter(activity, contactList);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.fragment_create_group, container, false);

		etGroupName = (EditText) view.findViewById(R.id.etGroupName);
		// ivGroupImage = (ImageView) view.findViewById(R.id.ivGroupImage);
		btnSelectImage = (ImageView) view.findViewById(R.id.btnSelectImage);
		tvAddMember = (TextView) view.findViewById(R.id.tvAddMember);
		btnNewGroupSave = (TextView) view.findViewById(R.id.btnNewGroupSave);
		btnNewGroupCancel = (TextView) view.findViewById(R.id.btnNewGroupCancel);
		ivAddMEmber = (ImageView) view.findViewById(R.id.ivAddMEmber);
		lvMembers = (ListView) view.findViewById(R.id.lvMembers);
		tvNoMembers = (TextView) view.findViewById(R.id.tvNoMembers);
		llNewGroup = (LinearLayout) view.findViewById(R.id.llNewGroup);
		llCreateGroup = (LinearLayout) view.findViewById(R.id.llCreateGroup);

		ivAddMEmber.setOnClickListener(this);
		btnNewGroupCancel.setOnClickListener(this);
		btnNewGroupSave.setOnClickListener(this);
		btnSelectImage.setOnClickListener(this);
		tvAddMember.setOnClickListener(this);

		memberAdapter = new MemberListAdapter(activity, selectedMemberList, false);

		lvMembers.setAdapter(memberAdapter);
		lvMembers.setItemsCanFocus(true);

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	
		// activity.setFontSize(btnNewGroupSave);
		activity.setFonts(btnNewGroupSave);
		activity.setFontColor(btnNewGroupSave);
			
		activity.setFonts(btnNewGroupCancel);
		activity.setFontColor(btnNewGroupCancel);
		//activity.SetBackgroundColor(llNewGroup);
		// activity.setFontSize(etGroupName);
		activity.setFonts(etGroupName);
		activity.setFontColor(etGroupName);

		//activity.setFontColor(tvAddMember);
		activity.setFonts(tvAddMember);

		//activity.setFontColor(tvNoMembers);
		activity.setFonts(tvNoMembers);

		activity.setBackground(llCreateGroup);
	}

	private boolean isValidate(int id) {
		String msg = null;

		switch (id) {
		case R.id.btnNewGroupSave:
			if (TextUtils.isEmpty(etGroupName.getText().toString())) {
				msg = getString(R.string.enter_group_name);
				etGroupName.requestFocus();
				// } else if (uri == null) {
				// msg = getString(R.string.enter_group_picture);
			} else if (selectedMemberList.size() < 2) {
				msg = getString(R.string.enter_member);
			} else if (filePath == null) {
				msg = getString(R.string.enter_group_picture);
			}

			break;

		case R.id.tvAddMember:
			if (selectedMemberList.size() >=25) {
				msg = activity.getString(R.string.toast_maximum_25);
			}
		default:
			break;
		}
		if (msg != null) {
			Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
			return false;

		}
		return true;
	}

	private void AddGroup() {

		// for (int i = 0; i < selectedMemberList.size(); i++) {
		// AppLog.Log(Const.TAG,
		// "i" + selectedMemberList.get(i).getPhoto(activity));
		// }

		Group group = new Group();
		group.setName(etGroupName.getText().toString());
		group.setPicture(filePath);
		group.setMemberList(selectedMemberList);
		long count = activity.dbHelper.createGroup(group);

		if (count != 0) {
			activity.addFragment(new ViewGroupFragment(), false, Const.FRAGMENT_VIEW_GROUP);
		} else {
			AndyUtils.showToast(activity.getString(R.string.enter_correct_group_name), activity);
			etGroupName.requestFocus();
		}

		// AndyUtils.showSimpleProgressDialog(activity);
		// HashMap<String, String> map = new     Map<String, String>();
		// map.put(Const.URL, Const.ServiceType.ADD_MEMBER);
		// map.put(Const.Params.ID, activity.preferenceHelper.getUserId());
		// map.put(Const.Params.TOKEN,
		// activity.preferenceHelper.getSessionToken());
		// if (!TextUtils.isEmpty("" + filePath)) {
		// map.put(Const.Params.PICTURE, "" + filePath);
		// }
		// map.put(Const.Params.PHONE, "" + TextUtils.join(",",
		// phoneNumberList));
		// map.put(Const.Params.GROUP_NAME, etGroupName.getText().toString());
		// new MultiPartRequester(activity, map, Const.ServiceCode.ADD_MEMBER,
		// this);
	}


	@Override
	public void onTaskCompleted(String response, int serviceCode) {
		// TODO Auto-generated method stub
		switch (serviceCode) {
		case Const.ServiceCode.ADD_MEMBER:
			AndyUtils.removeSimpleProgressDialog();
			// if (activity.parseContent.isSuccess(response)) {
			// activity.removeThisFragment(Const.FRAGMENT_CREATE_GROUP);
			// }
			activity.addFragment(new ViewGroupFragment(), false, Const.FRAGMENT_VIEW_GROUP);
			break;

		default:
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnNewGroupSave:
			if (isValidate(R.id.btnNewGroupSave)) {
				AddGroup();
			}
			break;
		case R.id.btnNewGroupCancel:
			if (isVisible()) {
				activity.removeThisFragment();
			}
			break;
		case R.id.btnSelectImage:
			ShowSelectPictureDialog();
			break;
		case R.id.btnGallary:
			choosePhotoFromGallary();
			break;
		case R.id.btnCamera:
			takePhotoFromCamera();
			break;
		case R.id.btnRemovePhoto:
			uri = null;
			filePath = null;
			// ivGroupPicture.setImageDrawable(activity.getResources()
			// .getDrawable(R.drawable.no_items));
			ivGroupPicture.setVisibility(View.GONE);
			tvNoImage.setVisibility(View.VISIBLE);
			break;
		case R.id.btnSubmitImage:
			// uri = uriTemp;
			if (uri != null) {
				btnSelectImage.setImageURI(uri);
			}
			selectPictureDialog.dismiss();
			break;

		case R.id.tvClose:
			selectPictureDialog.dismiss();
			break;

		case R.id.tvAddMember:
		case R.id.ivAddMEmber:
			if (isValidate(R.id.tvAddMember)) {
				activity.showAddMemberDialog(this, selectedMemberList);
			
			}
			else{
				
				
			
				
			}
			break;

		// case R.id.btnOkContact:
		//
		// break;
		default:
			break;
		}
	}

	private void ShowSelectPictureDialog() {
		selectPictureDialog = new Dialog(activity);
		selectPictureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		selectPictureDialog.setContentView(R.layout.dialog_select_picture);
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(selectPictureDialog.getWindow().getAttributes());
		lp.width = LayoutParams.MATCH_PARENT;
		lp.height = LayoutParams.MATCH_PARENT;
		lp.gravity = Gravity.CENTER;

		selectPictureDialog.getWindow().setAttributes(lp);

		tvClose = (TextView) selectPictureDialog.findViewById(R.id.tvClose);
		ivGroupPicture = (ImageView) selectPictureDialog.findViewById(R.id.ivGroupPicture);
		tvNoImage = (TextView) selectPictureDialog.findViewById(R.id.tvNoImage);

		btnGallary = (Button) selectPictureDialog.findViewById(R.id.btnGallary);
		btnCamera = (Button) selectPictureDialog.findViewById(R.id.btnCamera);
		btnRemovePhoto = (Button) selectPictureDialog.findViewById(R.id.btnRemovePhoto);
		btnSubmitImage = (TextView) selectPictureDialog.findViewById(R.id.btnSubmitImage);

		btnGallary.setOnClickListener(this);
		btnCamera.setOnClickListener(this);
		btnRemovePhoto.setOnClickListener(this);
		btnSubmitImage.setOnClickListener(this);
		tvClose.setOnClickListener(this);

		activity.setFonts(btnSubmitImage); 
		// activity.setFontSize(btnSubmitImage);
		activity.SetBackgroundColor(btnSubmitImage);

		activity.setFonts(tvClose);
		// activity.setFontSize(tvClose);
		activity.SetBackgroundColor(tvClose);

		activity.setFonts(btnCamera);
		// activity.setFontSize(btnCamera);
		activity.setFontColor(btnCamera);

		activity.setFonts(btnGallary);
		// activity.setFontSize(btnGallary);
		activity.setFontColor(btnGallary);

		activity.setFonts(btnRemovePhoto);
		// activity.setFontSize(btnRemovePhoto);
		activity.setFontColor(btnRemovePhoto);

		activity.setFonts(tvNoImage);
		activity.setFontColor(tvNoImage);

		selectPictureDialog.show();
	}

	private void choosePhotoFromGallary() {
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		activity.startActivityForResult(i, Const.CHOOSE_PHOTO, Const.FRAGMENT_CREATE_GROUP);

	}

	private void takePhotoFromCamera() {
		Calendar cal = Calendar.getInstance();
		File file = new File(Environment.getExternalStorageDirectory(), (cal.getTimeInMillis() + ".jpg"));
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		uriTemp = Uri.fromFile(file);
		Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		i.putExtra(MediaStore.EXTRA_OUTPUT, uriTemp);
		activity.startActivityForResult(i, Const.TAKE_PHOTO, Const.FRAGMENT_CREATE_GROUP);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {

		case Const.CHOOSE_PHOTO:
			if (data != null) {

				uriTemp = data.getData();
				if (uriTemp != null) {
					activity.setFbTag(Const.FRAGMENT_CREATE_GROUP);
					// ivGroupPicture.setImageURI(uriTemp);
					beginCrop(uriTemp);
				} else {
					Toast.makeText(activity, "unable to select image", Toast.LENGTH_LONG).show();
				}
			}
			break;
		case Const.TAKE_PHOTO:
			if (uriTemp != null) {
				activity.setFbTag(Const.FRAGMENT_CREATE_GROUP);
				// ivGroupPicture.setImageURI(uriTemp);
				beginCrop(uriTemp);

			} else {
				Toast.makeText(activity, "unable to select image", Toast.LENGTH_LONG).show();
			}
			break;
		case Crop.REQUEST_CROP:
			// AppLog.Log(Const.TAG, "Crop photo on activity result");
			if (data != null)
				handleCrop(resultCode, data);

			break;
		}

	}

	private void beginCrop(Uri source) {
		// Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
		// "cropped"));
		Uri outputUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
				(Calendar.getInstance().getTimeInMillis() + ".jpg")));
		new Crop(source).output(outputUri).asSquare().start(activity);
	}

	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == Activity.RESULT_OK) {
			// AppLog.Log(Const.TAG, "Handle crop");
			filePath = activity.getRealPathFromURI(Crop.getOutput(result));
			uri = Crop.getOutput(result);
			ivGroupPicture.setVisibility(View.VISIBLE);
			tvNoImage.setVisibility(View.GONE);
			ivGroupPicture.setImageURI(Crop.getOutput(result));

		} else if (resultCode == Crop.RESULT_ERROR) {
			Toast.makeText(activity, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
		}
	}

	// private void showAddMemberDialog() {
	// addMemberDialog = new Dialog(activity);
	// addMemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	// addMemberDialog.setContentView(R.layout.activity_contact_list);
	// WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	// lp.copyFrom(addMemberDialog.getWindow().getAttributes());
	// lp.width = WindowManager.LayoutParams.MATCH_PARENT;
	// lp.height = WindowManager.LayoutParams.MATCH_PARENT;
	// lp.gravity = Gravity.CENTER;
	// addMemberDialog.getWindow().setAttributes(lp);
	//
	// lvContact = (ListView) addMemberDialog.findViewById(R.id.lvContact);
	// btnOkContact = (Button) addMemberDialog.findViewById(R.id.btnOkContact);
	//
	// lvContact.setOnItemClickListener(this);
	// lvContact.setAdapter(listAdapter);
	//
	// Cursor cursor = null;
	// try {
	// cursor = activity.getContentResolver().query(Phone.CONTENT_URI,
	// null, null, null, null);
	// int contactIdIdx = cursor.getColumnIndex(Phone._ID);
	// int nameIdx = cursor.getColumnIndex(Phone.DISPLAY_NAME);
	// int phoneNumberIdx = cursor.getColumnIndex(Phone.NUMBER);
	// int photoIdIdx = cursor.getColumnIndex(Phone.PHOTO_ID);
	// contactList.clear();
	// Log.d("", "cursor" + cursor.getCount());
	// cursor.moveToFirst();
	// do {
	// Member member = new Member();
	// String idContact = cursor.getString(contactIdIdx);
	// String name = cursor.getString(nameIdx);
	// String phoneNumber = cursor.getString(phoneNumberIdx);
	//
	// member.setName(name);
	// member.setPhone(phoneNumber);
	// contactList.add(member);
	// // ...
	// } while (cursor.moveToNext());
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// // listAdapter.notifyDataSetChanged();
	// if (cursor != null) {
	// cursor.close();
	// }
	// }
	// listAdapter.notifyDataSetChanged();
	// addMemberDialog.show();
	//
	// }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		lvMembers.setItemsCanFocus(true);

		
		ImageView box = (ImageView) view.findViewById(R.id.check);
		// box.setChecked(true);
	if(box.getVisibility()==View.VISIBLE){
		box.setVisibility(View.GONE);
	}else
	{box.setVisibility(View.VISIBLE);
		
	}
		

		// TODO Auto-generated method stub
		// memberList.get(position).isChecked = true;
		// phoneNumberList.add(memberList.get(position).getContact());

		AppLog.Log(Const.TAG, "onItemClick");

		if (((Member) parent.getItemAtPosition(position)).getIsSelected() == true) {

			((Member) parent.getItemAtPosition(position)).setIsSelected(false);
			selectedMemberList.remove(parent.getItemAtPosition(position));
			nameList.remove(((Member) parent.getItemAtPosition(position)).getName());
			
		} else {

			((Member) parent.getItemAtPosition(position)).setIsSelected(true);
			selectedMemberList.add((Member) parent.getItemAtPosition(position));
			nameList.add(((Member) parent.getItemAtPosition(position)).getName());
		}

//		if(selectedMemberList.contains())
//		{
//			box.setVisibility(View.VISIBLE);
//			
//			
//		}
//		else
//		{
//			box.setVisibility(View.GONE);
//			
//		}
		System.out.print("checkbox" + activity.contactList.size());
		System.out.print("checkbox1" + parent.getItemAtPosition(position));
		// AppLog.Log(
		// Const.TAG,
		// "pic onitem:"
		// + ((Member) parent.getItemAtPosition(position))
		// .getPhoto(activity));
		// listAdapter.notifyDataSetChanged();
		memberAdapter.notifyDataSetChanged();
		/*
		 * if (activity.addMemberDialog != null) {
		 * activity.addMemberDialog.dismiss(); }
		 * 
		 */
		tvNoMembers.setVisibility(View.GONE);
		lvMembers.setVisibility(View.VISIBLE);
	}
}
