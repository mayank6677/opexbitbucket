package com.example.opex_new.fragment;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import com.androidquery.AQuery;
import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.User;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileFragment extends Fragment implements OnClickListener {

	private GroupActivity activity;
	private TextView btnProfileCancel, btnProfileSave, btnPasswordCancel, btnPasswordSave;
	private ImageView ivProfilePhoto, ivProfileChangePassWord;
	private LinearLayout llProfile;
	private Dialog passwordDialog;
	private EditText etOldPassword, etNewPassword, etReEnterPassword, etName;
	private DBHelper dbHelper;
	private String newPassWord;
	private Uri uriTemp = null, uri = null;
	private String filePath = null;
	private User user;
	private AQuery aQuery;
	private RelativeLayout rlProfile;

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.setActionBarTitle(getString(R.string.title_profile));
		dbHelper = new DBHelper(activity);
		user = new User();
		aQuery = new AQuery(activity);
		user.setUserId(new PreferenceHelper(activity).getUserId());
		user = dbHelper.getProfile(user);

		AppLog.Log(Const.TAG, "photo" + user.getPhoto());
		if (user.getPhoto() != null) {
			aQuery.id(ivProfilePhoto).image(user.getPhoto());
			filePath = user.getPhoto();
		}
		if (user.getName() != null) {
			etName.setText(user.getName());
		}
		// ivProfilePhoto.setImageURI(user.getPhoto());

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_profile, container, false);
		ivProfilePhoto = (ImageView) view.findViewById(R.id.ivProfilePhoto);
		ivProfileChangePassWord = (ImageView) view.findViewById(R.id.ivProfileChangePassWord);
		btnProfileCancel = (TextView) view.findViewById(R.id.btnProfileCancel);
		btnProfileSave = (TextView) view.findViewById(R.id.btnProfileSave);
		etName = (EditText) view.findViewById(R.id.etName);
		llProfile = (LinearLayout) view.findViewById(R.id.llProfile);
		rlProfile = (RelativeLayout) view.findViewById(R.id.rlProfile);

		ivProfileChangePassWord.setOnClickListener(this);
		ivProfilePhoto.setOnClickListener(this);
		btnProfileSave.setOnClickListener(this);
		btnProfileCancel.setOnClickListener(this);
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

	//	activity.SetBackgroundColor(btnProfileCancel);
		// activity.setFontSize(btnProfileCancel);
		activity.setFonts(btnProfileCancel);

	//	activity.SetBackgroundColor(btnProfileSave);
		// activity.setFontSize(btnProfileSave);
		activity.setFonts(btnProfileSave);
		activity.setFontColor(btnProfileSave);
		activity.setFontColor(btnProfileCancel);


		//activity.SetBackgroundColor(llProfile);
		// activity.setFontSize(etName);
		activity.setFonts(etName);
		//activity.setFontColor(etName);

	activity.setBackground(rlProfile);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {
		case R.id.btnProfileSave:
			AppLog.Log(Const.TAG, "name" + etName.getText().toString());
			user.setName(etName.getText().toString());
			user.setPhoto(filePath);
			long count = dbHelper.updateProfile(user);
			AppLog.Log(Const.TAG, "name" + user.getFullName());
			AppLog.Log(Const.TAG, "update profile" + count);
			if (newPassWord != null) {
				dbHelper.addPassword(user.getUserId(), newPassWord);
			}
			activity.onBackPressed();
			activity.onBackPressed();
			break;
		case R.id.btnProfileCancel:
			activity.removeThisFragment();
			break;
		case R.id.ivProfileChangePassWord:
			showPasswordDialog();
			break;
		case R.id.ivProfilePhoto:
			showPictureDialog();
			break;

		case R.id.btnPasswordSave:
			if (isValidate()) {
				newPassWord = etNewPassword.getText().toString();
				passwordDialog.dismiss();
			}
			break;

		case R.id.btnPasswordCancel:
			passwordDialog.dismiss();
			break;

		default:
			break;
		}
	}

	private void showPasswordDialog() {
		if (passwordDialog == null) {
			passwordDialog = new Dialog(activity);
			passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			passwordDialog.setContentView(R.layout.dialog_change_password);

			passwordDialog.setCancelable(false);
			etOldPassword = (EditText) passwordDialog.findViewById(R.id.etOldPassword);
			etNewPassword = (EditText) passwordDialog.findViewById(R.id.etNewPassword);
			etReEnterPassword = (EditText) passwordDialog.findViewById(R.id.etReEnterPassword);
			btnPasswordCancel = (TextView) passwordDialog.findViewById(R.id.btnPasswordCancel);
			btnPasswordSave = (TextView) passwordDialog.findViewById(R.id.btnPasswordSave);

			btnPasswordSave.setOnClickListener(this);
			btnPasswordCancel.setOnClickListener(this);

			passwordDialog.setOnDismissListener(new OnDismissListener() {

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					passwordDialog = null;
				}
			});

			activity.SetBackgroundColor(btnPasswordCancel);
			// activity.setFontSize(btnPasswordCancel);
			activity.setFonts(btnPasswordCancel);

			activity.SetBackgroundColor(btnPasswordSave);
			// activity.setFontSize(btnPasswordSave);
			activity.setFonts(btnPasswordSave);

			passwordDialog.show();
		}
	}

	private boolean isValidate() {
		String msg = null;

		if (TextUtils.isEmpty(etOldPassword.getText().toString())) {
			msg = activity.getString(R.string.enter_pass);
		} else if (TextUtils.isEmpty(etNewPassword.getText().toString())) {
			msg = activity.getString(R.string.enter_pass);
		} else if (TextUtils.isEmpty(etReEnterPassword.getText().toString())) {
			msg = activity.getString(R.string.enter_pass);
		} else if (!dbHelper.getPassword(user.getUserId()).equals(etOldPassword.getText().toString())) {
			msg = activity.getString(R.string.toast_invalid_old_pass);
		} else if (!etNewPassword.getText().toString().equals(etReEnterPassword.getText().toString())) {
			msg = activity.getString(R.string.toast_invalid_new_pass);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}
		return true;
	}

	private void showPictureDialog() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(activity);
		dialog.setTitle(getString(R.string.text_choosepicture));
		String[] items = { getString(R.string.text_gallary), getString(R.string.text_camera) };

		dialog.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				switch (which) {
				case 0:
					choosePhotoFromGallary();
					break;
				case 1:
					takePhotoFromCamera();
					break;

				}
			}
		});
		dialog.show();
	}

	private void choosePhotoFromGallary() {
		Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

		activity.startActivityForResult(i, Const.CHOOSE_PHOTO, Const.FRAGMENT_PROFILE);

	}

	private void takePhotoFromCamera() {
		Calendar cal = Calendar.getInstance();
		File file = new File(Environment.getExternalStorageDirectory(), (cal.getTimeInMillis() + ".jpg"));
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {

			file.delete();
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		uriTemp = Uri.fromFile(file);
		Intent i = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		i.putExtra(MediaStore.EXTRA_OUTPUT, uriTemp);
		activity.startActivityForResult(i, Const.TAKE_PHOTO, Const.FRAGMENT_PROFILE);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {

		case Const.CHOOSE_PHOTO:
			if (data != null) {

				uriTemp = data.getData();
				if (uriTemp != null) {
					activity.setFbTag(Const.FRAGMENT_CREATE_GROUP);
					ivProfilePhoto.setImageURI(uriTemp);
					filePath = getRealPathFromURI(uriTemp);
					// beginCrop(uriTemp);
				} else {
					Toast.makeText(activity, "unable to select image", Toast.LENGTH_LONG).show();
				}
			}
			break;
		case Const.TAKE_PHOTO:
			if (uriTemp != null) {
				activity.setFbTag(Const.FRAGMENT_CREATE_GROUP);
				ivProfilePhoto.setImageURI(uriTemp);
				filePath = getRealPathFromURI(uriTemp);
				// beginCrop(uriTemp);

			} else {
				Toast.makeText(activity, "unable to select image", Toast.LENGTH_LONG).show();
			}
			break;
		// case Crop.REQUEST_CROP:
		// AppLog.Log(Const.TAG, "Crop photo on activity result");
		// if (data != null)
		// handleCrop(resultCode, data);
		//
		// break;
		}

	}

	// private void beginCrop(Uri source) {
	// // Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
	// // "cropped"));
	// AppLog.Log(Const.TAG, "begin crop");
	// Uri outputUri = Uri.fromFile(new File(Environment
	// .getExternalStorageDirectory(), (Calendar.getInstance()
	// .getTimeInMillis() + ".jpg")));
	// new Crop(source).output(outputUri).asSquare().start(activity);
	// }

	// private void handleCrop(int resultCode, Intent result) {
	// if (resultCode == activity.RESULT_OK) {
	// AppLog.Log(Const.TAG, "Handle crop");
	// filePath = getRealPathFromURI(Crop.getOutput(result));
	// uri = Crop.getOutput(result);
	// ivProfilePhoto.setImageURI(Crop.getOutput(result));
	//
	// } else if (resultCode == Crop.RESULT_ERROR) {
	// Toast.makeText(activity, Crop.getError(result).getMessage(),
	// Toast.LENGTH_SHORT).show();
	// }
	// }

	private String getRealPathFromURI(Uri contentURI) {
		String result;
		Cursor cursor = activity.getContentResolver().query(contentURI, null, null, null, null);

		if (cursor == null) { // Source is Dropbox or other similar local file
			// path
			result = contentURI.getPath();
		} else {
			cursor.moveToFirst();
			int idx = cursor.getColumnIndex(MediaColumns.DATA);
			result = cursor.getString(idx);
			cursor.close();
		}
		return result;
	}

}
