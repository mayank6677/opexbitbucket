package com.example.opex_new.fragment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeSet;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.TempleteListAdapter;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Templete;

import android.content.ClipData.Item;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TempleteFragment extends Fragment implements OnClickListener, OnItemClickListener {

	private GroupActivity activity;
	private ListView lvTemp;
	private Button btnAddTemp;
	private ImageView ivNoTemp;
	private ArrayList<Templete> templeteList, templeteListOrg;
	private TempleteListAdapter templeteAdapter;
	// private ArrayList<Member> memberListForBroadcast;
	private ArrayList<String> categoryNameList;
	private TreeSet<Integer> mSeparatorsSet;
	private Group selectedGroup;
	private LinearLayout llTemplate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// memberListForBroadcast = getArguments().getParcelableArrayList(
		// Const.Params.MEMBER);
		selectedGroup = (Group) getArguments().getSerializable(Const.Params.GROUP);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_templete, container, false);
		lvTemp = (ListView) view.findViewById(R.id.lvTemp);
		ivNoTemp = (ImageView) view.findViewById(R.id.ivNoTemp);
		btnAddTemp = (Button) view.findViewById(R.id.btnAddTemp);
		llTemplate = (LinearLayout) view.findViewById(R.id.llTemplate);
		btnAddTemp.setOnClickListener(this);
	
		return view;
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	//	activity.SetBackgroundColor(btnAddTemp);
		// activity.setFontSize(btnAddTemp);
		activity.setFonts(btnAddTemp);
activity.setFontColor(btnAddTemp);
		activity.setBackground(llTemplate);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
	activity.actionBar.hide();
		templeteList = new ArrayList<Templete>();
		templeteListOrg = new ArrayList<Templete>();
		categoryNameList = new ArrayList<String>();
		mSeparatorsSet = new TreeSet<Integer>();
		templeteAdapter = new TempleteListAdapter(activity, templeteListOrg, mSeparatorsSet);
		// new ArrayAdapter<String>(activity,
		// android.R.layout.simple_list_item_1,
		// templeteDataList);
		lvTemp.setAdapter(templeteAdapter);
		lvTemp.setOnItemClickListener(this);
		getTemplete();
		
		
	//	lvTemp.performItemClick(null, 2, lvTemp.getItemIdAtPosition(2) ); 
		
		new Handler().postDelayed(new Runnable() {
		    @Override
		    public void run() {
		    	View parent=lvTemp;
		    	try{
	    			
	    	
	    			System.out.println(" Got Parent "+parent);
	    			//findViews(parent);
	    		}catch(Exception e){
	    			
	    		}
		    	for(int index=0; index<((ViewGroup)parent).getChildCount(); ++index) {
		    	    View nextChild = ((ViewGroup)parent).getChildAt(index);
		    		System.out.println(" Got View "+nextChild);
		    		if(mSeparatorsSet.contains(index)){
						continue;
					}

		    		   LayoutParams params = nextChild.getLayoutParams();

		                // Set the height of the Item View
		                params.height = 1;
		                nextChild.setLayoutParams(params);
		                
		                nextChild.setVisibility(View.INVISIBLE);
		    	}
		    	

				
		    
		    }
		},500);
		

	}
	public static void findViews(View v) {
	    try {
	        if (v instanceof ViewGroup) {
	            ViewGroup vg = (ViewGroup) v;
	            for (int i = 0; i < vg.getChildCount(); i++) {
	                View child = vg.getChildAt(i);
	                // recursively call this method 
	                findViews(child);
	            }
	        } else if (v instanceof TextView) {
	            //do whatever you want ...
	        	System.out.println("Got Text View "+ v.getId());
	        }
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
	private void getTemplete() {
		templeteList.clear();
		templeteListOrg.clear();
		mSeparatorsSet.clear();
		categoryNameList.clear();
		activity.dbHelper.getTempletes(templeteList);

		HashSet<String> listToSet = new HashSet<String>();
		for (int i = 0; i < templeteList.size(); i++) {
			AppLog.Log("template:", templeteList.get(i).getName() + "");
			if (listToSet.add(templeteList.get(i).getCategoryName())) {
				
				categoryNameList.add(templeteList.get(i).getCategoryName());
				
				
			}
		}
		for (int i = 0; i < categoryNameList.size(); i++) {
			AppLog.Log(Const.TAG, "" + i);
			Templete item = new Templete();
			item.setName(categoryNameList.get(i));
			templeteListOrg.add(item);

			mSeparatorsSet.add(templeteListOrg.size() - 1);
			for (int j = 0; j < templeteList.size(); j++) {
				AppLog.Log(Const.TAG, "" + templeteList.get(j).getCategoryName());
				if (templeteList.get(j).getCategoryName().equals(categoryNameList.get(i))) {
					templeteListOrg.add(templeteList.get(j));
				}
			}
		}

		templeteAdapter.notifyDataSetChanged();
		
	}

	@Override
	public void onClick(View v) {
		System.out.println("image event");
		// TODO Auto-generated method stub
		// activity.addFragment(new AddTempleteFragment(), true,
		// Const.FRAGMENT_ADD_TEMPLETE);
		activity.addTemplateFragment(activity.getString(R.string.hint_category_name));
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		
		// TODO Auto-generated method stub
		if (mSeparatorsSet.contains(position))
		{

System.out.println("Selected Cat ");

try{

			for (int i = position; i < templeteListOrg.size(); i++) {
				
				if(position == i)
				{
					
					
					
				}
				else{
					
					if(mSeparatorsSet.contains(i)){
						return;
					}

					if(parent.getChildAt(i).getVisibility() == View.VISIBLE)
					{
						 LayoutParams params = view.getLayoutParams();

			                // Set the height of the Item View
			                params.height = 1;
			                parent.getChildAt(i).setLayoutParams(params);
						parent.getChildAt(i).setVisibility(View.INVISIBLE);
					}
					else
					{
						   LayoutParams params = view.getLayoutParams();

			                // Set the height of the Item View
			                params.height = 0;
			                parent.getChildAt(i).setLayoutParams(params);
			                
						parent.getChildAt(i).setVisibility(View.VISIBLE);
					}
				
					
					
				}
			
				
			}
		
}catch(Exception e){
	
}
				return;
		}
		
		
System.out.println("item click listener");
		ChangeTempleteFragment changeTempleteFragment = new ChangeTempleteFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(Const.TEMPLETE, templeteListOrg.get(position));
		bundle.putBoolean(Const.ISFORMESSAGING, getArguments().getBoolean(Const.ISFORMESSAGING));
		bundle.putInt(Const.MSGSTATUS, getArguments().getInt(Const.MSGSTATUS));
		// bundle.putParcelableArrayList(Const.Params.MEMBER,
		// memberListForBroadcast);
		bundle.putSerializable(Const.Params.GROUP, selectedGroup);
		bundle.putBoolean(Const.ISBROADCAST, getArguments().getBoolean(Const.ISBROADCAST));
		changeTempleteFragment.setArguments(bundle);
		activity.addFragment(changeTempleteFragment, true, Const.FRAGMENT_CHANGE_TEMPLETE);
	}

}
