package com.example.opex_new.fragment;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.Templete;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AddTempleteFragment extends Fragment implements OnClickListener {
	private GroupActivity activity;
	private EditText etTemplete, etTempleteName, etCategoryName;
	private Button btnSaveTemplete;
	private TextView tvSizeTemplate;
	private String category;
	private LinearLayout llAddTemplate;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		category = getArguments().getString(Const.CATEGORY);
		if (!category.equals(activity.getString(R.string.hint_category_name))) {
			etCategoryName.setText(category);
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragmdent_add_templete, container, false);

		etCategoryName = (EditText) view.findViewById(R.id.etCategoryName);
		etTempleteName = (EditText) view.findViewById(R.id.etTempleteName);
		etTemplete = (EditText) view.findViewById(R.id.etTemplete);
		tvSizeTemplate = (TextView) view.findViewById(R.id.tvSizeTemplate);
		btnSaveTemplete = (Button) view.findViewById(R.id.btnSaveTemplete);
		llAddTemplate = (LinearLayout) view.findViewById(R.id.llAddTemplate);

		btnSaveTemplete.setOnClickListener(this);

		tvSizeTemplate.setText(Const.MAX_MSG_SIZE + "/" + 1);

		// etCategoryName.setOnClickListener(this);
		etTemplete.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				tvSizeTemplate.setText(Const.MAX_MSG_SIZE - (s.length() % Const.MAX_MSG_SIZE) + "/"
						+ ((s.length() / Const.MAX_MSG_SIZE) + 1));
			}
		});
		etCategoryName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    public void onFocusChange(View v, boolean hasFocus) {
		        if (hasFocus)
		        	etCategoryName.setHint("");
		        else
		        	etCategoryName.setHint("Category Name");
		    }
		});
		etTempleteName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    public void onFocusChange(View v, boolean hasFocus) {
		        if (hasFocus)
		        	etTempleteName.setHint("");
		        else
		        	etTempleteName.setHint("Template Name");
		    }
		});
		etTemplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
		    public void onFocusChange(View v, boolean hasFocus) {
		        if (hasFocus)
		        	etTemplete.setHint("");
		        else
		        	etTemplete.setHint("Template");
		    }
		});
		return view;
		
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//activity.SetBackgroundColor(etCategoryName);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(etCategoryName);

		//activity.SetBackgroundColor(etTempleteName);
		// activity.setFontSize(etTempleteName);
		activity.setFonts(etTempleteName);
	
		//activity.SetBackgroundColor(btnSaveTemplete);
		// activity.setFontSize(btnSaveTemplete);
		activity.setFonts(btnSaveTemplete);
activity.setFontColor(btnSaveTemplete);
	activity.setBackground(llAddTemplate);

		activity.setFontColor(etTemplete);
		// activity.setFontSize(etCategoryName);
		activity.setFonts(etTemplete);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.etCategoryName:
			showCategoryList();
			break;
		case R.id.btnSaveTemplete:
			if (isValidate()) {
				addTemplete();
				activity.removeThisFragment();
			}
		default:
			break;
		}
	}

	private void addTemplete() {
		Templete templete = new Templete();
		templete.setCategoryName(etCategoryName.getText().toString());
		templete.setName(etTempleteName.getText().toString());
		templete.setData(etTemplete.getText().toString());

		AppLog.Log(Const.TAG, "" + etCategoryName.getText().toString());
		activity.dbHelper.addTemplete(templete);
	}

	private boolean isValidate() {
		String msg = null;

		// if (etCategoryName.getText().toString()
		// .equals(activity.getString(R.string.hint_category_name))) {
		// msg = activity.getString(R.string.toast_category_name);
		// }
		if (TextUtils.isEmpty(etCategoryName.getText().toString())) {
			msg = activity.getString(R.string.toast_category_name);
		} else if (TextUtils.isEmpty(etTempleteName.getText().toString().trim())) {
			msg = activity.getString(R.string.toast_templete_name);
		} else if (etTemplete.getText().toString().trim().length() <= 1) {
			msg = activity.getString(R.string.toast_invalid_templete);
		} else if (etTemplete.getText().toString().trim().length() > 460) {
			msg = activity.getString(R.string.toast_invalid_templete_max);
		}

		if (msg != null) {
			AndyUtils.showToast(msg, activity);
			return false;
		}
		return true;
	}

	private void showCategoryList() {
		final String[] array = activity.getResources().getStringArray(R.array.array_category);
		new AlertDialog.Builder(activity).setItems(array, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				etCategoryName.setText(array[which]);
			}
		}).show();
	}
}
