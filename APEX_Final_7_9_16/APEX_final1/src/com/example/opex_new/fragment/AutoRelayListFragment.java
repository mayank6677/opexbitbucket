package com.example.opex_new.fragment;

import java.util.ArrayList;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.adapter.AutoRelayAdapter;
import com.example.opex_new.db.DBHelper;
import com.example.opex_new.model.Relay;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class AutoRelayListFragment extends Fragment {
	private GroupActivity activity;
	private ListView lvAutoRelay;
	private AutoRelayAdapter autoRelayAdapter;
	private ArrayList<Relay> relayList;
	private LinearLayout llAutoReLayList;
	private TextView tvNoRelay;
	private TextView tvTitleGroup, tvTitlePrefix, tvTitleNumberName;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_auto_relay_list, container, false);
		lvAutoRelay = (ListView) view.findViewById(R.id.lvAutoRelay);
		llAutoReLayList = (LinearLayout) view.findViewById(R.id.llAutoReLayList);
		tvNoRelay = (TextView) view.findViewById(R.id.tvNoRelay);
		tvTitleGroup = (TextView) view.findViewById(R.id.tvTitleGroup);
		tvTitleNumberName = (TextView) view.findViewById(R.id.tvTitleNumberName);
		tvTitlePrefix = (TextView) view.findViewById(R.id.tvTitlePrefix);
		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	activity.setBackground(llAutoReLayList);

		//activity.setFontColor(tvNoRelay);
		activity.setFonts(tvNoRelay);

		//activity.setFontColor(tvTitleGroup);
		activity.setFonts(tvTitleGroup);

		//activity.setFontColor(tvTitleNumberName);
		activity.setFonts(tvTitleNumberName);

		//activity.setFontColor(tvTitlePrefix);
		activity.setFonts(tvTitlePrefix);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.actionBar.hide();
		relayList = new ArrayList<Relay>();
		new DBHelper(activity).getRelayList(relayList);
		autoRelayAdapter = new AutoRelayAdapter(activity, relayList);
		lvAutoRelay.setAdapter(autoRelayAdapter);
		if (relayList.size() > 0) {
			tvNoRelay.setVisibility(View.GONE);
			lvAutoRelay.setVisibility(View.VISIBLE);
		}
	}
}
