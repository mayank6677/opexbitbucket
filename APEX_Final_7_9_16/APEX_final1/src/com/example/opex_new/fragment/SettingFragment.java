package com.example.opex_new.fragment;

import com.example.opex_new.GroupActivity;
import com.example.opex_new.R;
import com.example.opex_new.Utils.Const;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

public class SettingFragment extends Fragment implements OnClickListener {
	private GroupActivity activity;
	private Button btnProfile, btnOtherOption, btnAutoForward,polls;
	private LinearLayout llSetting;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment_setting, container, false);
		btnProfile = (Button) view.findViewById(R.id.btnProfile);
		btnOtherOption = (Button) view.findViewById(R.id.btnOtherOption);
		btnAutoForward = (Button) view.findViewById(R.id.btnAutoForward);
		polls = (Button) view.findViewById(R.id.polls);
		llSetting = (LinearLayout) view.findViewById(R.id.settingsLinear);

		btnProfile.setOnClickListener(this);
		btnOtherOption.setOnClickListener(this);
		btnAutoForward.setOnClickListener(this);
		polls.setOnClickListener(this);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		activity = (GroupActivity) getActivity();
		activity.actionBar.hide();
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		// activity.setFontSize(btnProfile);
		activity.setFonts(btnProfile);
		//activity.setFontColor(btnProfile);

		// activity.setFontSize(btnOtherOption);
		activity.setFonts(btnOtherOption);
		//activity.setFontColor(btnOtherOption);

		// activity.setFontSize(btnAutoForward);
		activity.setFonts(btnAutoForward);
		activity.setFonts(polls);
		//activity.setFontColor(btnAutoForward);

activity.setBackground(llSetting);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnProfile:
			activity.addFragment(new ProfileFragment(), true, Const.FRAGMENT_PROFILE);
			break;

		case R.id.btnOtherOption:
			activity.addFragment(new AppearanceFragment(), true, Const.FRAGMENT_APPEARANCE);
			break;

		case R.id.btnAutoForward:
			activity.addFragment(new AutoRelayListFragment(), true, Const.FRAGMENT_AUTORELAYLIST);
			break;
		case R.id.polls:
			activity.addFragment(new AnalyticsFragment2(), true, Const. FRAGMENT_ANALYTICS2);
			break;
		default:
			break;
		}
	}

}
