package com.example.opex_new;

import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.User;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ForgetPasswordActivity extends ActionBarBaseActivity {
	private EditText etPhone;
	private Button btnForget;
	private TextView spCCode;
	// private ArrayList<String> list;
	private String country;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		actionBar.hide();
		setContentView(R.layout.forget_pass_fragment);
		etPhone = (EditText) findViewById(R.id.etForgetEmail);
		btnForget = (Button) findViewById(R.id.tvForgetSubmit);

		btnForget.setOnClickListener(this);
		spCCode = (TextView) findViewById(R.id.spCCodeFp);
		spCCode.setOnClickListener(this);
		// country = Locale.getDefault().getDisplayCountry();
		//
		// list = parseContent.parseCountryCodes();
		// for (int i = 0; i < list.size(); i++) {
		// if (list.get(i).contains(country)) {
		// spCCode.setText((list.get(i).substring(0,
		// list.get(i).indexOf(" "))));
		// }
		// }
		// if (TextUtils.isEmpty(spCCode.getText())) {
		// spCCode.setText((list.get(0).substring(0,
		// list.get(0).indexOf(" "))));
		// }

	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//setFonts(btnForget);
		// setFontSize(btnForget);
	//	SetBackgroundColor(btnForget);
	}

	@Override
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.tvForgetSubmit:
			if (TextUtils.isEmpty(etPhone.getText())) {
				AndyUtils.showToast(getString(R.string.enter_numberr), this);
			} else if (etPhone.getText().toString().length() != 10) {
				AndyUtils.showToast(getString(R.string.enter_valid_numberr), this);
			} else {
				forgetPassowrd();
			}

			break;
		case R.id.spCCodeFp:
			showCountryCodeDialog(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					spCCode.setText(list.get(position).substring(0, list.get(position).indexOf(" ")));
					countryCodeDialog.dismiss();
				}
			});
			// AlertDialog.Builder countryBuilder = new Builder(this);
			// countryBuilder.setTitle("Country codes");
			//
			// final String[] array = new String[list.size()];
			// list.toArray(array);
			// countryBuilder.setItems(array,
			// new DialogInterface.OnClickListener() {
			//
			// @Override
			// public void onClick(DialogInterface dialog, int which) {
			// spCCode.setText(array[which].substring(0,
			// array[which].indexOf(" ")));
			// }
			// }).show();
			break;
		default:
			break;
		}

	}

	private void forgetPassowrd() {

		User user = new User();
		// user.setName(etUserName.getText().toString());
		user.setContact(spCCode.getText().toString().trim() + etPhone.getText().toString());
		user.setOtp(generateOtp());
		user.setUserId(
				dbHelper.updateOtp(spCCode.getText().toString().trim() + etPhone.getText().toString(), user.getOtp()));

		Intent intent = new Intent(ForgetPasswordActivity.this, OTPActivity.class);
		intent.putExtra(Const.USER, user);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
		finish();
		AndyUtils.hideKeyboard(this, etPhone);

		// AndyUtils.showSimpleProgressDialog(this);
		// HashMap<String, String> map = new HashMap<String, String>();
		// map.put(Const.URL, Const.ServiceType.FORGET_PASSWORD);
		// map.put(Const.Params.PHONE, spCCode.getText().toString().trim()
		// + etPhone.getText().toString());
		// new HttpRequester(this, map, Const.ServiceCode.FORGET_PASSWORD,
		// this);
	}

	// @Override
	// public void onTaskCompleted(String response, int serviceCode) {
	// // TODO Auto-generated method stub
	// switch (serviceCode) {
	// case Const.ServiceCode.FORGET_PASSWORD:
	// AndyUtils.removeSimpleProgressDialog();
	// Log.d("Forget password ", "forget password " + response);
	// if (parseContent.isSuccess(response)) {
	// try {
	// JSONObject object = new JSONObject(response);
	// gotoOtpActivity(this, false,
	// object.getInt(Const.Params.ID),
	// object.getString(Const.Params.TOKEN));
	// } catch (JSONException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// // startActivity(new Intent(ForgetPasswordActivity.this,
	// // OTPActivity.class));
	// // finish();
	// }
	// break;
	//
	// default:
	// break;
	// }
	//
	// }

}
