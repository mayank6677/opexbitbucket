package com.example.opex_new;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.TransactionDetails;

import android.app.Activity;
import android.os.Bundle;

public class InAppBillingActivity extends Activity implements BillingProcessor.IBillingHandler {
  BillingProcessor bp;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_in_app_billing);

    bp = new BillingProcessor(this, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5oiHqcI7tJggr1a+l19F+WQ9kwYSMP3f0pzXt5OC1hCVYWVBqcNwNlb7jL3pImYmT62oKIvch7f+HOWsYUovRKd5HlRh1XmPEAPoxw+y1TOi9ztxbjzHZ62lJUGf82YNWtl1mnzyod1NaBist7J0ph0ejNVI/y6cwBKDWf57u7dR83XytLWxDY4O7/OqBSwDUJnVPqF99JN9I9U6KV+DOpFMTgs0IeaGd0LXE5ok1FymtVfPOVs7xiourkM5yYcUAuoyWefORyYB/nBXnZ5I1zr7oc8KEsyRhK89SD0uHM90IAowfnAzHkbm1YqoQTfrwIqafA9kvNr1WgCyLG6FhQIDAQAB", this);
    bp.initialize();
    // or bp = BillingProcessor.newBillingProcessor(this, "YOUR LICENSE KEY FROM GOOGLE PLAY CONSOLE HERE", this);
    // See below on why this is a useful alternative
  }
	
  // IBillingHandler implementation
	
  @Override
  public void onBillingInitialized() {
    /*
    * Called when BillingProcessor was initialized and it's ready to purchase 
    */
  }
	
  @Override
  public void onProductPurchased(String productId, TransactionDetails details) {
    /*
    * Called when requested PRODUCT ID was successfully purchased
    */
  }
	
  @Override
  public void onBillingError(int errorCode, Throwable error) {
    /*
    * Called when some error occurred. See Constants class for more details
    * 
    * Note - this includes handling the case where the user canceled the buy dialog:
    * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
    */
  }
	
  @Override
  public void onPurchaseHistoryRestored() {
    /*
    * Called when purchase history was restored and the list of all owned PRODUCT ID's 
    * was loaded from Google Play
    */
  }
}