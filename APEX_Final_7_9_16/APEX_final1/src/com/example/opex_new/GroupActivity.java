package com.example.opex_new;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.adapter.ContactListAdapter;
import com.example.opex_new.fragment.AddTempleteFragment;
import com.example.opex_new.fragment.AnalyticsFragment;
import com.example.opex_new.fragment.AutoRelayFragment;
import com.example.opex_new.fragment.CreateMessageFragment;
import com.example.opex_new.fragment.SettingFragment;
import com.example.opex_new.fragment.TempleteFragment;
import com.example.opex_new.fragment.ViewGroupFragment;
import com.example.opex_new.fragment.delete_pollsFragment;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

public class GroupActivity extends ActionBarBaseActivity {

	public Dialog addMemberDialog;
	private ListView lvContact;
	public ArrayList<Member> contactList;
	private ContactListAdapter listAdapter;
	private EditText etSearchContact;
	private Button btnDone;
	LinearLayout linear;
	LinearLayout content_frame;
	Map<String, String> namePhoneMap = new HashMap<String, String>();
	public boolean isOnGroupDetail=false;

	// private String SEND = "send", DELIVERED = "delivered";
	// private BroadcastReceiver sendReceiver, deliveredReceiver;
	// public PendingIntent sendPendingIntent, deliveredPendingIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group);
		actionBar.hide();
		content_frame = (LinearLayout) findViewById(R.id.content_frame);
		contactList = new ArrayList<Member>();

		if (getIntent().getIntExtra(Const.POSITION, 0) == Const.CREATE_MESSAGE) {
			addFragment(new CreateMessageFragment(), false, Const.FRAGMENT_CREATE_MESSAGE);
		} else if (getIntent().getIntExtra(Const.POSITION, 0) == Const.GROUPS) {
			addFragment(new ViewGroupFragment(), false, Const.FRAGMENT_VIEW_GROUP);
		} else if (getIntent().getIntExtra(Const.POSITION, 0) == Const.TEMPLETES) {
			addTempleteFragment(false, null, false, 0);
		} else if (getIntent().getIntExtra(Const.POSITION, 0) == Const.SETTIINGS) {
			addFragment(new SettingFragment(), false, Const.FRAGMENT_SETTINGS);
		} else if (getIntent().getIntExtra(Const.POSITION, 0) == Const.AUTO_RELAY) {
			addFragment(new AutoRelayFragment(), false, Const.FRAGMENT_AUTO_RELAY);
		} else if (getIntent().getIntExtra(Const.POSITION, 0) == Const.REPORT_VIEW) {
			addFragment(new AnalyticsFragment(), false, Const.FRAGMENT_ANALYTICS);
		}

		// Intent sendIntent = new Intent(SEND);
		// Intent deliveredIntent = new Intent(DELIVERED);
		//
		// sendPendingIntent = PendingIntent.getBroadcast(this, 0, sendIntent,
		// 0);
		// deliveredPendingIntent = PendingIntent.getBroadcast(this, 0,
		// deliveredIntent, 0);
		// defineReceivers();

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.ivBack:
			onBackPressed();
			break;
		default:
			break;
		}

	}

	public void showAddMemberDialog(OnItemClickListener listener, ArrayList<Member> selectedMemberList) {
		Log.e("Here Dialog", "True");
		if(isOnGroupDetail==true){
			addMemberDialog=null;
			isOnGroupDetail=false;
		}
		if (addMemberDialog == null) {
			addMemberDialog = new Dialog(this);
			addMemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			addMemberDialog.setContentView(R.layout.activity_contact_list);
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(addMemberDialog.getWindow().getAttributes());
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;
			addMemberDialog.getWindow().setAttributes(lp);

			lvContact = (ListView) addMemberDialog.findViewById(R.id.lvContact);

			lvContact.setOnItemClickListener(listener);
			listAdapter = new ContactListAdapter(this, contactList, selectedMemberList);
			lvContact.setAdapter(listAdapter);
			etSearchContact = (EditText) addMemberDialog.findViewById(R.id.etSearchContact);
			etSearchContact.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					listAdapter.filterMember().filter(s);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			linear = (LinearLayout) inflater.inflate(R.layout.activity_contact_list, null);
			btnDone = (Button) linear.findViewById(R.id.btn_done);
			btnDone.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					addMemberDialog.dismiss();
					finish();
				}
			});
			
			
			
			
			content_frame.addView(linear);

			
			    Cursor cursor = null;
			    try {
			    	cursor = getContentResolver().query(Phone.CONTENT_URI, null, null, null,
					ContactsContract.Contacts.DISPLAY_NAME + " ASC ");
			    } catch (SecurityException e) {
			        //SecurityException can be thrown if we don't have the right permissions
			    }

			    if (cursor != null) {
			        try {
			            HashSet<String> normalizedNumbersAlreadyFound = new HashSet<String>();
			            int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
			            int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
			            int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
			        	int photoIdIdx = cursor.getColumnIndex(Phone.PHOTO_URI);
			        	contactList.clear();
			            while (cursor.moveToNext()) {
			                String normalizedNumber = cursor.getString(indexOfNormalizedNumber);
			            	Member member = new Member();
			                if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
			                    String displayName = cursor.getString(indexOfDisplayName);
			                    String displayNumber = cursor.getString(indexOfDisplayNumber);
			                    
			                    displayNumber = displayNumber.replaceAll("[()\\s-]+", "");
							String photo = cursor.getString(photoIdIdx);
							AppLog.Log(Const.TAG, "photo id" + photo);
								//
//							if (photo != null) {
//								 Bitmap bitmap = MediaStore.Images.Media.getBitmap(
//								 getContentResolver(), Uri.parse(photo));
//								
//								 ByteArrayOutputStream bos = new
//									bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
//									 byte[] bArray = bos.toByteArray();
//									member.setPhoto(photo);
//								}
								if (!contactList.contains(member)) {
									member.setName(displayName);
									member.setPhone(displayNumber);
		
								
			
								}
								boolean foundNumber = false;
								for (Member member2 : contactList) {
									if (member2.getPhone() == displayNumber) {
										foundNumber = true;
										Log.e("Found Number", "True");
									}
								}
								if (foundNumber == false) {
									contactList.add(member);
								}
			                    
			                    
			                    
			                    //haven't seen this number yet: do something with this contact!
			                } else {
			                	contactList.remove(member);
			                    //don't do anything with this contact because we've already found this number
			                }
			            }
			        } finally {
			            cursor.close();
			        }
			    }
			
			
			
			
			
			

			if (selectedMemberList != null) {
				if (selectedMemberList.size() >= 25) {
								
				AppLog.Log(Const.TAG, "" + selectedMemberList.size());
				for (int i = 0; i < selectedMemberList.size(); i++) {
					for (int j = 0; j < contactList.size(); j++) {
						if (contactList.get(j).getPhone().equals(selectedMemberList.get(i).getPhone())) {
							contactList.remove(j);
						}
						if (contactList.get(j).getName().equals(selectedMemberList.get(i).getName())) {
							contactList.remove(j);
						}
					}
					AppLog.Log(Const.TAG, "contact" + contactList.size());
				}
			}
			}

			listAdapter.notifyDataSetChanged();

			addMemberDialog.setOnDismissListener(new OnDismissListener() {
				

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//addMemberDialog = null;
					addMemberDialog.hide();
					AppLog.Log(Const.TAG, "member dialog dismiss condition");
					
				}
			});

			SetBackgroundColor(etSearchContact);
			// setFontSize(etSearchContact);
			setFonts(etSearchContact);

			addMemberDialog.show();

		}
		else{
			AppLog.Log(Const.TAG, "member dialog else condition");
			SetBackgroundColor(etSearchContact);
			// setFontSize(etSearchContact);
			setFonts(etSearchContact);

			addMemberDialog.show();
			
		}
	}

	
	

	public void showAddMemberDialogGroupDetails(OnItemClickListener listener, ArrayList<Member> selectedMemberList) {
		Log.e("Here Dialog", "True");
		if(isOnGroupDetail==false){
			addMemberDialog=null;
			isOnGroupDetail=true;
		}
		
		if (addMemberDialog == null) {
			addMemberDialog = new Dialog(this);
			addMemberDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			addMemberDialog.setContentView(R.layout.activity_contact_list);
			WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
			lp.copyFrom(addMemberDialog.getWindow().getAttributes());
			lp.width = LayoutParams.MATCH_PARENT;
			lp.height = LayoutParams.MATCH_PARENT;
			lp.gravity = Gravity.CENTER;
			addMemberDialog.getWindow().setAttributes(lp);

			lvContact = (ListView) addMemberDialog.findViewById(R.id.lvContact);

			lvContact.setOnItemClickListener(listener);
			listAdapter = new ContactListAdapter(this, contactList, selectedMemberList);
			lvContact.setAdapter(listAdapter);
			etSearchContact = (EditText) addMemberDialog.findViewById(R.id.etSearchContact);
			etSearchContact.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					listAdapter.filterMember().filter(s);
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					// TODO Auto-generated method stub

				}

				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub

				}
			});
			final LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			linear = (LinearLayout) inflater.inflate(R.layout.activity_contact_list, null);
			btnDone = (Button) linear.findViewById(R.id.btn_done);
			btnDone.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub

					addMemberDialog.dismiss();
					finish();
				}
			});
			
			
			
			
			content_frame.addView(linear);

			
			    Cursor cursor = null;
			    try {
			    	cursor = getContentResolver().query(Phone.CONTENT_URI, null, null, null,
					ContactsContract.Contacts.DISPLAY_NAME + " ASC ");
			    } catch (SecurityException e) {
			        //SecurityException can be thrown if we don't have the right permissions
			    }

			    if (cursor != null) {
			        try {
			            HashSet<String> normalizedNumbersAlreadyFound = new HashSet<String>();
			            int indexOfNormalizedNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
			            int indexOfDisplayName = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
			            int indexOfDisplayNumber = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
			        	int photoIdIdx = cursor.getColumnIndex(Phone.PHOTO_URI);
			        	contactList.clear();
			            while (cursor.moveToNext()) {
			                String normalizedNumber = cursor.getString(indexOfNormalizedNumber);
			            	Member member = new Member();
			                if (normalizedNumbersAlreadyFound.add(normalizedNumber)) {
			                    String displayName = cursor.getString(indexOfDisplayName);
			                    String displayNumber = cursor.getString(indexOfDisplayNumber);
			                    
			                    displayNumber = displayNumber.replaceAll("[()\\s-]+", "");
							String photo = cursor.getString(photoIdIdx);
							AppLog.Log(Const.TAG, "photo id" + photo);
								//
//							if (photo != null) {
//								 Bitmap bitmap = MediaStore.Images.Media.getBitmap(
//								 getContentResolver(), Uri.parse(photo));
//								
//								 ByteArrayOutputStream bos = new
//									bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
//									 byte[] bArray = bos.toByteArray();
//									member.setPhoto(photo);
//								}
								if (!contactList.contains(member)) {
									member.setName(displayName);
									member.setPhone(displayNumber);
		
								
			
								}
								boolean foundNumber = false;
								for (Member member2 : contactList) {
									if (member2.getPhone() == displayNumber) {
										foundNumber = true;
										Log.e("Found Number", "True");
									}
								}
								if (foundNumber == false) {
									contactList.add(member);
								}
			                    
			                    
			                    
			                    //haven't seen this number yet: do something with this contact!
			                } else {
			                	contactList.remove(member);
			                    //don't do anything with this contact because we've already found this number
			                }
			            }
			        } finally {
			            cursor.close();
			        }
			    }
			
			
			
			
			
			

			if (selectedMemberList != null) {
				if (selectedMemberList.size() >= 25) {
								
				AppLog.Log(Const.TAG, "" + selectedMemberList.size());
				for (int i = 0; i < selectedMemberList.size(); i++) {
					for (int j = 0; j < contactList.size(); j++) {
						if (contactList.get(j).getPhone().equals(selectedMemberList.get(i).getPhone())) {
							contactList.remove(j);
						}
						if (contactList.get(j).getName().equals(selectedMemberList.get(i).getName())) {
							contactList.remove(j);
						}
					}
					AppLog.Log(Const.TAG, "contact" + contactList.size());
				}
			}
			}

			listAdapter.notifyDataSetChanged();

			addMemberDialog.setOnDismissListener(new OnDismissListener() {
				

				@Override
				public void onDismiss(DialogInterface dialog) {
					// TODO Auto-generated method stub
					//addMemberDialog = null;
					addMemberDialog.hide();
					AppLog.Log(Const.TAG, "member dialog dismiss condition");
					
				}
			});

			SetBackgroundColor(etSearchContact);
			// setFontSize(etSearchContact);
			setFonts(etSearchContact);

			addMemberDialog.show();

		}
		else{
			AppLog.Log(Const.TAG, "member dialog else condition");
			SetBackgroundColor(etSearchContact);
			// setFontSize(etSearchContact);
			setFonts(etSearchContact);

			addMemberDialog.show();
			
		}
	}

	
	// public void defineReceivers() {
	// sendReceiver = new BroadcastReceiver() {
	//
	// @Override
	// public void onReceive(Context context, Intent intent) {
	// // TODO Auto-generated method stub
	// switch (getResultCode()) {
	// case Activity.RESULT_OK:
	// AppLog.Log(Const.TAG, "sendReceiver RESULT_OK");
	// break;
	// case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
	// AppLog.Log(Const.TAG,
	// "sendReceiver RESULT_ERROR_GENERIC_FAILURE");
	// break;
	// case SmsManager.RESULT_ERROR_NO_SERVICE:
	// AppLog.Log(Const.TAG,
	// "sendReceiver RESULT_ERROR_NO_SERVICE");
	// break;
	// case SmsManager.RESULT_ERROR_NULL_PDU:
	// AppLog.Log(Const.TAG, "sendReceiver RESULT_ERROR_NULL_PDU");
	// break;
	// case SmsManager.RESULT_ERROR_RADIO_OFF:
	// AppLog.Log(Const.TAG, "sendReceiver RESULT_ERROR_RADIO_OFF");
	// break;
	// default:
	// break;
	// }
	// }
	// };
	//
	// deliveredReceiver = new BroadcastReceiver() {
	//
	// @Override
	// public void onReceive(Context context, Intent intent) {
	// // TODO Auto-generated method stub
	// switch (getResultCode()) {
	// case Activity.RESULT_OK:
	// AppLog.Log(Const.TAG, "deliveredReceiver RESULT_OK");
	// break;
	// case Activity.RESULT_CANCELED:
	// AppLog.Log(Const.TAG, "deliveredReceiver RESULT_CANCELED");
	// break;
	// default:
	// break;
	// }
	// }
	// };
	//
	// }

	public void addTempleteFragment(boolean isForMessaging,
			/* ArrayList<Member> memberList, */Group selectedGroup, boolean isBroadcast, int msgStatus) {
		TempleteFragment fragment = new TempleteFragment();
		Bundle bundle = new Bundle();
		// bundle.putParcelableArrayList(Const.Params.MEMBER, memberList);
		bundle.putSerializable(Const.Params.GROUP, selectedGroup);
		bundle.putBoolean(Const.ISFORMESSAGING, isForMessaging);
		bundle.putBoolean(Const.ISBROADCAST, isBroadcast);
		bundle.putInt(Const.MSGSTATUS, msgStatus);
		fragment.setArguments(bundle);
		if (isForMessaging) {
			addFragment(fragment, true, Const.FRAGMENT_TEMPLETE);
		} else {
			addFragment(fragment, false, Const.FRAGMENT_TEMPLETE);
		}

	}

	// public void sendSMS(String phoneNo, String message) {
	// SmsManager smsManager = SmsManager.getDefault();
	// smsManager.sendTextMessage(phoneNo, null, message, sendPendingIntent,
	// deliveredPendingIntent);
	// }

	public void addTemplateFragment(String category) {
		AddTempleteFragment fragment = new AddTempleteFragment();
		Bundle bundle = new Bundle();
		bundle.putString(Const.CATEGORY, category);
		fragment.setArguments(bundle);
		addFragment(fragment, true, Const.FRAGMENT_ADD_TEMPLETE);
	}

	public void addFragment(delete_pollsFragment delete_pollsFragment, boolean addToBackStack, String delete_polls) {
		// TODO Auto-generated method stub
		
	}

}
