package com.example.opex_new;

import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;
import com.example.opex_new.model.User;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends ActionBarBaseActivity {

	private EditText etUserName, /* etPassword, */etNumber;
	private Button btnNext, btnCancel;
	public PreferenceHelper phelper;
	private TextView spCCode;
	private CheckBox cbTerms;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		actionBar.hide();
		setContentView(R.layout.register);

		etUserName = (EditText) findViewById(R.id.etUserName);
		etNumber = (EditText) findViewById(R.id.etNumber);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		// etPassword = (EditText) findViewById(R.id.etPassword);
		btnNext = (Button) findViewById(R.id.btnSubmit);
		spCCode = (TextView) findViewById(R.id.spCCode);
		cbTerms = (CheckBox) findViewById(R.id.cbTerms);
	
		btnCancel.setOnClickListener(this);
		phelper = new PreferenceHelper(this);
		btnNext.setOnClickListener(this);
		spCCode.setOnClickListener(this);
	}


	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		setFonts(btnNext);
		// setFontSize(btnNext);
	//	SetBackgroundColor(btnNext);

		//setFonts(btnCancel);
		// setFontSize(btnCancel);
	//	SetBackgroundColor(btnCancel);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		switch (v.getId()) {

		case R.id.btnCancel:
			onBackPressed();
			break;
		case R.id.btnSubmit:
			if (TextUtils.isEmpty(etUserName.getText())) {
				AndyUtils.showToast(getString(R.string.enter_user_name), this);
				etUserName.requestFocus();
			} else if (TextUtils.isEmpty(etNumber.getText())) {
				AndyUtils.showToast(getString(R.string.enter_numberr), this);
			} else if (etNumber.getText().toString().length() != 10) {
				AndyUtils.showToast(getString(R.string.enter_valid_numberr), this);
		
			} else if (!cbTerms.isChecked()) {
			
				AndyUtils.showToast(getString(R.string.accept_terms), this);
			} else {
				// register();
				showConfirmNumberDialog();
			}
			break;
		case R.id.spCCode:
			showCountryCodeDialog(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// TODO Auto-generated method stub
					spCCode.setText(list.get(position).substring(0, list.get(position).indexOf(" ")));
					countryCodeDialog.dismiss();
				}
			});
			break;

		default:
			break;
		}

	}

	private void register() {

		AppLog.Log(Const.TAG, "spc " + spCCode.getText().toString().trim());
		User user = new User();
		user.setName(etUserName.getText().toString());
		user.setContact(spCCode.getText().toString().trim() + etNumber.getText().toString());
		user.setOtp(generateOtp());
		dbHelper.registerUser(user);

		if (user.getUserId() == 0) {
			onBackPressed();
		} else {
			Intent intent = new Intent(RegisterActivity.this, OTPActivity.class);
			intent.putExtra(Const.USER, user);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
			finish();
		}
		AndyUtils.hideKeyboard(this, etNumber);
	}

	private void showConfirmNumberDialog() {
		new AlertDialog.Builder(this).setTitle(getString(R.string.text_confirm_number))
				.setMessage(getString(R.string.text_your_mobile_number) + "\n " + spCCode.getText().toString().trim() + " "
						+ etNumber.getText().toString() + "?")
				.setPositiveButton(getString(R.string.text_ok), new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						register();
					}
				}).setNegativeButton(getString(R.string.text_edit), new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.dismiss();
						etNumber.requestFocus();
					}
				}).show();
	}
}
