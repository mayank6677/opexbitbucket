package com.example.opex_new;

import java.util.ArrayList;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.opex_new.adapter.ContactListAdapter;
import com.example.opex_new.model.Contact;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;

public class ContactListActivity extends ActionBarBaseActivity implements OnClickListener {

	private final int PICK_CONTACT = 1;
	private ListView lvContact;
	private ContactListAdapter listAdapter;
	private ArrayList<Contact> contactList;
	private ArrayList<CharSequence> phoneNumberList;
	private RequestQueue requestQueue;
	private Button btnCreateGroup;
	public GroupActivity activity;
	Button btnDone;
	LinearLayout linear;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		activity = getActivity();
		requestQueue = Volley.newRequestQueue(this);
		contactList = new ArrayList<Contact>();
		phoneNumberList = new ArrayList<CharSequence>();

		setContentView(R.layout.activity_contact_list);
		lvContact = (ListView) findViewById(R.id.lvContact);
		btnCreateGroup = (Button) findViewById(R.id.btnOkContact);
		// listAdapter = new ContactListAdapter(this, contactList);
		lvContact.setAdapter(listAdapter);
		btnCreateGroup.setOnClickListener(this);

		Cursor cursor = null;
		try {
			cursor = getContentResolver().query(Phone.CONTENT_URI, null, null, null, null);
			int contactIdIdx = cursor.getColumnIndex(BaseColumns._ID);
			int nameIdx = cursor.getColumnIndex(Phone.DISPLAY_NAME);
			int phoneNumberIdx = cursor.getColumnIndex(Phone.NUMBER);
			int photoIdIdx = cursor.getColumnIndex(Phone.PHOTO_ID);

			Log.d("", "cursor" + cursor.getCount());
			cursor.moveToFirst();
			do {
				Contact contact = new Contact();
				String idContact = cursor.getString(contactIdIdx);
				String name = cursor.getString(nameIdx);
				String phoneNumber = cursor.getString(phoneNumberIdx);

				contact.setName(name);
				contact.setContact(phoneNumber);
				contactList.add(contact);
				// ...
			} while (cursor.moveToNext());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			listAdapter.notifyDataSetChanged();
			if (cursor != null) {
				cursor.close();
			}
		}

		// getContact();
		/*
		 * btnDone.setOnClickListener(new View.OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * if(listAdapter.selectedContactsList.contactArrayList.isEmpty()){
		 * setResult(RESULT_CANCELED); } else{
		 * 
		 * Intent resultIntent = new Intent();
		 * 
		 * resultIntent.putParcelableArrayListExtra("SelectedContacts",
		 * listAdapter.selectedContactsList.contactArrayList);
		 * setResult(RESULT_OK,resultIntent);
		 * 
		 * } finish();
		 * 
		 * } });
		 */

	}

	private GroupActivity getActivity() {
		// TODO Auto-generated method stub
		return null;
	}

	private void getContact() {
		Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, PICK_CONTACT);
	}

	@Override
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);

		switch (reqCode) {
		case (PICK_CONTACT):
			if (resultCode == Activity.RESULT_OK) {
				Cursor cursor = null;
				try {
					cursor = getContentResolver().query(Phone.CONTENT_URI, null, null, null, null);
					int contactIdIdx = cursor.getColumnIndex(BaseColumns._ID);
					int nameIdx = cursor.getColumnIndex(Phone.DISPLAY_NAME);
					int phoneNumberIdx = cursor.getColumnIndex(Phone.NUMBER);
					int photoIdIdx = cursor.getColumnIndex(Phone.PHOTO_ID);
					cursor.moveToFirst();
					do {
						Contact contact = new Contact();
						String idContact = cursor.getString(contactIdIdx);
						String name = cursor.getString(nameIdx);
						String phoneNumber = cursor.getString(phoneNumberIdx);

						contact.setName(name);
						contact.setContact(phoneNumber);
						contactList.add(contact);
						// ...
					} while (cursor.moveToNext());
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (cursor != null) {
						cursor.close();
					}
					listAdapter.notifyDataSetChanged();
				}
			}
			break;
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		for (int i = 0; i < contactList.size(); i++) {
			if (contactList.get(i).isChecked) {
				phoneNumberList.add(contactList.get(i).getContact());
			}
		}
		// AddGroup();
	}

	// private void AddGroup() {
	//
	// // if (!AndyUtils.isNetworkAvailable(activity)) {
	// // AndyUtils.showToast(getResources().getString(R.string.no_internet),
	// // activity);
	// // return;
	// // }
	// // AndyUtils.showCustomProgressDialog(activity,
	// // getResources().getString(R.string.text_signing), false, null);
	// HashMap<String, String> map = new HashMap<String, String>();
	// map.put(Const.URL, Const.ServiceType.ADD_MEMBER);
	// map.put(Const.Params.ID, new PreferenceHelper(this).getUserId());
	// map.put(Const.Params.TOKEN,
	// new PreferenceHelper(this).getSessionToken());
	// map.put(Const.Params.PHONE, "" + TextUtils.join(",", phoneNumberList));
	// map.put(Const.Params.GROUP_NAME, "aaa");
	// // new HttpRequester(activity, map, Const.ServiceCode.LOGIN, this);
	// requestQueue.add(new VolleyHttpRequest(Method.POST, map,
	// Const.ServiceCode.ADD_MEMBER, this, null));
	// }

}
