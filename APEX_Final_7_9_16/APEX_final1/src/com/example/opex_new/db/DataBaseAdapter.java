package com.example.opex_new.db;

import java.util.ArrayList;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.BroadcastPart;
import com.example.opex_new.model.Feedback;
import com.example.opex_new.model.FeedbackPart;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;
import com.example.opex_new.model.Question;
import com.example.opex_new.model.Relay;
import com.example.opex_new.model.Templete;
import com.example.opex_new.model.User;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import android.util.Log;

public class DataBaseAdapter {

	private static final String DATABASE_NAME = "OPEXDB";
	private static final int DATABASE_VERSION = 1;

	private DataBaseHelper dBHelper;
	private SQLiteDatabase db;
	private Context context;

	private static final String TABLE_GROUP = "table_group";
	private static final String KEY_GROUP_ID = "group_id";
	private static final String KEY_GROUP_NAME = "group_name";
	private static final String KEY_GROUP_PICTURE = "group_picture";

	private static final String CREATE_TABLE_GROUP = "create table if not exists " + TABLE_GROUP + "( " + KEY_GROUP_ID
			+ " integer primary key autoincrement," + KEY_GROUP_NAME + " text not null," + KEY_GROUP_PICTURE
			+ " text);";

	private static final String TABLE_MEMBER = "member";
	private static final String KEY_MEMBER_ID = "member_id";
	private static final String KEY_MEMBER_NAME = "member_name";
	private static final String KEY_MEMBER_PHONE = "member_phone";
	private static final String KEY_MEMBER_PHOTO = "member_photo";
	private static final String CREATE_TABLE_MEMBER = "create table " + TABLE_MEMBER + " (" + KEY_MEMBER_ID
			+ " integer primary key autoincrement," + KEY_GROUP_ID + " integer not null," + KEY_MEMBER_NAME
			+ " text not null," + KEY_MEMBER_PHONE + " text not null," + KEY_MEMBER_PHOTO + " text);";

	private static final String TABLE_TEMPLETE = "templete";
	private static final String KEY_TEMPLETE_ID = "templete_id";
	private static final String KEY_TEMPLETE_NAME = "templete_name";
	private static final String KEY_TEMPLETE_DATA = "templete_data";
	private static final String KEY_CATEGORY_NAME = "category_name";
	private static final String CREATE_TABLE_TEMPLETE = "create table " + TABLE_TEMPLETE + " (" + KEY_TEMPLETE_ID
			+ " integer primary key autoincrement," + KEY_CATEGORY_NAME + " text not null," + KEY_TEMPLETE_DATA
			+ " longtext not null," + KEY_TEMPLETE_NAME + " text not null);";

	private static final String TABLE_USER = "user";
	private static final String KEY_USER_ID = "user_id";
	private static final String KEY_USER_NAME = "user_name";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_MOBILE_NO = "mobile_no";
	private static final String KEY_OTP = "otp";
	private static final String KEY_NAME = "name";
	private static final String KEY_PHOTO = "photo";
	private static final String CREATE_TABLE_USER = "create table " + TABLE_USER + " (" + KEY_USER_ID
			+ " integer primary key autoincrement," + KEY_USER_NAME + " text not null," + KEY_PASSWORD + " text,"
			+ KEY_MOBILE_NO + " text not null," + KEY_OTP + " integer not null," + KEY_NAME + " text," + KEY_PHOTO
			+ " text);";

	private static final String TABLE_AURO_RELAY = "auto_relay";
	private static final String KEY_RELAY_ID = "relay_id";
	private static final String KEY_RELAY_NUMBER = "relay_number";
	private static final String KEY_RELAY_NAME = "relay_name";
	private static final String KEY_RELAY_PREFIX = "relay_prefix";
	private static final String CREATE_TABLE_AUTO_RELAY = "create table " + TABLE_AURO_RELAY + " (" + KEY_RELAY_ID
			+ " integer primary key autoincrement," + KEY_RELAY_NUMBER + " text not null," + KEY_RELAY_NAME
			+ " text not null," + KEY_RELAY_PREFIX + " text not null," + KEY_GROUP_ID + " integer not null);";

	private static final String TABLE_BROADCAST = "broacast";
	private static final String KEY_BROADCAST_ID = "broacast_id";
	private static final String KEY_BROADCAST_TIME = "broacast_time";
	private static final String KEY_BROADCAST_MSG = "broacast_msg";
	private static final String KEY_BROADCAST_SENT = "broacast_sent";
	private static final String KEY_BROADCAST_DELIVERED = "broacast_delivered";
	private static final String KEY_BROADCAST_STATUS = "broadcast_status";
	private static final String CREATE_TABLE_BROADCAST = "create table " + TABLE_BROADCAST + " (" + KEY_BROADCAST_ID
			+ " integer primary key autoincrement," + KEY_BROADCAST_TIME + " datetime not null," + KEY_GROUP_ID
			+ " integer not null," + KEY_BROADCAST_SENT + " integer," + KEY_BROADCAST_DELIVERED + " integer,"
			+ KEY_BROADCAST_STATUS + " integer not null," + KEY_BROADCAST_MSG + " longtext not null);";

	private static final String TABLE_BROADCAST_PART = "broacast_part";
	private static final String KEY_BROADCAST_PART_ID = "broacast_part_id";
	private static final String KEY_BROADCAST_PART_MSG = "broacast_part_msg";
	private static final String KEY_BROADCAST_PART_MEMBER_ID = "broacast_part_member_id";
	private static final String KEY_BROADCAST_PART_TIME = "broacast_part_time";
	private static final String KEY_BROADCAST_PART_SENT = "broacast_part_sent";
	private static final String KEY_BROADCAST_PART_DELIVERED = "broacast_part_delivered";
	private static final String KEY_BROADCAST_PART_RETRY = "broacast_part_retry";
	private static final String CREATE_TABLE_BROADCAST_PART = "create table " + TABLE_BROADCAST_PART + " ("
			+ KEY_BROADCAST_PART_ID + " integer primary key autoincrement," + KEY_BROADCAST_PART_MSG
			+ " longtext not null," + KEY_BROADCAST_PART_MEMBER_ID + " integer not null," + KEY_BROADCAST_PART_TIME
			+ " datetime not null," + KEY_BROADCAST_ID + " integer not null," + KEY_BROADCAST_PART_SENT + " integer,"
			+ KEY_BROADCAST_PART_DELIVERED + " integer," + KEY_BROADCAST_PART_RETRY + " integer);";

	private static final String TABLE_FEEDBACK = "feedback";
	private static final String KEY_FEEDBACK_ID = "feedback_id";
	private static final String KEY_FEEDBACK_TIME = "feedback_time";
	private static final String KEY_FEEDBACK_SENT = "feedback_sent";
	private static final String KEY_FEEDBACK_DELIVERED = "feedback_delivered";
	private static final String KEY_FEEDBACK_RECEIVED = "feedback_received";
	private static final String KEY_FEEDBACK_STATUS = "feedback_status";
	private static final String KEY_FEEDBACK_ANSWER_A = "feedback_answer_a";
	private static final String KEY_FEEDBACK_ANSWER_B = "feedback_answer_b";
	private static final String KEY_FEEDBACK_ANSWER_C = "feedback_answer_c";
	private static final String KEY_FEEDBACK_ANSWER_D = "feedback_answer_d";
	private static final String KEY_FEEDBACK_ANSWER_E = "feedback_answer_e";
	private static final String KEY_FEEDBACK_MSG = "feedback_msg";

	private static final String CREATE_TABLE_FEEDBACK = "create table " + TABLE_FEEDBACK + " (" + KEY_FEEDBACK_ID
			+ " integer primary key autoincrement," + KEY_FEEDBACK_TIME + " text not null," + KEY_GROUP_ID
			+ " integer not null," + KEY_FEEDBACK_SENT + " integer," + KEY_FEEDBACK_DELIVERED + " integer,"
			+ KEY_FEEDBACK_RECEIVED + " integer," + KEY_FEEDBACK_STATUS + " integer," + KEY_FEEDBACK_ANSWER_A
			+ " integer," + KEY_FEEDBACK_ANSWER_B + " integer," + KEY_FEEDBACK_ANSWER_C + " integer,"
			+ KEY_FEEDBACK_ANSWER_D + " integer," + KEY_FEEDBACK_ANSWER_E + " integer," + KEY_FEEDBACK_MSG
			+ " longtext);";

	private static final String TABLE_FEEDBACK_PART = "feedback_part";
	private static final String KEY_FEEDBACK_PART_ID = "feedback_part_id";
	private static final String KEY_FEEDBACK_PART_MSG = "feedback_part_msg";
	private static final String KEY_FEEDBACK_PART_MEMBER_ID = "feedback_part_member_id";
	private static final String KEY_FEEDBACK_PART_TIME = "feedback_part_time";
	private static final String KEY_FEEDBACK_PART_SENT = "feedback_part_sent";
	private static final String KEY_FEEDBACK_PART_DELIVERED = "feedback_part_delivered";
	private static final String KEY_FEEDBACK_PART_RETRY = "feedback_part_retry";
	private static final String KEY_FEEDBACK_PART_RECEIVED = "feedback_part_received";
	private static final String KEY_FEEDBACK_PART_STATUS = "feedback_part_status";
	private static final String KEY_FEEDBACK_PART_ANSWER_A = "feedback_part_answer_a";
	private static final String KEY_FEEDBACK_PART_ANSWER_B = "feedback_part_answer_b";
	private static final String KEY_FEEDBACK_PART_ANSWER_C = "feedback_part_answer_c";
	private static final String KEY_FEEDBACK_PART_ANSWER_D = "feedback_part_answer_d";
	private static final String KEY_FEEDBACK_PART_ANSWER_E = "feedback_part_answer_e";
	private static final String KEY_FEEDBACK_PART_MEMBER_NUMBER = "feedback_part_member_number";

	private static final String CREATE_TABLE_FEEDBACK_PART = "create table " + TABLE_FEEDBACK_PART + " ("
			+ KEY_FEEDBACK_PART_ID + " integer primary key autoincrement," + KEY_FEEDBACK_PART_MSG + " longtext,"
			+ KEY_FEEDBACK_PART_MEMBER_ID + " integer not null," + KEY_FEEDBACK_PART_TIME + " datetime not null,"
			+ KEY_FEEDBACK_ID + " integer not null," + KEY_FEEDBACK_PART_SENT + " integer,"
			+ KEY_FEEDBACK_PART_DELIVERED + " integer," + KEY_FEEDBACK_PART_STATUS + " integer,"
			+ KEY_FEEDBACK_PART_ANSWER_A + " integer," + KEY_FEEDBACK_PART_ANSWER_B + " integer,"
			+ KEY_FEEDBACK_PART_ANSWER_C + " integer," + KEY_FEEDBACK_PART_ANSWER_D + " integer,"
			+ KEY_FEEDBACK_PART_ANSWER_E + " integer," + KEY_FEEDBACK_PART_RETRY + " integer,"
			+ KEY_FEEDBACK_PART_RECEIVED + " integer," + KEY_FEEDBACK_PART_MEMBER_NUMBER + " text);";

	private static final String TABLE_QUESTION = "question";
	private static final String KEY_QUESTION_ID = "question_id";
	private static final String KEY_QUESTION_TIME = "question_time";
	private static final String KEY_QUESTION_MSG = "question_msg";
	private static final String KEY_QUESTION_NO = "question_no";
	private static final String KEY_QUESTION_ANSWER_A = "question_answer_a";
	private static final String KEY_QUESTION_ANSWER_B = "question_answer_b";
	private static final String KEY_QUESTION_ANSWER_C = "question_answer_c";

	private static final String KEY_QUESTION_SEND = "question_send";
	private static final String KEY_QUESTION_DELIVERED = "question_delivered";
	private static final String KEY_QUESTION_RECEIVED = "question_received";
	private static final String CREATE_TABLE_QUESTION = "create table " + TABLE_QUESTION + " (" + KEY_QUESTION_ID
			+ " integer primary key autoincrement," + KEY_QUESTION_MSG + " longtext," + KEY_QUESTION_NO
			+ " integer not null," + KEY_FEEDBACK_ID + " integer not null," + KEY_QUESTION_TIME + " datetime not null,"
			+ KEY_QUESTION_SEND + " integer," + KEY_QUESTION_DELIVERED + " integer," + KEY_QUESTION_RECEIVED
			+ " integer," + KEY_QUESTION_ANSWER_A + " integer," + KEY_QUESTION_ANSWER_B + " integer,"
			+ KEY_QUESTION_ANSWER_C + " integer);";

	public DataBaseAdapter(Context context) {
		// TODO Auto-generated constructor stub
		dBHelper = new DataBaseHelper(context);
		this.context = context;
	}

	private static class DataBaseHelper extends SQLiteOpenHelper {

		public DataBaseHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			// db.execSQL(TABLE_CREATE_USER);

			db.execSQL(CREATE_TABLE_GROUP);
			db.execSQL(CREATE_TABLE_MEMBER);
			db.execSQL(CREATE_TABLE_TEMPLETE);
			db.execSQL(CREATE_TABLE_USER);
			db.execSQL(CREATE_TABLE_AUTO_RELAY);
			db.execSQL(CREATE_TABLE_BROADCAST);
			db.execSQL(CREATE_TABLE_BROADCAST_PART);
			db.execSQL(CREATE_TABLE_FEEDBACK);
			System.out.println("feedback table is"+CREATE_TABLE_FEEDBACK);
			db.execSQL(CREATE_TABLE_FEEDBACK_PART);
			System.out.println("feedbackpart table is"+CREATE_TABLE_FEEDBACK_PART);
			db.execSQL(CREATE_TABLE_QUESTION);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_GROUP);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_MEMBER);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_TEMPLETE);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_USER);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_AUTO_RELAY);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_BROADCAST);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_BROADCAST_PART);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_FEEDBACK);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_FEEDBACK_PART);
			db.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_QUESTION);
			onCreate(db);
		}

	}

	public DataBaseAdapter open() throws SQLiteException {
		db = dBHelper.getWritableDatabase();
		return this;
	}

	public void close() {
		dBHelper.close();
		db.close();
	}

	public int getNoOfUser() {
   

		String getNoOfUserQuery = "select * from " + TABLE_USER;
		int count = 0;
		try {
			
			Cursor cursor = db.rawQuery(getNoOfUserQuery, null);
			if (cursor.moveToFirst()) {
				do {
					if (!TextUtils.isEmpty(cursor.getString(2))) {
						count++;
					}
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return count;
	}

	public long registerUser(User user) {
		ContentValues values = new ContentValues();
		values.put(KEY_USER_NAME, user.getName());
		values.put(KEY_MOBILE_NO, user.getContact());
		values.put(KEY_OTP, user.getOtp());
		return db.insert(TABLE_USER, null, values);
	}

	public long addPassword(long userId, String password) {
		ContentValues values = new ContentValues();
		values.put(KEY_PASSWORD, password);
		AppLog.Log("", "id" + userId);
		return db.update(TABLE_USER, values, KEY_USER_ID + "=" + userId, null);
	}

	public int getOTP(long userId) {
	//	 System.out.println("otp is"+getOTP());

		String getOTPQuery = "select " + KEY_OTP + " from " + TABLE_USER + " where " + KEY_USER_ID + "=" + userId;
		try {

			Cursor cursor = db.rawQuery(getOTPQuery, null);
			if (cursor.moveToFirst()) {
				return cursor.getInt(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return 0;
	}

	public String getPassword(long userId) {
		 System.out.println("password is"+getPassword(userId));
		String getOTPQuery = "select " + KEY_PASSWORD + " from " + TABLE_USER + " where " + KEY_USER_ID + "=" + userId;
		try {

			Cursor cursor = db.rawQuery(getOTPQuery, null);
			if (cursor.moveToFirst()) {
				return cursor.getString(0);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return null;
	}

	public User checkUser(String userName) {
		String getPasswordQuery = "select * from " + TABLE_USER + " where " + KEY_USER_NAME + "='" + userName + "'";
		User user = null;
		try {
			Cursor cursor = db.rawQuery(getPasswordQuery, null);
			if (cursor.moveToLast()) {
				user = new User();
				user.setUserId(cursor.getLong(0));
				user.setName(cursor.getString(1));
				user.setPassword(cursor.getString(2));
				user.setContact(cursor.getString(3));

				AppLog.Log("setUserId", "" + user.getUserId());
				AppLog.Log("setName", "" + user.getName());
				AppLog.Log("setPassword", "" + user.getPassword());
				AppLog.Log("setContact", "" + user.getContact());
				AppLog.Log("otp", "" + cursor.getInt(4));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return user;

	}

	public long updateOtp(String mobileNo, int otp) {
		ContentValues values = new ContentValues();
		values.put(KEY_OTP, otp);
		return db.update(TABLE_USER, values, KEY_MOBILE_NO + " LIKE '%" + mobileNo + "%'", null);
	}

	public long addProfile(User user) {
		ContentValues values = new ContentValues();
		values.put(KEY_USER_NAME, user.getName());
		values.put(KEY_PHOTO, user.getPhoto());
		AppLog.Log("", "id" + user.getUserId());
		AppLog.Log("", "name add" + user.getName());
		return db.update(TABLE_USER, values, KEY_USER_ID + "=" + user.getUserId(), null);
	}

	public User getProfile(User user) {
		String getProfileQuery = "select * from " + TABLE_USER + " where " + KEY_USER_ID + "=" + user.getUserId();
		try {
			Cursor cursor = db.rawQuery(getProfileQuery, null);
			if (cursor.moveToLast()) {
				user.setName(cursor.getString(1));
				user.setPhoto(cursor.getString(6));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		AppLog.Log(Const.TAG, "name get" + user.getName());
		return user;
	}

	public long createGroup(Group group) {
		ContentValues values = new ContentValues();
		values.put(KEY_GROUP_NAME, group.getName());
		values.put(KEY_GROUP_PICTURE, group.getPicture());
		return db.insert(TABLE_GROUP, null, values);
	}

	public boolean deleteGroup(int groupId) {

		String selectQuery = "select " + KEY_MEMBER_ID + " from " + TABLE_MEMBER + " where " + KEY_GROUP_ID + "="
				+ groupId;
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {

					int a = db.delete(TABLE_FEEDBACK_PART, KEY_FEEDBACK_PART_MEMBER_ID + "=" + cursor.getInt(0), null);
					AppLog.Log(Const.TAG, "member" + a);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		db.delete(TABLE_MEMBER, KEY_GROUP_ID + "=" + groupId, null);
		db.delete(TABLE_AURO_RELAY, KEY_GROUP_ID + "=" + groupId, null);
		return db.delete(TABLE_GROUP, KEY_GROUP_ID + "=" + groupId, null) > 0;
	}

	public long addMember(Member member, int groupId) {
		ContentValues values = new ContentValues();
		values.put(KEY_MEMBER_NAME, member.getName());
		values.put(KEY_MEMBER_PHONE, member.getPhone());
		values.put(KEY_MEMBER_PHOTO, member.getPhoto());
		values.put(KEY_GROUP_ID, groupId);
		return db.insert(TABLE_MEMBER, null, values);
	}

	public ArrayList<Member> getMemberList(ArrayList<Member> memberList, int groupId) {
		String selectQuery = "select * from " + TABLE_MEMBER + " where " + KEY_GROUP_ID + "=" + groupId;
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					Member member = new Member();
					member.setId(cursor.getInt(0));
					member.setName(cursor.getString(2));
					member.setPhone(cursor.getString(3));
					member.setPhoto(cursor.getString(4));
					memberList.add(member);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return memberList;
	}

	public Member getMember(int memberId) {
		String selectQuery = "select * from " + TABLE_MEMBER + " where " + KEY_MEMBER_ID + "=" + memberId;
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				Member member = new Member();
				member.setId(cursor.getInt(0));
				member.setName(cursor.getString(2));
				member.setPhone(cursor.getString(3));
				member.setPhoto(cursor.getString(4));
				return member;
			}
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	public boolean deleteMember(int memberId) {
		db.delete(TABLE_BROADCAST_PART, KEY_BROADCAST_PART_MEMBER_ID + "=" + memberId, null);
		db.delete(TABLE_FEEDBACK_PART, KEY_FEEDBACK_PART_MEMBER_ID + "=" + memberId, null);
		return db.delete(TABLE_MEMBER, KEY_MEMBER_ID + "=" + memberId, null) > 0;
	}

	public ArrayList<Group> getGroupList(ArrayList<Group> groupList) {
		String selectQuery = "select * from " + TABLE_GROUP;
		System.out.println("group members are"+selectQuery);
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					Group group = new Group();
					group.setId(cursor.getInt(0));
					group.setName(cursor.getString(1));
					group.setPicture(cursor.getString(2));
					groupList.add(group);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return groupList;
	}

	public Group getGroup(int groupId) {
		String selectQuery = "select * from " + TABLE_GROUP + " where " + KEY_GROUP_ID + "=" + groupId;
		Group group = null;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				group = new Group();
				group.setId(cursor.getInt(0));
				group.setName(cursor.getString(1));
				group.setPicture(cursor.getString(2));
				ArrayList<Member> memberList = new ArrayList<Member>();
				group.setMemberList(getMemberList(memberList, groupId));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return group;
	}

	public int getGroupFromName(String groupName) {
		String selectQuery = "select * from " + TABLE_GROUP + " where " + KEY_GROUP_NAME + "='" + groupName + "'";
		int count = 0;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				AppLog.Log(Const.TAG, "getGroupFromName" + count);
				count++;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return count;
	}

	public long addTemplete(Templete templete) {
		ContentValues values = new ContentValues();
		// values.put(KEY_TEMPLETE_ID, templete.getId());
		values.put(KEY_TEMPLETE_NAME, templete.getName());
		values.put(KEY_CATEGORY_NAME, templete.getCategoryName());
		values.put(KEY_TEMPLETE_DATA, templete.getData());
		return db.insert(TABLE_TEMPLETE, null, values);
	}

	public ArrayList<Templete> getTempleteList(ArrayList<Templete> templeteList) {
		Cursor cursor = db.query(TABLE_TEMPLETE, null, null, null, null, null, KEY_CATEGORY_NAME + " ASC");
		if (cursor.moveToFirst()) {
			do {
				Templete templete = new Templete();
				templete.setId(cursor.getInt(0));
				templete.setCategoryName(cursor.getString(1));
				templete.setData(cursor.getString(2));
				templete.setName(cursor.getString(3));
				templeteList.add(templete);
			} while (cursor.moveToNext());
		}

		return templeteList;
	}

	public long updateTemplete(Templete templete) {
		ContentValues values = new ContentValues();
		values.put(KEY_TEMPLETE_DATA, templete.getData());
		return db.update(TABLE_TEMPLETE, values, KEY_TEMPLETE_ID + "=" + templete.getId(), null);
	}

	public boolean deleteTemplete(int templeteId) {
		return db.delete(TABLE_TEMPLETE, KEY_TEMPLETE_ID + "=" + templeteId, null) > 0;
	}

	public boolean deleteFeedback(int feedbackId) {
		return db.delete(TABLE_FEEDBACK, KEY_FEEDBACK_ID + "=" + feedbackId, null) > 0;
	}

	
	
	public long addAutoRelay(Relay relay) {
		ContentValues values = new ContentValues();
		values.put(KEY_RELAY_NUMBER, relay.getNumber());
		values.put(KEY_RELAY_NAME, relay.getName());
		values.put(KEY_RELAY_PREFIX, relay.getPrefix());
		values.put(KEY_GROUP_ID, relay.getGroup().getId());

		AppLog.Log(Const.TAG, "addAutoRelay id" + relay.getId());
		AppLog.Log(Const.TAG, "addAutoRelay number" + relay.getNumber());
		AppLog.Log(Const.TAG, "addAutoRelay prefix" + relay.getPrefix());
		AppLog.Log(Const.TAG, "addAutoRelay group" + relay.getGroup());
		return db.insert(TABLE_AURO_RELAY, null, values);
	}

	// public Relay getRelay(String number, Relay relay) {
	// String selectQuery = "select * from " + TABLE_AURO_RELAY + " where "
	// + KEY_RELAY_NUMBER + " = ?";
	// try {
	// Cursor cursor = db.rawQuery(selectQuery, new String[] { number });
	// AppLog.Log(Const.TAG, "count" + cursor.getCount());
	// if (cursor.moveToFirst()) {
	// do {
	// relay.setNumber(cursor.getString(1));
	// relay.setPrefix(cursor.getString(2));
	// int groupId = cursor.getInt(3);
	// relay.setGroup(getGroup(groupId));
	//
	// AppLog.Log(Const.TAG, "getRelay id" + relay.getId());
	// AppLog.Log(Const.TAG, "getRelay number" + relay.getNumber());
	// AppLog.Log(Const.TAG, "getRelay prefix" + relay.getPrefix());
	// AppLog.Log(Const.TAG, "getRelay group" + relay.getGroup());
	// } while (cursor.moveToNext());
	// }
	// } catch (Exception e) {
	// // TODO: handle exception
	// e.printStackTrace();
	// }
	// return relay;
	// }

	public ArrayList<Relay> getRelay(String number, String numberWithCountryCode, ArrayList<Relay> relayList) {
		String selectQuery = "select * from " + TABLE_AURO_RELAY + " where " + KEY_RELAY_NUMBER + " = '" + number
				+ "' OR " + KEY_RELAY_NUMBER + " LIKE '%" + numberWithCountryCode + "%'";
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					Relay relay = new Relay();
					relay.setNumber(cursor.getString(1));
					relay.setName(cursor.getString(2));
					relay.setPrefix(cursor.getString(3));
					int groupId = cursor.getInt(4);
					relay.setGroup(getGroup(groupId));
					relayList.add(relay);

					AppLog.Log(Const.TAG, "getRelay id" + relay.getId());
					AppLog.Log(Const.TAG, "getRelay number" + relay.getNumber());
					AppLog.Log(Const.TAG, "getRelay name" + relay.getName());
					AppLog.Log(Const.TAG, "getRelay prefix" + relay.getPrefix());
					AppLog.Log(Const.TAG, "getRelay group" + relay.getGroup());
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return relayList;
	}

	public ArrayList<Relay> getRelayList(ArrayList<Relay> relayList) {
		String selectQuery = "select * from " + TABLE_AURO_RELAY;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					Relay relay = new Relay();
					relay.setId(cursor.getInt(0));
					relay.setNumber(cursor.getString(1));
					relay.setName(cursor.getString(2));
					relay.setPrefix(cursor.getString(3));
					relay.setGroup(getGroup(cursor.getInt(4)));
					relayList.add(relay);
				} while (cursor.moveToNext());
			}
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return relayList;
	}

	public boolean deleteRelay(int relayId) {
		return db.delete(TABLE_AURO_RELAY, KEY_RELAY_ID + "=" + relayId, null) > 0;
	}

	public long adddBroadcast(Broadcast broadcast) {
		ContentValues values = new ContentValues();
		values.put(KEY_BROADCAST_TIME, broadcast.getTime());
		values.put(KEY_GROUP_ID, broadcast.getGroup().getId());
		values.put(KEY_BROADCAST_STATUS, broadcast.getMsgStatus());
		values.put(KEY_BROADCAST_MSG, broadcast.getMessage());
		return db.insert(TABLE_BROADCAST, null, values);
	}

	public long addBroadcastPart(BroadcastPart broadcastPart) {
		ContentValues values = new ContentValues();
		values.put(KEY_BROADCAST_PART_MSG, broadcastPart.getMsg());
		values.put(KEY_BROADCAST_PART_TIME, broadcastPart.getTime());
		values.put(KEY_BROADCAST_PART_MEMBER_ID, broadcastPart.getMember().getId());
		values.put(KEY_BROADCAST_ID, broadcastPart.getBroadcastId());
		return db.insert(TABLE_BROADCAST_PART, null, values);

		// add sent delivered and retry and fetch it and testing of this is
		// remaining.
	}

	public long addSentBroadcastPart(long broadcastId) {
		ContentValues values = new ContentValues();
		values.put(KEY_BROADCAST_PART_SENT, 1);
		return db.update(TABLE_BROADCAST_PART, values, KEY_BROADCAST_PART_ID + "=" + broadcastId, null);
	}

	public long addDeliveredBroadcastPart(long broadcastId) {
		ContentValues values = new ContentValues();
		values.put(KEY_BROADCAST_PART_DELIVERED, 1);
		return db.update(TABLE_BROADCAST_PART, values, KEY_BROADCAST_PART_ID + "=" + broadcastId, null);
	}

	public boolean isRetryBroadcastPart(long broadcastId, String time) {
		int isSent = 0, isDelivered = 0, retryNo = 0;
		String selectQuery = "select " + KEY_BROADCAST_PART_SENT + "," + KEY_BROADCAST_DELIVERED + ","
				+ KEY_BROADCAST_PART_RETRY + " from " + TABLE_BROADCAST_PART + " where " + KEY_BROADCAST_PART_ID + " = "
				+ broadcastId;

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				isSent = cursor.getInt(0);
				isDelivered = cursor.getInt(1);
				retryNo = cursor.getInt(2);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		if (isSent == 0 || isDelivered == 0) {
			ContentValues values = new ContentValues();
			values.put(KEY_BROADCAST_PART_RETRY, retryNo++);
			values.put(KEY_BROADCAST_PART_TIME, time);
			db.update(TABLE_BROADCAST_PART, values, KEY_BROADCAST_PART_ID + "=" + broadcastId, null);
			return true;
		}
		return false;
	}

	public long addSent(long broadcastId) {
		AppLog.Log(Const.TAG, "broadcastId" + broadcastId);
		int sentCount = 0;
		String selectQuery = "select " + KEY_BROADCAST_SENT + " from " + TABLE_BROADCAST + " where " + KEY_BROADCAST_ID
				+ " = " + broadcastId;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				sentCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "sent" + sentCount);
				sentCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_BROADCAST_SENT, sentCount);
				return db.update(TABLE_BROADCAST, values, KEY_BROADCAST_ID + "=" + broadcastId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long addDeliverd(long broadcastId) {
		AppLog.Log(Const.TAG, "broadcastId" + broadcastId);
		int deliveredCount = 0;
		String selectQuery = "select " + KEY_BROADCAST_DELIVERED + " from " + TABLE_BROADCAST + " where "
				+ KEY_BROADCAST_ID + " = " + broadcastId;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				deliveredCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "delivered" + deliveredCount);
				deliveredCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_BROADCAST_DELIVERED, deliveredCount);
				return db.update(TABLE_BROADCAST, values, KEY_BROADCAST_ID + "=" + broadcastId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public ArrayList<BroadcastPart> getBroadcastPartList(String time,
			ArrayList<BroadcastPart> broadcastPartList/*
														 * , String time1
														 */) {
		AppLog.Log(Const.TAG, "time" + time);
		// int deliveredCount = 0;
		String selectQuery = "select * from " + TABLE_BROADCAST_PART + " where " + KEY_BROADCAST_PART_TIME
				+ " > Datetime ('" + time + "')";

		// String selectQuery1 = "select * from " + TABLE_BROADCAST
		// + " where date(" + KEY_BROADCAST_TIME + ") between Date('"
		// + time + "') and Date('" + time1 + "')";
		//
		// String selectQuery2 = "select * from " + TABLE_BROADCAST
		// + " where Datetime('" + KEY_BROADCAST_TIME
		// + "')= Datetime('22/04/2016 10:28:59')";

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					BroadcastPart broadcastPart = new BroadcastPart();
					broadcastPart.setId(cursor.getInt(0));
					broadcastPart.setMsg(cursor.getString(1));
					// member is 2
					broadcastPart.setTime(cursor.getString(3));
					broadcastPart.setBroadcastId(cursor.getInt(4));
					if (cursor.getInt(5) == 0) {
						broadcastPart.setSent(false);
					} else {
						broadcastPart.setSent(true);
					}
					if (cursor.getInt(6) == 0) {
						broadcastPart.setDelivered(false);
					} else {
						broadcastPart.setDelivered(true);
					}

					AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
					AppLog.Log(Const.TAG, "msg" + cursor.getString(1));
					AppLog.Log(Const.TAG, "time" + cursor.getString(3));
					AppLog.Log(Const.TAG, "broadcast" + cursor.getInt(4));
					AppLog.Log(Const.TAG, "sent" + cursor.getInt(5));
					AppLog.Log(Const.TAG, "delivered" + cursor.getInt(6));
					broadcastPartList.add(broadcastPart);
				} while (cursor.moveToNext());
				// deliveredCount = cursor.getInt(0);
				// AppLog.Log(Const.TAG, "delivered" + deliveredCount);
				// deliveredCount++;
				//
				// ContentValues values = new ContentValues();
				// values.put(KEY_BROADCAST_DELIVERED, deliveredCount);
				// return db.update(TABLE_BROADCAST, values, KEY_BROADCAST_ID
				// + "=" + broadcastId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return broadcastPartList;
	}

	public ArrayList<Broadcast> getBroadcastList(String time,
			ArrayList<Broadcast> broadcastList/*
												 * , String time1
												 */) {
		AppLog.Log(Const.TAG, "time" + time);
		String selectQuery;
		if (time == null) {
			selectQuery = "select * from " + TABLE_BROADCAST;
		} else {
			selectQuery = "select * from " + TABLE_BROADCAST + " where " + KEY_BROADCAST_TIME + " > Datetime ('" + time
					+ "')";
		}

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					Broadcast broadcast = new Broadcast();
					broadcast.setId(cursor.getInt(0));
					broadcast.setTime(cursor.getString(1));
					// group 2
					broadcast.setSent(cursor.getInt(3));
					broadcast.setDelivered(cursor.getInt(4));
					broadcast.setMessage(cursor.getString(6));

					AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
					AppLog.Log(Const.TAG, "time" + cursor.getString(1));
					AppLog.Log(Const.TAG, "sent" + cursor.getInt(3));
					AppLog.Log(Const.TAG, "delivered" + cursor.getInt(4));
					AppLog.Log(Const.TAG, "message" + cursor.getString(6));
					broadcastList.add(broadcast);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return broadcastList;
	}

	public long adddFeedback(Feedback feedback) {
		ContentValues values = new ContentValues();
		values.put(KEY_FEEDBACK_TIME, feedback.getTime());
		values.put(KEY_GROUP_ID, feedback.getGroup().getId());
		return db.insert(TABLE_FEEDBACK, null, values);
		
	}

	public long addSentFeedback(long feedbackId) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		int sentCount = 0;
		String selectQuery = "select " + KEY_FEEDBACK_SENT + " from " + TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID
				+ " = " + feedbackId;
		System.out.println("feedback sent  answer  is"+selectQuery);
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				sentCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "sent" + sentCount);
				sentCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_FEEDBACK_SENT, sentCount);
				return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long addDeliverdFeedback(long feedbackId) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		int deliveredCount = 0;
		String selectQuery = "select " + KEY_FEEDBACK_DELIVERED + " from " + TABLE_FEEDBACK + " where "
				+ KEY_FEEDBACK_ID + " = " + feedbackId;
		System.out.println("feedback delivered answer  is"+selectQuery);
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				deliveredCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "delivered" + deliveredCount);
				deliveredCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_FEEDBACK_DELIVERED, deliveredCount);
				return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long addReceivedFeedback(long feedbackId, int answer, boolean isReceivedIncrease) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		int receivedCount = 0, answerCount = 0;
		if (answer == Const.answerA) {
			String selectQuery = "select " + KEY_FEEDBACK_RECEIVED + "," + KEY_FEEDBACK_ANSWER_A + " from "
					+ TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
			System.out.println("feedback answer A is"+selectQuery);
			
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					AppLog.Log(Const.TAG, "received a" + receivedCount);
					System.out.println("feedback received count is"+receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_ANSWER_A, answerCount);
					return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerB) {
			String selectQuery = "select " + KEY_FEEDBACK_RECEIVED + "," + KEY_FEEDBACK_ANSWER_B + " from "
					+ TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					AppLog.Log(Const.TAG, "received b" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_ANSWER_B, answerCount);
					return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerC) {
			String selectQuery = "select " + KEY_FEEDBACK_RECEIVED + "," + KEY_FEEDBACK_ANSWER_C + ","
					+ KEY_FEEDBACK_MSG + " from " + TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					if (!cursor.getString(2).contains("C.")) {
						return -1;
					}
					AppLog.Log(Const.TAG, "received c" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_ANSWER_C, answerCount);
					return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerD) {
			String selectQuery = "select " + KEY_FEEDBACK_RECEIVED + "," + KEY_FEEDBACK_ANSWER_D + ","
					+ KEY_FEEDBACK_MSG + " from " + TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					if (!cursor.getString(2).contains("D.")) {
						return -1;
					}
					AppLog.Log(Const.TAG, "received d" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_ANSWER_D, answerCount);
					return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerE) {
			String selectQuery = "select " + KEY_FEEDBACK_RECEIVED + "," + KEY_FEEDBACK_ANSWER_E + ","
					+ KEY_FEEDBACK_MSG + " from " + TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					if (!cursor.getString(2).contains("E.")) {
						return -1;
					}

					AppLog.Log(Const.TAG, "received e" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_ANSWER_E, answerCount);
					return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public long addSentFeedbackPart(long feedbackPartId) {
		AppLog.Log(Const.TAG, "feedbackPartId" + feedbackPartId);
		int sentCount = 0;
		String selectQuery = "select " + KEY_FEEDBACK_PART_SENT + " from " + TABLE_FEEDBACK_PART + " where "
				+ KEY_FEEDBACK_PART_ID + " = " + feedbackPartId;
		try {

			// ContentValues valuesQuestion = new ContentValues();
			// valuesQuestion.put(KEY_QUESTION_SEND, 1);
			// db.update(TABLE_QUESTION, valuesQuestion, KEY_FEEDBACK_PART_ID
			// + "=" + feedbackPartId, null);

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				sentCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "sent" + sentCount);
				sentCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_FEEDBACK_PART_SENT, sentCount);
				return db.update(TABLE_FEEDBACK_PART, values, KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long addDeliverdFeedbackPart(long feedbackPartId) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackPartId);
		int deliveredCount = 0;
		String selectQuery = "select " + KEY_FEEDBACK_PART_DELIVERED + " from " + TABLE_FEEDBACK_PART + " where "
				+ KEY_FEEDBACK_PART_ID + " = " + feedbackPartId;
		try {

			// ContentValues valuesQuestion = new ContentValues();
			// valuesQuestion.put(KEY_QUESTION_DELIVERED, 1);
			// db.update(TABLE_QUESTION, valuesQuestion, KEY_FEEDBACK_PART_ID
			// + "=" + feedbackPartId, null);

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				deliveredCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "delivered" + deliveredCount);
				deliveredCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_FEEDBACK_PART_DELIVERED, deliveredCount);
				return db.update(TABLE_FEEDBACK_PART, values, KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long getFeedbackId(long feedbackPartId) {
		String selectQuery = "select " + KEY_FEEDBACK_ID + " from " + TABLE_FEEDBACK_PART + " where "
				+ KEY_FEEDBACK_PART_ID + "=" + feedbackPartId;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				return cursor.getInt(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	// public long addReceivedFeedbackPart(long feedbackPartId, int answer,
	// boolean isReceivedIncrease, long questionNo) {
	// AppLog.Log(Const.TAG, "feedbackId" + feedbackPartId);
	// int receivedCount = 0, answerCount = 0;
	// if (answer == Const.answerA) {
	// String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + ","
	// + KEY_FEEDBACK_PART_ANSWER_A + " from "
	// + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_PART_ID
	// + " = " + feedbackPartId;
	// try {
	//
	// // ContentValues valuesQuestion = new ContentValues();
	// // valuesQuestion.put(KEY_QUESTION_RECEIVED, 1);
	// // valuesQuestion.put(KEY_QUESTION_ANSWER, "a");
	// // db.update(TABLE_QUESTION, valuesQuestion,
	// // KEY_FEEDBACK_PART_ID
	// // + "=" + feedbackPartId + " AND " + KEY_QUESTION_NO
	// // + "=" + questionNo, null);
	//
	// Cursor cursor = db.rawQuery(selectQuery, null);
	// if (cursor.moveToFirst()) {
	// receivedCount = cursor.getInt(0);
	// answerCount = cursor.getInt(1);
	// AppLog.Log(Const.TAG, "received a part" + receivedCount);
	// AppLog.Log(Const.TAG, "answer a part" + answerCount);
	// receivedCount++;
	// answerCount++;
	//
	// ContentValues values = new ContentValues();
	// if (isReceivedIncrease) {
	// values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
	// }
	// values.put(KEY_FEEDBACK_PART_ANSWER_A, answerCount);
	// return db.update(TABLE_FEEDBACK_PART, values,
	// KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// } else if (answer == Const.answerB) {
	// String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + ","
	// + KEY_FEEDBACK_PART_ANSWER_B + " from "
	// + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_PART_ID
	// + " = " + feedbackPartId;
	// try {
	//
	// // ContentValues valuesQuestion = new ContentValues();
	// // valuesQuestion.put(KEY_QUESTION_RECEIVED, 1);
	// // valuesQuestion.put(KEY_QUESTION_ANSWER, "b");
	// // db.update(TABLE_QUESTION, valuesQuestion,
	// // KEY_FEEDBACK_PART_ID
	// // + "=" + feedbackPartId + " AND " + KEY_QUESTION_NO
	// // + "=" + questionNo, null);
	//
	// Cursor cursor = db.rawQuery(selectQuery, null);
	// if (cursor.moveToFirst()) {
	// receivedCount = cursor.getInt(0);
	// answerCount = cursor.getInt(1);
	// AppLog.Log(Const.TAG, "received b part" + receivedCount);
	// AppLog.Log(Const.TAG, "answer b part" + answerCount);
	// receivedCount++;
	// answerCount++;
	//
	// ContentValues values = new ContentValues();
	// if (isReceivedIncrease) {
	// values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
	// }
	// values.put(KEY_FEEDBACK_PART_ANSWER_B, answerCount);
	// return db.update(TABLE_FEEDBACK_PART, values,
	// KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// } else if (answer == Const.answerC) {
	// String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + ","
	// + KEY_FEEDBACK_PART_ANSWER_C + " from "
	// + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_PART_ID
	// + " = " + feedbackPartId;
	// try {
	//
	// // ContentValues valuesQuestion = new ContentValues();
	// // valuesQuestion.put(KEY_QUESTION_RECEIVED, 1);
	// // valuesQuestion.put(KEY_QUESTION_ANSWER, "c");
	// // db.update(TABLE_QUESTION, valuesQuestion,
	// // KEY_FEEDBACK_PART_ID
	// // + "=" + feedbackPartId + " AND " + KEY_QUESTION_NO
	// // + "=" + questionNo, null);
	//
	// Cursor cursor = db.rawQuery(selectQuery, null);
	// if (cursor.moveToFirst()) {
	// receivedCount = cursor.getInt(0);
	// answerCount = cursor.getInt(1);
	// AppLog.Log(Const.TAG, "received c part" + receivedCount);
	// AppLog.Log(Const.TAG, "answer c part" + answerCount);
	// receivedCount++;
	// answerCount++;
	//
	// ContentValues values = new ContentValues();
	// if (isReceivedIncrease) {
	// values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
	// }
	// values.put(KEY_FEEDBACK_PART_ANSWER_C, answerCount);
	// return db.update(TABLE_FEEDBACK_PART, values,
	// KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// // } else if (answer == Const.answerD) {
	// // String selectQuery = "select " + KEY_FEEDBACK_DELIVERED + ","
	// // + KEY_FEEDBACK_ANSWER_D + " from " + TABLE_FEEDBACK
	// // + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
	// // try {
	// // Cursor cursor = db.rawQuery(selectQuery, null);
	// // if (cursor.moveToFirst()) {
	// // receivedCount = cursor.getInt(0);
	// // answerCount = cursor.getInt(1);
	// // AppLog.Log(Const.TAG, "delivered" + receivedCount);
	// // receivedCount++;
	// // answerCount++;
	// //
	// // ContentValues values = new ContentValues();
	// // values.put(KEY_FEEDBACK_DELIVERED, receivedCount);
	// // values.put(KEY_FEEDBACK_ANSWER_D, answerCount);
	// // return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID
	// // + "=" + feedbackId, null);
	// // }
	// // } catch (Exception e) {
	// // e.printStackTrace();
	// // }
	// }
	// return -1;
	// }

	public long addReceivedFeedbackPart(long feedbackId, int answer, boolean isReceivedIncrease,
			/* long questionNo, */String number, String numberWithoutCountryCode) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		int receivedCount = 0, answerCount = 0; 
		int feedbackCount=0;
		if (answer == Const.answerA) {
			String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + "," + KEY_FEEDBACK_PART_ANSWER_A + " from "
					+ TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_ID + " = " + feedbackId+"  AND ("
					+ KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '" + number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER
					+ " LIKE '%" + numberWithoutCountryCode + "%')" ;
			AppLog.Log(Const.TAG, "feedback query" + selectQuery);
			try {

				Cursor cursor = db.rawQuery(selectQuery, null);
				AppLog.Log("addReceivedFeedbackPart", "addReceivedFeedbackPart:" + cursor.getCount());
				AppLog.Log("Cursor Object", DatabaseUtils.dumpCursorToString(cursor));
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
				//	feedbackCount= cursor.getInt(2);
					System.out.println("the counts are following "+receivedCount+"-"+answerCount+"-"+feedbackCount);
					
					if (receivedCount > 0) {
						return -1;
					}

					AppLog.Log("received a part", "received a part" + receivedCount);
					AppLog.Log(Const.TAG, "answer a part" + answerCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_PART_ANSWER_A, answerCount);
					return db.update(TABLE_FEEDBACK_PART, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '"
									+ number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER +" LIKE '%" + numberWithoutCountryCode + "%')",
							null);
				}
			} catch (Exception e) {
				AppLog.Log(Const.TAG, "error in db" +e.toString() );
				e.printStackTrace();
			}
		} else if (answer == Const.answerB) {
			String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + "," + KEY_FEEDBACK_PART_ANSWER_B + " from "
					+ TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_ID + " = " + feedbackId + " AND ("
					+ KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '" + number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER
					+ " LIKE '%" + numberWithoutCountryCode + "%')";
			
			try {
				AppLog.Log(Const.TAG, "feedback query" + selectQuery);
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);

					if (receivedCount > 0) {
						return -1;
					}

					AppLog.Log(Const.TAG, "received b part" + receivedCount);
					AppLog.Log(Const.TAG, "answer b part" + answerCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_PART_ANSWER_B, answerCount);
					return db.update(TABLE_FEEDBACK_PART, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '"
									+ number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER + " LIKE '%" + numberWithoutCountryCode + "%')",
							null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerC) {
			String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + "," + KEY_FEEDBACK_PART_ANSWER_C + ","
					+ KEY_FEEDBACK_PART_MSG + " from " + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_ID + " = "
					+ feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '" + number + "' OR "
					+ KEY_FEEDBACK_PART_MEMBER_NUMBER +" LIKE '%" + numberWithoutCountryCode + "%')";
			try {

				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					if ((!cursor.getString(2).contains("C.")) || receivedCount > 0) {
						return -1;
					}

					AppLog.Log(Const.TAG, "received c part" + receivedCount);
					AppLog.Log(Const.TAG, "answer c part" + answerCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_PART_ANSWER_C, answerCount);
					return db.update(TABLE_FEEDBACK_PART, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '"
									+ number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER +" LIKE '%" + numberWithoutCountryCode + "%')",
							null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerD) {
			String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + "," + KEY_FEEDBACK_PART_ANSWER_D + ","
					+ KEY_FEEDBACK_PART_MSG + " from " + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_ID + " = "
					+ feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '" + number + "' OR "
					+ KEY_FEEDBACK_PART_MEMBER_NUMBER +" LIKE '%" + numberWithoutCountryCode + "%')";
			try {

				Cursor cursor = db.rawQuery(selectQuery, null);
				AppLog.Log("addReceivedFeedbackPart", "addReceivedFeedbackPart:" + cursor.getCount());
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					if ((!cursor.getString(2).contains("D.")) || receivedCount > 0) {
						return -1;
					}

					AppLog.Log("received d part", "received d part" + receivedCount);
					AppLog.Log(Const.TAG, "answer d part" + answerCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_PART_ANSWER_D, answerCount);
					return db.update(TABLE_FEEDBACK_PART, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '"
									+ number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER +" LIKE '%" + numberWithoutCountryCode + "%')",
							null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerE) {
			String selectQuery = "select " + KEY_FEEDBACK_PART_RECEIVED + "," + KEY_FEEDBACK_PART_ANSWER_E + ","
					+ KEY_FEEDBACK_PART_MSG + " from " + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_ID + " = "
					+ feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '" + number + "' OR "
					+ KEY_FEEDBACK_PART_MEMBER_NUMBER +" LIKE '%" + numberWithoutCountryCode + "%')";
			try {

				Cursor cursor = db.rawQuery(selectQuery, null);
				AppLog.Log("addReceivedFeedbackPart", "addReceivedFeedbackPart:" + cursor.getCount());
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					if ((!cursor.getString(2).contains("E.")) || receivedCount > 0) {
						return -1;
					}

					AppLog.Log("received e part", "received e part" + receivedCount);
					AppLog.Log(Const.TAG, "answer e part" + answerCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_FEEDBACK_PART_RECEIVED, receivedCount);
					}
					values.put(KEY_FEEDBACK_PART_ANSWER_E, answerCount);
					return db.update(TABLE_FEEDBACK_PART, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND (" + KEY_FEEDBACK_PART_MEMBER_NUMBER + " = '"
									+ number + "' OR " + KEY_FEEDBACK_PART_MEMBER_NUMBER + " LIKE '%" + numberWithoutCountryCode + "%')",
							null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	public long addFeedbackPart(FeedbackPart feedbackPart) {
		ContentValues values = new ContentValues();
		// values.put(KEY_FEEDBACK_PART_MSG, feedback.getMsg());
		values.put(KEY_FEEDBACK_PART_TIME, feedbackPart.getTime());
		values.put(KEY_FEEDBACK_PART_MEMBER_ID, feedbackPart.getMember().getId());
		values.put(KEY_FEEDBACK_ID, feedbackPart.getFeedbackId());
		values.put(KEY_FEEDBACK_PART_STATUS, feedbackPart.getMsgStatus());
		values.put(KEY_FEEDBACK_PART_RECEIVED, 0);
		values.put(KEY_FEEDBACK_PART_MEMBER_NUMBER, feedbackPart.getMember().getPhone());
		
		AppLog.Log(Const.TAG, "addFeedbackpart: Phone " + feedbackPart.getMember().getPhone());
		return db.insert(TABLE_FEEDBACK_PART, null, values);
	}

	public long addMessageInFeedbackPart(String msg, long feedbackPartId) {
		ContentValues values = new ContentValues();
		values.put(KEY_FEEDBACK_PART_MSG, msg);
		return db.update(TABLE_FEEDBACK_PART, values, KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
	}

	public long addMessageInFeedback(String msg, long feedbackId) {
		ContentValues values = new ContentValues();
		values.put(KEY_FEEDBACK_MSG, msg);
		return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
	}

	public long addTimeInFeedback(String time, long feedbackId) {
		ContentValues values = new ContentValues();
		values.put(KEY_FEEDBACK_TIME, time);
		return db.update(TABLE_FEEDBACK, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
	}

	public long addTimeInFeedbackPart(String time, long feedbackId) {
		ContentValues values = new ContentValues();
		values.put(KEY_FEEDBACK_PART_TIME, time);
		return db.update(TABLE_FEEDBACK_PART, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
	}

	public ArrayList<FeedbackPart> getFeedbackPartList(String time, ArrayList<FeedbackPart> feedbackPartList) {
		AppLog.Log(Const.TAG, "time" + time);
		String selectQuery = "select * from " + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_PART_TIME
				+ " > Datetime ('" + time + "')";

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					FeedbackPart feedbackPart = new FeedbackPart();
					feedbackPart.setId(cursor.getInt(0));
					feedbackPart.setMsg(cursor.getString(1));
					// member is 2
					feedbackPart.setTime(cursor.getString(3));
					feedbackPart.setFeedbackId(cursor.getInt(4));
					if (cursor.getInt(5) == 0) {
						feedbackPart.setSent(false);
					} else {
						feedbackPart.setSent(true);
					}
					if (cursor.getInt(6) == 0) {
						feedbackPart.setDelivered(false);
					} else {
						feedbackPart.setDelivered(true);
					}
					if (cursor.getInt(7) == 0) {
						feedbackPart.setReceived(false);
					} else {
						feedbackPart.setReceived(true);
					}

					feedbackPart.setAnswerA(cursor.getInt(8));
					feedbackPart.setAnswerB(cursor.getInt(9));
					feedbackPart.setAnswerC(cursor.getInt(10));
					// feedbackPart.setAnswerD(cursor.getInt(11));
					// feedbackPart.setAnswerE(cursor.getInt(12));

				 AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
				 AppLog.Log(Const.TAG, "msg" + cursor.getString(1));
				 AppLog.Log(Const.TAG, "time" + cursor.getString(3));
				 AppLog.Log(Const.TAG, "feedback" + cursor.getInt(4));
				 AppLog.Log(Const.TAG, "sent" + cursor.getInt(5));
					 AppLog.Log(Const.TAG, "delivered" + cursor.getInt(6));
				 AppLog.Log(Const.TAG, "received" + cursor.getInt(7));
				 AppLog.Log(Const.TAG, "A" + cursor.getInt(8));
				 AppLog.Log(Const.TAG, "C" + cursor.getInt(10));

					feedbackPartList.add(feedbackPart);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedbackPartList;
	}

	public ArrayList<Feedback> getFeedbackList(String time, ArrayList<Feedback> feedbackList) {
		AppLog.Log(Const.TAG, "time" + time);
		String selectQuery;
		if (time == null) {
			selectQuery = "select * from " + TABLE_FEEDBACK;
		} else {
			selectQuery = "select * from " + TABLE_FEEDBACK + " where " + KEY_FEEDBACK_TIME + " > Datetime ('" + time
					+ "')";
		}

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					Feedback feedback = new Feedback();
					feedback.setId(cursor.getInt(0));
					// group is 2
					feedback.setTime(cursor.getString(1));
					feedback.setSent(cursor.getInt(3));
					feedback.setDelivered(cursor.getInt(4));
					feedback.setReceived(cursor.getInt(5));
					feedback.setMsgStatus(cursor.getInt(6));
					feedback.setAnswerA(cursor.getInt(7));
					feedback.setAnswerB(cursor.getInt(8));
					feedback.setAnswerC(cursor.getInt(9));
					feedback.setAnswerD(cursor.getInt(10));
					feedback.setAnswerE(cursor.getInt(11));
					feedback.setMessgae(cursor.getString(12));

					ArrayList<FeedbackPart> feedbackPartList = new ArrayList<FeedbackPart>();
					feedback.setFeedbackPartList(getFeedbackPartList(feedbackPartList, feedback.getId()));

				 AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
				 AppLog.Log(Const.TAG, "time" + cursor.getString(1));
				 AppLog.Log(Const.TAG, "sent" + cursor.getInt(3));
				 AppLog.Log(Const.TAG, "delivered" + cursor.getInt(4));
				 AppLog.Log(Const.TAG, "received" + cursor.getInt(5));
				 AppLog.Log(Const.TAG, "status" + cursor.getInt(6));
					 AppLog.Log(Const.TAG, "A" + cursor.getInt(7));
				 AppLog.Log(Const.TAG, "B" + cursor.getInt(8));
				 AppLog.Log(Const.TAG, "C" + cursor.getInt(9));
			 AppLog.Log(Const.TAG, "message" + cursor.getString(10));

					feedbackList.add(feedback);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedbackList;
	}

	public ArrayList<FeedbackPart> getFeedbackPartList(ArrayList<FeedbackPart> feedbackPartList, long feedbackId) {
		String selectQuery = "select * from " + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_ID + "=" + feedbackId;

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					FeedbackPart feedbackPart = new FeedbackPart();
					feedbackPart.setId(cursor.getInt(0));
					feedbackPart.setMsg(cursor.getString(1));
					// member is 2
					feedbackPart.setMember(getMember(cursor.getInt(2)));
					feedbackPart.setTime(cursor.getString(3));
					feedbackPart.setFeedbackId(cursor.getInt(4));
					if (cursor.getInt(5) == 0) {
						feedbackPart.setSent(false);
					} else {
						feedbackPart.setSent(true);
					}
					if (cursor.getInt(6) == 0) {
						feedbackPart.setDelivered(false);
					} else {
						feedbackPart.setDelivered(true);
					}

					feedbackPart.setMsgStatus(cursor.getInt(7));
					feedbackPart.setAnswerA(cursor.getInt(8));
					feedbackPart.setAnswerB(cursor.getInt(9));
					feedbackPart.setAnswerC(cursor.getInt(10));
					feedbackPart.setAnswerD(cursor.getInt(11));
					feedbackPart.setAnswerE(cursor.getInt(12));
					// feedbackPart.setAnswerD(cursor.getInt(11));
					// feedbackPart.setAnswerE(cursor.getInt(12));
					feedbackPart.setRetry(cursor.getInt(13));
					if (cursor.getInt(14) == 0) {
						feedbackPart.setReceived(false);
					} else {
						feedbackPart.setReceived(true);
					}

				 AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
					 AppLog.Log(Const.TAG, "msg" + cursor.getString(1));
					 AppLog.Log(Const.TAG, "time" + cursor.getString(3));
				 AppLog.Log(Const.TAG, "feedback" + cursor.getInt(4));
				 AppLog.Log(Const.TAG, "sent" + cursor.getInt(5));
				 AppLog.Log(Const.TAG, "delivered" + cursor.getInt(6));
					 AppLog.Log(Const.TAG, "status" + cursor.getInt(7));
				 AppLog.Log(Const.TAG, "A" + cursor.getInt(8));
			 AppLog.Log(Const.TAG, "B" + cursor.getInt(9));
				 AppLog.Log(Const.TAG, "C" + cursor.getInt(10));
			 AppLog.Log(Const.TAG, "retry" + cursor.getInt(11));
				 AppLog.Log(Const.TAG, "received" + cursor.getInt(12));

					feedbackPartList.add(feedbackPart);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedbackPartList;
	}

	public Feedback getFeedback(long feedbackId) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		String selectQuery = "select * from " + TABLE_FEEDBACK + " where " + KEY_FEEDBACK_ID + " = " + feedbackId;
		Feedback feedback = null;
		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				feedback = new Feedback();
				feedback.setId(cursor.getInt(0));
				// group is 2
				feedback.setTime(cursor.getString(1));
				feedback.setGroup(getGroup(cursor.getInt(2)));
				feedback.setSent(cursor.getInt(3));
				feedback.setDelivered(cursor.getInt(4));
				feedback.setReceived(cursor.getInt(5));
				feedback.setMsgStatus(cursor.getInt(6));
				feedback.setAnswerA(cursor.getInt(7));
				feedback.setAnswerB(cursor.getInt(8));
				feedback.setAnswerC(cursor.getInt(9));
				feedback.setAnswerD(cursor.getInt(10));
				feedback.setAnswerE(cursor.getInt(11));
				feedback.setMessgae(cursor.getString(12));

				ArrayList<FeedbackPart> feedbackPartList = new ArrayList<FeedbackPart>();
				feedback.setFeedbackPartList(getFeedbackPartList(feedbackPartList, cursor.getInt(0)));

			 AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
			 AppLog.Log(Const.TAG, "group" + feedback.getGroup());
			 AppLog.Log(Const.TAG, "sent" + cursor.getInt(3));
			 AppLog.Log(Const.TAG, "delivered" + cursor.getInt(4));
			 AppLog.Log(Const.TAG, "received" + cursor.getInt(5));
				 AppLog.Log(Const.TAG, "status" + cursor.getInt(6));
			 AppLog.Log(Const.TAG, "A" + cursor.getInt(7));
				 AppLog.Log(Const.TAG, "B" + cursor.getInt(8));
			 AppLog.Log(Const.TAG, "C" + cursor.getInt(9));
			 AppLog.Log(Const.TAG, "message" + cursor.getString(10));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedback;
	}

	public ArrayList<FeedbackPart> getFeedbackPartList(ArrayList<FeedbackPart> feedbackPartList) {
		long received = 0;
		String selectQuery = "select * from " + TABLE_FEEDBACK_PART + " where " + KEY_FEEDBACK_PART_RECEIVED + " = 0"
		/*
		 * + "AND " + KEY_FEEDBACK_PART_STATUS + "!=" + Const . NORMAL
		 */;

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			AppLog.Log(Const.TAG, "count" + cursor.getCount());
			if (cursor.moveToFirst()) {
				do {
					FeedbackPart feedbackPart = new FeedbackPart();
					feedbackPart.setId(cursor.getInt(0));
					feedbackPart.setMsg(cursor.getString(1));
					// member is 2
					feedbackPart.setMember(getMember(cursor.getInt(2)));
					feedbackPart.setTime(cursor.getString(3));
					feedbackPart.setFeedbackId(cursor.getInt(4));
					if (cursor.getInt(5) == 0) {
						feedbackPart.setSent(false);
					} else {
						feedbackPart.setSent(true);
					}
					if (cursor.getInt(6) == 0) {
						feedbackPart.setDelivered(false);
					} else {
						feedbackPart.setDelivered(true);
					}

					feedbackPart.setMsgStatus(cursor.getInt(7));
					feedbackPart.setAnswerA(cursor.getInt(8));
					feedbackPart.setAnswerB(cursor.getInt(9));
					feedbackPart.setAnswerC(cursor.getInt(10));
					// feedbackPart.setAnswerD(cursor.getInt(11));
					// feedbackPart.setAnswerE(cursor.getInt(12));
					feedbackPart.setRetry(cursor.getInt(13));
					if (cursor.getInt(14) == 0) {
						feedbackPart.setReceived(false);
					} else {
						feedbackPart.setReceived(true);
					}

				AppLog.Log(Const.TAG, "id" + cursor.getInt(0));
					AppLog.Log(Const.TAG, "msg" + cursor.getString(1));
					 AppLog.Log(Const.TAG, "time" + cursor.getString(3));
					 AppLog.Log(Const.TAG, "feedback" + cursor.getInt(4));
				 AppLog.Log(Const.TAG, "sent" + cursor.getInt(5));
				 AppLog.Log(Const.TAG, "delivered" + cursor.getInt(6));
				 AppLog.Log(Const.TAG, "status" + cursor.getInt(7));
					 AppLog.Log(Const.TAG, "A" + cursor.getInt(8));
					 AppLog.Log(Const.TAG, "B" + cursor.getInt(9));
					 AppLog.Log(Const.TAG, "C" + cursor.getInt(10));
					AppLog.Log(Const.TAG, "retry" + cursor.getInt(13));
					 AppLog.Log(Const.TAG, "received" + cursor.getInt(12));

					feedbackPartList.add(feedbackPart);
				} while (cursor.moveToNext());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return feedbackPartList;
	}

	public int retryFeedbackPart(long feedbackPartId, String time) {
		int retryNo = 0;
		String selectQuery = "select " + KEY_FEEDBACK_PART_RETRY + " from " + TABLE_FEEDBACK_PART + " where "
				+ KEY_FEEDBACK_PART_ID + " = " + feedbackPartId;

		try {
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				retryNo = cursor.getInt(0);
				retryNo++;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		AppLog.Log(Const.TAG, "retry" + retryNo);
		ContentValues values = new ContentValues();
		values.put(KEY_FEEDBACK_PART_RETRY, retryNo);
		values.put(KEY_FEEDBACK_PART_TIME, time);
		return db.update(TABLE_FEEDBACK_PART, values, KEY_FEEDBACK_PART_ID + "=" + feedbackPartId, null);
		// retryNo;
	}

	public long addQuestion(Question question) {
		ContentValues values = new ContentValues();
		values.put(KEY_QUESTION_MSG, question.getMsg());
		values.put(KEY_QUESTION_NO, question.getQuestionNo());
		values.put(KEY_FEEDBACK_ID, question.getFeedbackId());
		values.put(KEY_QUESTION_TIME, question.getTime());
		return db.insert(TABLE_QUESTION, null, values);
	}

	// public long addAnswer(String answer, long feedbackPartId, long
	// questionNo) {
	// ContentValues values = new ContentValues();
	// values.put(KEY_QUESTION_ANSWER, answer);
	// return db.update(TABLE_QUESTION, values,
	// KEY_FEEDBACK_PART_ID + "=" + feedbackPartId + " AND "
	// + KEY_QUESTION_NO + "=" + questionNo, null);
	// }

	public ArrayList<Question> getQuestionList(ArrayList<Feedback> feedbackList, ArrayList<Question> questionList) {

		Question question = null;
		for (int i = 0; i < feedbackList.size(); i++) {
			AppLog.Log(Const.TAG, "Id:" + feedbackList.get(i).getId());
			String selectQuery = "select * from " + TABLE_QUESTION + " where " + KEY_FEEDBACK_ID + " = "
					+ feedbackList.get(i).getId();
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				AppLog.Log(Const.TAG, "count" + cursor.getCount());

				if (cursor.moveToFirst()) {
					do {
						question = new Question();
						question.setId(cursor.getInt(0));
						question.setMsg(cursor.getString(1));
						question.setQuestionNo(cursor.getInt(2));
						question.setFeedbackId(cursor.getInt(3));
						question.setTime(cursor.getString(4));
						question.setSend(cursor.getInt(5));
						question.setDelivered(cursor.getInt(6));
						question.setReceived(cursor.getInt(7));
						question.setAnswerA(cursor.getInt(8));
						question.setAnswerB(cursor.getInt(9));
						question.setAnswerC(cursor.getInt(10));
						questionList.add(question);

						AppLog.Log(Const.TAG, "msg:" + cursor.getString(1));
						AppLog.Log(Const.TAG, "questionNo:" + cursor.getInt(2));
						AppLog.Log(Const.TAG, "feedbackPartId" + cursor.getInt(3));
						AppLog.Log(Const.TAG, "time" + cursor.getString(4));
						AppLog.Log(Const.TAG, "send" + cursor.getInt(5));
						AppLog.Log(Const.TAG, "delivered" + cursor.getInt(6));
						AppLog.Log(Const.TAG, "received" + cursor.getInt(7));
						AppLog.Log(Const.TAG, "answer a" + cursor.getString(8));
						AppLog.Log(Const.TAG, "answer b" + cursor.getString(9));
						AppLog.Log(Const.TAG, "answer c" + cursor.getString(10));

					} while (cursor.moveToNext());
				}
			} catch (Exception e) {

			}
		}
		return questionList;
	}

	public long addSentQuestion(long feedbackId) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		int sentCount = 0;
		String selectQuery = "select " + KEY_QUESTION_SEND + " from " + TABLE_QUESTION + " where " + KEY_FEEDBACK_ID
				+ " = " + feedbackId;
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				sentCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "sent" + sentCount);
				sentCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_QUESTION_SEND, sentCount);
				return db.update(TABLE_QUESTION, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long addDeliverdQuestion(long feedbackId) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		int deliveredCount = 0;
		String selectQuery = "select " + KEY_QUESTION_DELIVERED + " from " + TABLE_QUESTION + " where "
				+ KEY_FEEDBACK_ID + " = " + feedbackId;
		try {

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				deliveredCount = cursor.getInt(0);
				AppLog.Log(Const.TAG, "delivered" + deliveredCount);
				deliveredCount++;

				ContentValues values = new ContentValues();
				values.put(KEY_QUESTION_DELIVERED, deliveredCount);
				return db.update(TABLE_QUESTION, values, KEY_FEEDBACK_ID + "=" + feedbackId, null);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;
	}

	public long addReceivedQuestion(long feedbackId, long qustionNo, int answer, boolean isReceivedIncrease) {
		AppLog.Log(Const.TAG, "feedbackId" + feedbackId);
		AppLog.Log(Const.TAG, "qustionNo" + qustionNo);
		int receivedCount = 0, answerCount = 0;
		if (answer == Const.answerA) {
			String selectQuery = "select " + KEY_QUESTION_RECEIVED + "," + KEY_QUESTION_ANSWER_A + " from "
					+ TABLE_QUESTION + " where " + KEY_FEEDBACK_ID + " = " + feedbackId + " AND " + KEY_QUESTION_NO
					+ "=" + qustionNo;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					AppLog.Log(Const.TAG, "received a" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_QUESTION_RECEIVED, receivedCount);
					}
					values.put(KEY_QUESTION_ANSWER_A, answerCount);
					return db.update(TABLE_QUESTION, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND " + KEY_QUESTION_NO + "=" + qustionNo, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerB) {
			String selectQuery = "select " + KEY_QUESTION_RECEIVED + "," + KEY_QUESTION_ANSWER_B + " from "
					+ TABLE_QUESTION + " where " + KEY_FEEDBACK_ID + " = " + feedbackId + " AND " + KEY_QUESTION_NO
					+ "=" + qustionNo;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					AppLog.Log(Const.TAG, "received b" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_QUESTION_RECEIVED, receivedCount);
					}
					values.put(KEY_QUESTION_ANSWER_B, answerCount);
					return db.update(TABLE_QUESTION, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND " + KEY_QUESTION_NO + "=" + qustionNo, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (answer == Const.answerC) {
			String selectQuery = "select " + KEY_QUESTION_RECEIVED + "," + KEY_QUESTION_ANSWER_C + " from "
					+ TABLE_QUESTION + " where " + KEY_FEEDBACK_ID + " = " + feedbackId + " AND " + KEY_QUESTION_NO
					+ "=" + qustionNo;
			try {
				Cursor cursor = db.rawQuery(selectQuery, null);
				if (cursor.moveToFirst()) {
					receivedCount = cursor.getInt(0);
					answerCount = cursor.getInt(1);
					AppLog.Log(Const.TAG, "received c" + receivedCount);
					receivedCount++;
					answerCount++;

					ContentValues values = new ContentValues();
					if (isReceivedIncrease) {
						values.put(KEY_QUESTION_RECEIVED, receivedCount);
					}
					values.put(KEY_QUESTION_ANSWER_C, answerCount);
					return db.update(TABLE_QUESTION, values,
							KEY_FEEDBACK_ID + "=" + feedbackId + " AND " + KEY_QUESTION_NO + "=" + qustionNo, null);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return -1;
	}
}
