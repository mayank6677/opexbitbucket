package com.example.opex_new.db;

import java.util.ArrayList;

import com.example.opex_new.R;
import com.example.opex_new.Utils.AndyUtils;
import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.Utils.Const;
import com.example.opex_new.Utils.PreferenceHelper;
import com.example.opex_new.model.Broadcast;
import com.example.opex_new.model.BroadcastPart;
import com.example.opex_new.model.Feedback;
import com.example.opex_new.model.FeedbackPart;
import com.example.opex_new.model.Group;
import com.example.opex_new.model.Member;
import com.example.opex_new.model.Question;
import com.example.opex_new.model.Relay;
import com.example.opex_new.model.Templete;
import com.example.opex_new.model.User;

import android.content.Context;
import android.text.TextUtils;

public class DBHelper {

	// private DBAdapter dbAdapter;
	private DataBaseAdapter dataBaseAdapter;
	private Context context;

	public DBHelper(Context context) {
		// dbAdapter = new DBAdapter(context);
		dataBaseAdapter = new DataBaseAdapter(context);
		this.context = context;
	}

	// public void addLocation(LatLng latLng) {
	// dbAdapter.open();
	//
	// dbAdapter.addLocation(latLng);
	//
	// dbAdapter.close();
	// }

	// public ArrayList<LatLng> getLocations() {
	// dbAdapter.open();
	// ArrayList<LatLng> points = dbAdapter.getLocations();
	// dbAdapter.close();
	// return points;
	// }

	// public int deleteAllLocations() {
	// int count = 0;
	// dbAdapter.open();
	// count = dbAdapter.deleteAllLocations();
	// dbAdapter.close();
	// return count;
	//
	// }

	// public boolean isLocationsExists() {
	// boolean isExists = false;
	// dbAdapter.open();
	// isExists = dbAdapter.isLocationsExists();
	// dbAdapter.close();
	// return isExists;
	// }

	// public long createUser(User user) {
	// long count = 0;
	// dbAdapter.open();
	// count = dbAdapter.createUser(user);
	// dbAdapter.close();
	// return count;
	//
	// }
	//
	// public User getUser() {
	// User user = null;
	// dbAdapter.open();
	// user = dbAdapter.getUser();
	// dbAdapter.close();
	// return user;
	// }
	//
	// public int deleteUser() {
	// int count = 0;
	// dbAdapter.open();
	// count = dbAdapter.deleteUser();
	// dbAdapter.close();
	// return count;
	// }

	public User registerUser(User user) {
		dataBaseAdapter.open();
		if (dataBaseAdapter.getNoOfUser() <= 0) {
			long count = dataBaseAdapter.registerUser(user);
			AppLog.Log("", "registerUser" + count);
			user.setUserId(count);
		} else {
			AndyUtils.showToast(context.getString(R.string.toast_already_user), context);
			user.setUserId(0);
		}
		dataBaseAdapter.close();

		return user;
	}

	public void addPassword(long userId, String password) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addPassword(userId, password);
		AppLog.Log("", "addPassword:" + count);
		dataBaseAdapter.close();
	}

	public int getOtp(long userId) {
		dataBaseAdapter.open();
		int otp = dataBaseAdapter.getOTP(userId);
		dataBaseAdapter.close();
		return otp;
	}

	public String getPassword(long userId) {
		dataBaseAdapter.open();
		String otp = dataBaseAdapter.getPassword(userId);
		dataBaseAdapter.close();
		return otp;
	}

	public User login(String userName, String password) {
		dataBaseAdapter.open();
		User user = dataBaseAdapter.checkUser(userName);
		dataBaseAdapter.close();
		AppLog.Log("login", "password" + password);

		if (user == null) {
			AndyUtils.showToast(context.getString(R.string.toast_not_registered_user), context);
		} else if (TextUtils.isEmpty(user.getPassword())) {
			AndyUtils.showToast(context.getString(R.string.toast_incorrect_password), context);
			user = null;
		} else if (!user.getPassword().toString().equals(password)) {
			AndyUtils.showToast(context.getString(R.string.toast_incorrect_password), context);
			user = null;
		}

		return user;
	}

	public long updateOtp(String mobileNo, int otp) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.updateOtp(mobileNo, otp);
		dataBaseAdapter.close();
		AppLog.Log("updateOtp", "updateOtp" + count);
		return count;
	}

	public long updateProfile(User user) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addProfile(user);
		if (count >= 0) {
			new PreferenceHelper(context).putUserName(user.getName());
			new PreferenceHelper(context).putUserPhoto(user.getPhoto());
		}
		dataBaseAdapter.close();
		return count;
	}

	public User getProfile(User user) {
		dataBaseAdapter.open();
		user = dataBaseAdapter.getProfile(user);
		dataBaseAdapter.close();
		return user;
	}

	public long createGroup(Group group) {
		long count = 0;
		dataBaseAdapter.open();

		if (dataBaseAdapter.getGroupFromName(group.getName()) == 0) {
			count = dataBaseAdapter.createGroup(group);
			AppLog.Log("", "createGroup" + count);
			for (int i = 0; i < group.getMemberList().size(); i++) {

				long count1 = dataBaseAdapter.addMember(group.getMemberList().get(i), (int) count);
				AppLog.Log("getMemberList", "getMemberList" + count1);
			}
		}
		dataBaseAdapter.close();
		return count;
	}

	public void deleteGroup(int groupId) {
		dataBaseAdapter.open();
		dataBaseAdapter.deleteGroup(groupId);
		dataBaseAdapter.close();
	}

	public void addMember(Member member, int groupId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addMember(member, groupId);
		AppLog.Log("count", "addMember" + count);
		dataBaseAdapter.close();
	}

	public ArrayList<Member> getMembers(ArrayList<Member> memberList, int groupId) {
		dataBaseAdapter.open();
		dataBaseAdapter.getMemberList(memberList, groupId);
		dataBaseAdapter.close();
		return memberList;
	}

	public void deleteMember(int memberId) {
		dataBaseAdapter.open();
		dataBaseAdapter.deleteMember(memberId);
		dataBaseAdapter.close();
	}

	public ArrayList<Group> getGroups(ArrayList<Group> groupList) {
		dataBaseAdapter.open();
		ArrayList<Group> groupsWithoutMember = new ArrayList<Group>();
		dataBaseAdapter.getGroupList(groupsWithoutMember);

		for (int i = 0; i < groupsWithoutMember.size(); i++) {
			Group group = groupsWithoutMember.get(i);
			ArrayList<Member> memberList = new ArrayList<Member>();
			dataBaseAdapter.getMemberList(memberList, group.getId());
			group.setMemberList(memberList);
			groupList.add(group);
		}
		dataBaseAdapter.close();
		return groupList;
	}

	public void addTemplete(Templete templete) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addTemplete(templete);
		AppLog.Log("count", "addTemplete" + count);
		dataBaseAdapter.close();
	}

	public ArrayList<Templete> getTempletes(ArrayList<Templete> templeteList) {
		dataBaseAdapter.open();
		dataBaseAdapter.getTempleteList(templeteList);
		dataBaseAdapter.close();
		return templeteList;
	}

	public void updateTempleteName(Templete templete) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.updateTemplete(templete);
		AppLog.Log("count", "updateTemplete" + count);
		dataBaseAdapter.close();
	}

	public void deleteTemplete(int templeteId) {
		dataBaseAdapter.open();
		dataBaseAdapter.deleteTemplete(templeteId);
		dataBaseAdapter.close();
	}
	public void deletefeedback(int feedbackId) {
		dataBaseAdapter.open();
		dataBaseAdapter.deleteFeedback(feedbackId);
		dataBaseAdapter.close();
	}


	public void addAutoRelay(Relay relay) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addAutoRelay(relay);
		AppLog.Log("count", "addAutoRelay" + count);
		dataBaseAdapter.close();
	}

	public ArrayList<Relay> getRelayList(ArrayList<Relay> relayList) {
		dataBaseAdapter.open();
		dataBaseAdapter.getRelayList(relayList);
		dataBaseAdapter.close();
		return relayList;
	}

	public void deleteRelay(int relayId) {
		dataBaseAdapter.open();
		dataBaseAdapter.deleteRelay(relayId);
		dataBaseAdapter.close();
	}

	// public Relay getRelay(String number, Relay relay) {
	// dataBaseAdapter.open();
	// relay = dataBaseAdapter.getRelay(number, relay);
	// if (relay.getGroup() != null) {
	// Group group = relay.getGroup();
	// ArrayList<Member> memberList = new ArrayList<Member>();
	// dataBaseAdapter.getMemberList(memberList, group.getId());
	// group.setMemberList(memberList);
	// relay.setGroup(group);
	// }
	// dataBaseAdapter.close();
	// return relay;
	// }

	public ArrayList<Relay> getRelay(String number, String numberWithCountryCode, ArrayList<Relay> relayList) {
		dataBaseAdapter.open();
		dataBaseAdapter.getRelay(number, numberWithCountryCode, relayList);
		for (int i = 0; i < relayList.size(); i++) {
			if (relayList.get(i).getGroup() != null) {
				Group group = relayList.get(i).getGroup();
				ArrayList<Member> memberList = new ArrayList<Member>();
				dataBaseAdapter.getMemberList(memberList, group.getId());
				group.setMemberList(memberList);
				relayList.get(i).setGroup(group);
			}
		}

		dataBaseAdapter.close();
		return relayList;
	}

	public long[] addBroadcast(Broadcast broadcast, long ids[]) {
		dataBaseAdapter.open();

		ids[0] = dataBaseAdapter.adddBroadcast(broadcast);
		for (int i = 0; i < broadcast.getBroadcastPartList().size(); i++) {
			broadcast.getBroadcastPartList().get(i).setBroadcastId(ids[0]);
			ids[i + 1] = dataBaseAdapter.addBroadcastPart(broadcast.getBroadcastPartList().get(i));
			AppLog.Log(Const.TAG, "addBroadcastpart:" + ids[i + 1]);
		}
		AppLog.Log(Const.TAG, "addBroadcast:" + ids[0]);
		dataBaseAdapter.close();
		return ids;
	}

	// public long addBroadcastPart(BroadcastPart broadcastPart) {
	// dataBaseAdapter.open();
	// long count = dataBaseAdapter.addBroadcastPart(broadcastPart);
	// AppLog.Log(Const.TAG, "addBroadcastpart:" + count);
	// dataBaseAdapter.close();
	// return count;
	// }

	public long addSentBroadcastPart(long broadcastId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addSentBroadcastPart(broadcastId);
		AppLog.Log(Const.TAG, "addSentBroadcastPart:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long addDeliveredBroadcastPart(long broadcastId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addDeliveredBroadcastPart(broadcastId);
		AppLog.Log(Const.TAG, "addDeliveredBroadcastPart:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public boolean isRetryBroadcastPart(long broadcastId, String time) {
		dataBaseAdapter.open();
		boolean isRetry = dataBaseAdapter.isRetryBroadcastPart(broadcastId, time);
		AppLog.Log(Const.TAG, "isRetryBroadcastPart:" + isRetry);
		dataBaseAdapter.close();
		return isRetry;
	}

	public long addSent(long broadcastId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addSent(broadcastId);
		AppLog.Log(Const.TAG, "addSent:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long addDeliverd(long broadcastId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addDeliverd(broadcastId);
		AppLog.Log(Const.TAG, "addDeliverd:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public ArrayList<BroadcastPart> getBroadcastPartList(String time,
			ArrayList<BroadcastPart> broadcastPartList/* , String time1 */) {
		dataBaseAdapter.open();
		dataBaseAdapter.getBroadcastPartList(time,
				broadcastPartList/* , time1 */);
		dataBaseAdapter.close();
		return broadcastPartList;
	}

	public ArrayList<Broadcast> getBroadcastList(String time,
			ArrayList<Broadcast> broadcastList/* , String time1 */) {
		dataBaseAdapter.open();
		dataBaseAdapter.getBroadcastList(time, broadcastList/* , time1 */);
		dataBaseAdapter.close();
		return broadcastList;
	}

	public long[] addFeedback(Feedback feedback, long ids[]) {
		dataBaseAdapter.open();
		ids[0] = dataBaseAdapter.adddFeedback(feedback);
		for (int i = 0; i < feedback.getFeedbackPartList().size(); i++) {
			feedback.getFeedbackPartList().get(i).setFeedbackId(ids[0]);
			ids[i + 1] = dataBaseAdapter.addFeedbackPart(feedback.getFeedbackPartList().get(i));
			AppLog.Log(Const.TAG, "addFeedbackpart:" + ids[i + 1]);
		}

		AppLog.Log(Const.TAG, "addFeedback:" + ids[0]);
		dataBaseAdapter.close();
		return ids;
	}

	public long addSentFeedback(long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addSentFeedback(feedbackId);
		AppLog.Log(Const.TAG, "addSent:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long addDeliverdFeedback(long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addDeliverdFeedback(feedbackId);
		AppLog.Log(Const.TAG, "addDeliverd:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long addReceivedFeedback(long feedbackId, int answer, boolean isReceivedIncrease) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addReceivedFeedback(feedbackId, answer, isReceivedIncrease);
		AppLog.Log(Const.TAG, "addReceived:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long addSentFeedbackPart(long feedbackPartId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addSentFeedbackPart(feedbackPartId);
		AppLog.Log(Const.TAG, "addSent:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long addDeliverdFeedbackPart(long feedbackPartId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addDeliverdFeedbackPart(feedbackPartId);
		AppLog.Log(Const.TAG, "addDeliverd:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public long getFeedbackId(long feedbackPartId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.getFeedbackId(feedbackPartId);
		AppLog.Log(Const.TAG, "addReceived:" + count);
		dataBaseAdapter.close();
		return count;
	}
	

	public long addReceivedFeedbackPart(long feedbackPartId, int answer, boolean isReceivedIncrease,
			/* long questionNo, */String number, String numberWithoutCountryCode) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addReceivedFeedbackPart(feedbackPartId, answer, isReceivedIncrease,
				/* questionNo, */number, numberWithoutCountryCode);
		AppLog.Log(Const.TAG, "addReceivedFeedbackPart:" + count);
		dataBaseAdapter.close();
		return count;
	}

	public ArrayList<FeedbackPart> getFeedbackPartList(String time,
			ArrayList<FeedbackPart> feedbackPartList/* , String time1 */) {
		dataBaseAdapter.open();
		dataBaseAdapter.getFeedbackPartList(time, feedbackPartList/* , time1 */);
		dataBaseAdapter.close();
		return feedbackPartList;
	}

	public ArrayList<FeedbackPart> getFeedbackPartList(ArrayList<FeedbackPart> feedbackPartList) {
		dataBaseAdapter.open();
		dataBaseAdapter.getFeedbackPartList(feedbackPartList);
		dataBaseAdapter.close();
		return feedbackPartList;
	}

	public Feedback getFeedback(long feedbackId) {
		dataBaseAdapter.open();
		Feedback feedback = dataBaseAdapter.getFeedback(feedbackId);
		dataBaseAdapter.close();
		return feedback;
	}

	public ArrayList<Feedback> getFeedbackList(String time,
			ArrayList<Feedback> feedbackList/* , String time1 */) {
		dataBaseAdapter.open();
		dataBaseAdapter.getFeedbackList(time, feedbackList/* , time1 */);
		dataBaseAdapter.close();
		return feedbackList;
	}

	public long addMessageInFeedbackPart(String msg, long feedbackPartId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addMessageInFeedbackPart(msg, feedbackPartId);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addMessageInFeedbackPart:" + count);
		return count;
	}

	public long addMessageInFeedback(String msg, long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addMessageInFeedback(msg, feedbackId);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addMessageInFeedback:" + count);
		return count;
	}

	public long addTimeInFeedback(String time, long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addTimeInFeedback(time, feedbackId);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addTimeInFeedback:" + count);
		return count;
	}

	public long addTimeInFeedbackPart(String time, long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addTimeInFeedbackPart(time, feedbackId);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addTimeInFeedback:" + count);
		return count;
	}

	public int retryFeedbackPart(long broadcastId, String time) {
		dataBaseAdapter.open();
		int retry = dataBaseAdapter.retryFeedbackPart(broadcastId, time);
		AppLog.Log(Const.TAG, "retryFeedbackPart:" + retry);
		dataBaseAdapter.close();
		return retry;

	}

	public long addQuestion(Question question) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addQuestion(question);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addQuestion:" + count);
		return count;
	}

	// public long addAnswer(String answer, long feedbackPartId, long
	// questionNo) {
	// dataBaseAdapter.open();
	// long count = dataBaseAdapter.addAnswer(answer, feedbackPartId,
	// questionNo);
	// dataBaseAdapter.close();
	// AppLog.Log(Const.TAG, "addAnswer:" + count);
	// return count;
	// }

	public ArrayList<Question> getQuestionList(ArrayList<Feedback> feedbackList, ArrayList<Question> questionList) {
		dataBaseAdapter.open();
		dataBaseAdapter.getQuestionList(feedbackList, questionList);
		dataBaseAdapter.close();
		return questionList;
	}

	public long addSentQuestion(long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addSentQuestion(feedbackId);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addSentQuestion:" + count);
		return count;
	}

	public long addDeliverdQuestion(long feedbackId) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addDeliverdQuestion(feedbackId);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addDeliverdQuestion:" + count);
		return count;
	}

	public long addReceivedQuestion(long feedbackId, long qustionNo, int answer, boolean isReceivedIncrease) {
		dataBaseAdapter.open();
		long count = dataBaseAdapter.addReceivedQuestion(feedbackId, qustionNo, answer, isReceivedIncrease);
		dataBaseAdapter.close();
		AppLog.Log(Const.TAG, "addReceivedQuestion:" + count);
		return count;
	}
}
