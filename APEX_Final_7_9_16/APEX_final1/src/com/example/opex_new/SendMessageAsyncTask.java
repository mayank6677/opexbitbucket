package com.example.opex_new;

import java.util.ArrayList;

import com.example.opex_new.Utils.AppLog;
import com.example.opex_new.model.Member;

import android.app.PendingIntent;
import android.os.AsyncTask;
import android.telephony.SmsManager;

public class SendMessageAsyncTask extends AsyncTask<Object, Void, Void> {

	ArrayList<Member> memberList;
	final String message;
	final PendingIntent sendPendingIntent;
	final PendingIntent deliveredPendingIntent;

	public SendMessageAsyncTask(ArrayList<Member> memberList, final String message,
			final PendingIntent sendPendingIntent, final PendingIntent deliveredPendingIntent) {
		// TODO Auto-generated constructor stub
		this.memberList = memberList;
		this.message = message;
		this.sendPendingIntent = sendPendingIntent;
		this.deliveredPendingIntent = deliveredPendingIntent;
	}

	@Override
	protected Void doInBackground(Object... params) {
		// TODO Auto-generated method stub

		for (int i = 0; i < memberList.size(); i++) {
			sendSMS(memberList.get(i).getPhone(), message, sendPendingIntent, deliveredPendingIntent);
		}
		return null;
	}

	public void sendSMS(final String phoneNo, final String message, final PendingIntent sendPendingIntent,
			final PendingIntent deliveredPendingIntent) {
		AppLog.Log("", "sendSMS");
		// AndyUtils.showSimpleProgressDialog(this);
		SmsManager smsManager = SmsManager.getDefault();
		try {
			AppLog.Log("", "try");

			if (message.length() > 160) {
				ArrayList<String> messageList = smsManager.divideMessage(message);
				for (int i = 0; i < messageList.size(); i++) {
					if (i == 1) {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), sendPendingIntent,
								deliveredPendingIntent);
					} else {
						smsManager.sendTextMessage(phoneNo, null, messageList.get(i), null, null);
					}

				}
			} else {
				smsManager.sendTextMessage(phoneNo, null, message, sendPendingIntent, deliveredPendingIntent);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
